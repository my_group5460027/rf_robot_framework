#!/bin/bash

# This script gets automatically executed if a container is started, no need to manually run this.
cd /root

# This library is needed for compilation of the ws
cp ~/catkin_ws/src/rf_cartesian_motion_generator/plugins/mogen_p2p/libmogen_p2p.so /usr/local/lib/libmogen_p2p.so
ldconfig -n -v /usr/local/lib 
cd ~/catkin_ws
source /opt/ros/noetic/setup.bash
source /root/ur_ws/devel/setup.bash
rosdep install --from-paths  src --ignore-src -r -y
catkin build -DCMAKE_BUILD_TYPE=Release

# Read the correct IPs and write them to the .bashrc
while read -r line; do 
    echo "export $line" >> ~/.bashrc
    echo "set $line"
done < ~/catkin_ws/src/ros_ips.txt

# Add /usr/local/lib to the dynamic library path
echo "LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib/" >> ~/.bashrc

# To keep the container alive, we open a shell here
/bin/bash
