# rf-robot-framework

## About RFRobotFramework

RFRobotFramework is a single ROS node that wraps franka_ros and ros_control and provides a graphical user-interface to offer additional useful features, such as teach-in of task points and joint configurations, easy startup of programs, resetting errors... The components are, as usual in ROS, designed to be executed on multiple machines. With this setup franka_ros and ros_control part is expected to run on a control-pc with a realtime kernel, that is directly connected to the franka master controller.
The framework is designed such that it allows the development either directly on the control-pc or on separate developer-pcs which are connected via ethernet with the control-pc and do not need to have an installed realtime.

The framework currently supports three types of motions, which are defined as classes 

- MoveCartesian (RFLIN) : It generates linear moves from one Cartesian point to another. 

- MoveJoint (RFPTP): It generates moves direct from one point to another in joint-space

- MoveToContact (RFMTC): a linear point moves in a particular direction until a opposing force is exceeded.

Within these classes there are a series of functions available to be executed with the desired type of motion. E.g. the robot can move to a desired point, or with a relative movement in a specific direction from a point. More about the classes and functions can be found in the [Doxygen Documentation](/doc/doxygen_output.zip).

This Framework currently supports the Franka Emika Panda robot, a simulation on CoppeliaSim and limited support for Universal Robots. 
Note though, that the installation documents focus on the usage with a Franka Emika robot and are currently only available in German.

## Installation
This repository comes with a compiled docker image with Ubuntu 20.04 including all necessary packages and requirements to enable easy and fast usage of the framework.
The preparation of your computer (control-pc and eventually developer-pc) and the necessary settings of the Franka robot are described in detail here:
- [Installation instructions](/doc/RF-Framework_Installation.pdf)

Note that the RFRobotFramework requires FCI for working with a Franka robot and that a realtime kernel is required to use FCI see <https://frankaemika.github.io/docs/>.
We provide more detailed installation instructions for the realtime kernel here:
- [Realtime kernel installation](/doc/RT-Kernel_Installation.pdf)

## Startup and user guide
A detailed documentation for the startup and usage of the RFRobotFramework can be found here:
- [Startup](/doc/RF-Framework_Startup.pdf)
- [Overview and user guide](/doc/RF-Framework_Programming.pdf)
 
## Simulation on CoppeliaSim

This package extends the `rf-robot-control` framework with robot simulation capabilities. Based on Coppeliasim, it can be used to test paths and programs before running them on real hardware. It is a single node that sends commands from the control framwork to Coppeliasim via a BlueZero-API connection. More information about the startup and usage of the simulation environment can be found here:
- [Simulation user guide](/doc/RF-Framework_Simulation.pdf). 

So far, simulation is supported for the Franka Emika Panda and Universal robots, but it can be easily expanded (more details below).

## Additional guidance 
- [Framework design](/doc/tutorials/Framework/design.md)
- [Simulation design](/doc/tutorials/Simulation/structure.md)
- [Adding other robots and grippers to the simulation](/doc/tutorials/Simulation/expansion.md)
- [Simulation troubleshooting](/doc/tutorials/Simulation/troubleshooting.md)

## Contributors
The RFRobotFramework was developed and extended in several theses at the Institute of Automatic Control of the Leibniz University Hannover in the context of the project *roboterfabrik* which is supported by the Region Hannover. The following developers contributed to the project:
- Dwayne Steinke
- Hannes Südkamp
- Tom Hattendorf
- Philipp Caspers
- Keno Garbe
- Anais Millan
- Pascal Popp
- Marvin Becker