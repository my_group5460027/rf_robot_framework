/// @file rf_cartesian_motion_generator.cpp
///
/// @author Dwayne Steinke
/// @date 19.12.19
///
/// @brief Class implementation of RFCartesianMotionGenerator
///
/// rf_cartesian_motion_generator.cpp
/// rf_cartesian_motion_generator
///
/// Created by Dwayne Steinke on 19.12.19 for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.
///
///


#include "rf_cartesian_motion_generator/rf_cartesian_motion_generator.h"
namespace rf {

RFCartesianMotionGenerator::RFCartesianMotionGenerator()
    : m_actionServer("MoveLinearCartesian", false) {}

RFCartesianMotionGenerator::~RFCartesianMotionGenerator() {}

bool RFCartesianMotionGenerator::init(
    hardware_interface::RobotHW* robot_hardware, ros::NodeHandle& node_handle) {
    std::string arm_id;
    if (!node_handle.getParam("arm_id", arm_id)) {
        ROS_ERROR(
            "RFCartesianMotionGenerator::init - Could not get parameter "
            "arm_id");
        return false;
    }

    m_pVelocityCartesianInterface =
        robot_hardware->get<franka_hw::FrankaVelocityCartesianInterface>();
    if (m_pVelocityCartesianInterface == nullptr) {
        ROS_ERROR(
            "RFCartesianMotionGenerator::init - Could not get Velocity "
            "Cartesian interface from hardware");
        return false;
    }

    try {
        m_pCartesianVelocityHandle =
            std::make_unique<franka_hw::FrankaCartesianVelocityHandle>(
                m_pVelocityCartesianInterface->getHandle(arm_id + "_robot"));
    } catch (const hardware_interface::HardwareInterfaceException& e) {
        ROS_ERROR_STREAM(
            "RFCartesianMotionGenerator::init - Exception getting Cartesian "
            "handle: "
            << e.what());
        return false;
    }



    return true;
}







void RFCartesianMotionGenerator::starting(const ros::Time&) {
    ROS_INFO_STREAM(
        "RFCartesianMotionGenerator::starting - Starting controller");


    // The order is important: controllerIsRunning is required
    // prior to a call to handleActionServer()
    m_actionServer.start();
    controllerIsRunning = true;
    m_actionServerThreadResult =
        std::async(std::launch::async,
                   &RFCartesianMotionGenerator::handleActionServer, this);
    ROS_INFO_STREAM("After async");
    m_elapsedTime = ros::Duration(0.0);

    // This setCommand is mandatory at startup. If the motion was aborted due to
    // a reflex and the controller stopped (slowdown through controller) it
    // occurs that libfranka seems to follow the last set setCommand (which was
    // some velocity) This leads to velocity discontinuity if you restart this
    // motion generator.
    std::array<double, 6> command = {0, 0, 0, 0, 0, 0};
    m_pCartesianVelocityHandle->setCommand(command);
}

void RFCartesianMotionGenerator::update(const ros::Time&,
                                        const ros::Duration& period) {
    m_elapsedTime += period;

    if (m_control) {
        ROS_INFO_STREAM_THROTTLE(
            1, "RFCartesianMotionGenerator::update - controlling");

        if (!m_motionGeneratorInitialized) {
            // Initialize
            m_motionGenerator.initialize(m_inputMotionGenerator,
                                         m_parametersMotionGenerator);
            m_motionGeneratorInitialized = true;
            m_controlGoalReached = false;
        }

        // Control step
        m_motionGenerator.step(m_inputMotionGenerator, m_outputMotionGenerator);

        std::array<double, 6> command;
        bool isZeroCommand;

        isZeroCommand = !generateCommand(command);
        m_pCartesianVelocityHandle->setCommand(command);

        if (checkSuccess(isZeroCommand)) {
            m_controlGoalReached = true;
            ROS_INFO_STREAM(
                "RFCartesianMotionGenerator::update - control goal reached");
        }

    } else {
        ROS_INFO_STREAM_THROTTLE(
            0.5, "RFCartesianMotionGenerator::update - Not controlling");
        if (m_motionGeneratorInitialized) {
            // Terminate
            m_motionGenerator.terminate();
            m_motionGeneratorInitialized = false;
        }
    }
}

void RFCartesianMotionGenerator::stopping(const ros::Time&) {
    ROS_INFO_STREAM(
        "RFCartesianMotionGenerator::stopping - Stopping controller");
    if (m_motionGeneratorInitialized){
		m_motionGenerator.terminate();
		m_motionGeneratorInitialized = false;
	}

    controllerIsRunning = false;
    m_actionServerThreadResult.get();
    if (m_actionServer.isActive()) {
        rf_cartesian_motion_generator::MoveLinearCartesianResult result;
        result.success = false;
        m_actionServer.setAborted(result);
    }
    m_actionServer.shutdown();
    // WARNING: DO NOT SEND ZERO VELOCITIES HERE AS IN CASE OF ABORTING DURING
    // MOTION A JUMP TO ZERO WILL BE COMMANDED PUTTING HIGH LOADS ON THE ROBOT.
    // LET THE DEFAULT BUILT-IN STOPPING BEHAVIOR SLOW DOWN THE ROBOT.
}

void RFCartesianMotionGenerator::updateActionServer() {
    ROS_INFO_STREAM_THROTTLE(1,
                             "RFCartesianMotionGenerator::updateActionServer");
    // Accept or preempt new goals.
    if (!m_motionGeneratorInitialized && !m_control) {
        if (m_actionServer.isNewGoalAvailable()) {
            ROS_INFO_STREAM(
                "RFCartesianMotionGenerator::updateActionServer - Accepting "
                "new goal");
            m_activeGoal = m_actionServer.acceptNewGoal();
            parseGoal();
            m_zeroCommandsCount = 0;
        }
    }

    if (m_actionServer.isActive()) {
        if (m_actionServer.isPreemptRequested()) {
            rf_cartesian_motion_generator::MoveLinearCartesianResult result;
            result.success = false;
            m_actionServer.setPreempted(result);
        }
    }

    // Check if goal is reached
    if (m_controlGoalReached) {
        rf_cartesian_motion_generator::MoveLinearCartesianResult result;
        result.success = true;
        m_actionServer.setSucceeded(result);
        m_controlGoalReached = false;
    }

    if (m_actionServer.isActive()) {
        m_control = true;
    } else {
        m_control = false;
    }
}

void RFCartesianMotionGenerator::handleActionServer() {
    ros::Rate rate(m_ACTION_SERVER_UPDATE_RATE);  // Hz
    while (controllerIsRunning) {
        updateActionServer();
        rate.sleep();
    }
}

void RFCartesianMotionGenerator::parseGoal() {
    ROS_INFO_STREAM(
        "RFCartesianMotionGenerator::parseGoal - Parsing new Goal with "
        "parameters:");
    m_initialPose = m_pCartesianVelocityHandle->getRobotState().O_T_EE;

    Eigen::Matrix4d initialPose(m_initialPose.data());
    Eigen::Matrix4d targetPose(m_activeGoal->O_T_EE_d.data());
    Eigen::Matrix4d referencePoint(m_activeGoal->referencePoint.data());
    Eigen::Matrix4d unit;
    unit.setIdentity();

    if (m_activeGoal->relative) {
        ROS_INFO_STREAM("Relative Motion enabled.");

        if (!referencePoint.isZero(0)) {
            if (m_activeGoal->useEECordiantes) {
                targetPose = referencePoint * targetPose;

            } else {
                targetPose = referencePoint + targetPose;
            }
        } else {
            if (m_activeGoal->useEECordiantes) {
                targetPose = initialPose * targetPose;

            } else {
                targetPose = initialPose + targetPose - unit;
            }
        }
    }

    ROS_INFO_STREAM("Inital Pose: \n" << initialPose);
    ROS_INFO_STREAM("TargetPose: \n" << targetPose);
    m_parametersMotionGenerator.TF_T_EE_0 = initialPose;
    m_parametersMotionGenerator.TF_T_EE_1 = targetPose;

    Eigen::Vector2d dX_max(m_activeGoal->dX_max[0], m_activeGoal->dX_max[1]);
    Eigen::Vector2d ddX_max(m_activeGoal->ddX_max[0], m_activeGoal->ddX_max[1]);
    ROS_INFO_STREAM("dX_max: " << dX_max);
    ROS_INFO_STREAM("ddX_max: " << ddX_max);
    m_parametersMotionGenerator.dX_max = dX_max;
    m_parametersMotionGenerator.ddX_max = ddX_max;

    Eigen::Vector2d t_scale(m_activeGoal->t_scale[0], m_activeGoal->t_scale[1]);
    ROS_INFO_STREAM("t_scale: " << t_scale);
    m_inputMotionGenerator.t_scale = t_scale;
}

bool RFCartesianMotionGenerator::generateCommand(
    std::array<double, 6>& command) {
    bool nonZero = false;
    double* data = m_outputMotionGenerator.dX_d.data();
    for (size_t i = 0; i < 6; i++) {
        command[i] = data[i];
        if (!isZero(data[i])) {
            nonZero = true;
        }
    }
    return nonZero;
}

bool RFCartesianMotionGenerator::isZero(double number) {
    if (number > -0.000001 && number < 0.000001) {
        return true;
    }
    return false;
}

bool RFCartesianMotionGenerator::checkSuccess(bool isZeroCommand) {
    if (isZeroCommand) {
        m_zeroCommandsCount = m_zeroCommandsCount + 1;
    } else {
        m_zeroCommandsCount = 0;
    }

    if (m_zeroCommandsCount >= m_controlCyclesToSuccess) {
        m_zeroCommandsCount = 0;
        return true;
    } else {
        return false;
    }
}

}  // namespace rf

PLUGINLIB_EXPORT_CLASS(rf::RFCartesianMotionGenerator,
                       controller_interface::ControllerBase);
