#!/bin/bash

# Run this script to prepare the host of the rf_robot_framework docker container

sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install -y docker.io curl 
sudo groupadd docker || true
sudo usermod -aG docker $USER

# Install X11 Docker for GUI support
curl -fsSL https://raw.githubusercontent.com/mviereck/x11docker/master/x11docker | sudo bash -s -- --update

# Create an alias for quick container startup
while read -r line; do 
    echo "alias cont='x11docker --name=rf_robot_framework -it -I --hostdisplay --gpu --user=RETAIN --sudouser --network=host --hostdbus -- --mount type=bind,source=$line,target=/root/catkin_ws --privileged -- registry.gitlab.com/my_group5460027/rf_robot_framework'" >> ~/.bashrc
    echo "Setting workspace path to: $line"
done < ~/catkin_ws/src/workspace_path.txt

echo "alias contbuild='docker build ~/catkin_ws/src/ -t registry.gitlab.com/my_group5460027/rf_robot_framework'" >> ~/.bashrc

# Install vscode with needed extensions
sudo snap install --classic code
code --install-extension ms-azuretools.vscode-docker
code --install-extension ms-python.python
code --install-extension ms-iot.vscode-ros
code --install-extension ms-python.vscode-pylance
code --install-extension ms-vscode-remote.remote-containers
code --install-extension ms-vscode-remote.remote-ssh
code --install-extension ms-vscode.cmake-tools
code --install-extension ms-vscode.cpptools
code --install-extension ms-vscode.cpptools-extension-pack 
code --install-extension streetsidesoftware.code-spell-checker
code --install-extension vscode-icons-team.vscode-icons

# Backup the vscode config if it exists
[ -f ~/.config/Code/User/settings.json ] && sudo mv ~/.config/Code/User/settings.json ~/.config/Code/User/settings.json.bak

# Write a new vscode config
echo '{
    "workbench.iconTheme": "vscode-icons",
    "remote.downloadExtensionsLocally": true,
    "terminal.integrated.scrollback": 10000,
    "[cpp]": {
        "editor.defaultFormatter": "ms-vscode.cpptools"
    },
    "editor.formatOnSave": true,
    "C_Cpp.clang_format_sortIncludes": true,
    "C_Cpp.formatting": "clangFormat",
    "remote.containers.defaultExtensions": [
        "ms-python.python",
        "ms-python.vscode-pylance",
        "ms-vscode.cpptools",
	"ms-vscode.cpptools-extension-pack",
        "streetsidesoftware.code-spell-checker",
        "vscode-icons-team.vscode-icons",
        "ms-vscode.cmake-tools",
        "ms-iot.vscode-ros",
    ],
}' >> ~/.config/Code/User/settings.json

# Write a new vscode container config
rm -f ~/.config/Code/User/globalStorage/ms-vscode-remote.remote-containers/imageConfigs/registry.gitlab.com%2fmy_group5460027%2frf_robot_framework%3alatest.json
mkdir -p ~/.config/Code/User/globalStorage/ms-vscode-remote.remote-containers/imageConfigs
echo '{
	"extensions": [
		"cschlosser.doxdocgen",
		"jeff-hykin.better-cpp-syntax",
		"ms-python.python",
		"ms-python.vscode-pylance",
		"ms-toolsai.jupyter",
		"ms-toolsai.jupyter-keymap",
		"ms-toolsai.jupyter-renderers",
		"ms-vscode.cmake-tools",
		"ms-vscode.cpptools",
		"ms-vscode.cpptools-extension-pack",
		"ms-vscode.cpptools-themes",
		"streetsidesoftware.code-spell-checker",
		"twxs.cmake",
		"vscode-icons-team.vscode-icons",
		"ms-iot.vscode-ros"
	],
	"workspaceFolder": "/root/catkin_ws"
}' >> ~/.config/Code/User/globalStorage/ms-vscode-remote.remote-containers/imageConfigs/registry.gitlab.com%2fmy_group5460027%2frf_robot_framework%3alatest.json

echo "

Success, please restart the pc before you continue."
