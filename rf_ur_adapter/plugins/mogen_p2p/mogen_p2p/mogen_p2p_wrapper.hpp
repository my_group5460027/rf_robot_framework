#pragma once

#include "simulink_pipeline/plugin.hpp"
class mogen_p2pModelClass;

namespace mogen_p2p {

struct In_P_mogen_p2p : public In_P{
Eigen::Matrix<double,4,4> TF_T_EE_0;
Eigen::Matrix<double,4,4> TF_T_EE_1;
Eigen::Matrix<double,2,1> dX_max;
Eigen::Matrix<double,2,1> ddX_max;
};
struct In_U_mogen_p2p : public In_U{
Eigen::Matrix<double,2,1> t_scale;
};
struct Out_Y_mogen_p2p : public Out_Y{
Eigen::Matrix<double,6,1> dX_d;
};
struct Out_L_mogen_p2p : public Out_L{
Eigen::Matrix<double,1,1> arrived;
};
class mogen_p2p : Plugin{
public:
mogen_p2p();
~mogen_p2p();
Out_Y_mogen_p2p get_out_y();
Out_L_mogen_p2p get_out_l();
void initialize(const In_U& in_u,const In_P& in_p,bool log = false,unsigned long long l_len = 0,std::string path_logs="");
void step(const In_U& in_u,Out_Y& out_y);
void terminate();

private:
void write_params_to_model();
void write_logs();
mogen_p2pModelClass* _model;
std::vector<In_U_mogen_p2p> _log_in_u;
std::vector<Out_Y_mogen_p2p> _log_out_y;
std::vector<Out_L_mogen_p2p> _log_out_l;
Out_L_mogen_p2p _log;
};

}