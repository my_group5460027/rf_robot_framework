# Installing a plugin of the simulink pipeline

Follow the documentation of the simulink pipeline:
"
1. Create a dedicated folder for plugins in your project folder. In the following this folder is referred to as plugins . Note that you can put the plugin
whereever you like as long as you understand how CMake handles project structures


2. Copy your plugin (i.e. the entire folder as it is from the install folder) into the plugins folder in your project structure.


3. In the main CMakeLists.txt of your project add the following three lines and substitute the name of the plugin. It
is assumed that you know enough about CMake to include these lines at a proper place in the CMakeLists.txt
file. As a hint, it should come before any linking that requires the plugin.

		add_subdirectory(plugins/<plugin_name>)
		include_directories(${<plugin_name>_INCLUDE_DIRS})
		link_directories(${<plugin_name>_LIBRARIES})



4. Link the plugin to the library or executable that uses it, e.g.

		target_link_libraries(<your_library> <plugin_name>)


5. At this point your program should compile. Note that in case of installing your program in binary form somewhere else, you also need to install the
plugin library named lib<plugin_name>.so in the plugin's installation folder.
"

(Possible addition to step 5: Copy the library to usr/local/lib and install it.

		cd /path/to/<plugin_name>
		sudo cp lib<plugin_name>.so /usr/local/lib/lib<plugin_name>.so
		ldconfig -n -v /usr/local/lib

)
