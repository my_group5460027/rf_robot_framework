/// @file rf_ur_cartesian_motion_generator.cpp
///
/// @author Tom Hattendorf
/// @date 08.10.2021
///
/// @brief Class implementation of RFURCartesianMotionGenerator
///
/// rf_ur_cartesian_motion_generator.cpp
/// rf_ur_cartesian_motion_generator
///
/// Created by Tom Hattendorf on 08.10.2021 for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.
///
///


#include "rf_ur_adapter/rf_ur_cartesian_motion_generator.h"
namespace rf {

	RFURCartesianMotionGenerator::RFURCartesianMotionGenerator() :m_actionServer("URMoveLinearCartesian", false) {

	}


	RFURCartesianMotionGenerator::~RFURCartesianMotionGenerator() {

	}






	bool RFURCartesianMotionGenerator::init(hardware_interface::RobotHW* robot_hardware, ros::NodeHandle& node_handle) {
		std::string tf_prefix;
		// if (!node_handle.getParam("tf_prefix", tf_prefix)) {
		// 	ROS_ERROR("RFURCartesianMotionGenerator::init - Could not get parameter arm_id");
		// 	return false;
		// }

		m_pTwistCommandInterface = robot_hardware->get<ros_controllers_cartesian::TwistCommandInterface>();
		if (m_pTwistCommandInterface == nullptr) {
			ROS_ERROR("RFURCartesianMotionGenerator::init - Could not get Twist interface from hardware");
			return false;
		}

		try {
			m_pTwistCommandHandle = std::make_unique<ros_controllers_cartesian::TwistCommandHandle>(m_pTwistCommandInterface->getHandle("tool0_controller"));
		}
		catch (const hardware_interface::HardwareInterfaceException& e) {
			ROS_ERROR_STREAM("RFURCartesianMotionGenerator::init - Exception getting Twist handle: " << e.what());
			return false;
		}


		return true;
	}







	void RFURCartesianMotionGenerator::starting(const ros::Time&) {
		ROS_INFO_STREAM("RFURCartesianMotionGenerator::starting - Starting controller");


		if (controllerIsRunning) {
			ROS_WARN_STREAM("RFURCartesianMotionGenerator::starting - Motion generator is already running. Ignoring new request for start setup routine.");
			return;
		}

		// The order is important: controllerIsRunning is required
		// prior to a call to handleActionServer()
		m_actionServer.start();
		controllerIsRunning = true;
		m_actionServerThreadResult = std::async(std::launch::async, &RFURCartesianMotionGenerator::handleActionServer, this);
		ROS_INFO_STREAM("After async");
		m_elapsedTime = ros::Duration(0.0);

		// This setCommand is mandatory at startup. If the motion was aborted due to a reflex
		// and the controller stopped (slowdown through controller) it occurs that libfranka
		// seems to follow the last set setCommand (which was some velocity)
		// This leads to velocity discontinuity if you restart this motion generator.
		std::array<double, 6> command = { 0,0,0,0,0,0 };
		geometry_msgs::Twist twistCommand = convertCommandToTwist(command);
		m_pTwistCommandHandle->setCommand(twistCommand);
	}



	void RFURCartesianMotionGenerator::update(const ros::Time&, const ros::Duration& period) {
		m_elapsedTime += period;

		if (m_control) {
			ROS_INFO_STREAM_THROTTLE(1, "RFURCartesianMotionGenerator::update - controlling");

			if (!m_motionGeneratorInitialized) {
				//Initialize
				m_motionGenerator.initialize(m_inputMotionGenerator, m_parametersMotionGenerator);
				m_motionGeneratorInitialized = true;
				m_controlGoalReached = false;
			}

			// Control step - update twice because the UR connection runs at 500 Hz but the motion controller is fixed at 1 kHz
			m_motionGenerator.step(m_inputMotionGenerator, m_outputMotionGenerator);
			m_motionGenerator.step(m_inputMotionGenerator, m_outputMotionGenerator);
			
            std::array<double, 6> command;
		    bool isZeroCommand;

		    isZeroCommand = !generateCommand(command);

			geometry_msgs::Twist twistCommand = convertCommandToTwist(command);
			m_pTwistCommandHandle->setCommand(twistCommand);

			if (checkSuccess(isZeroCommand)) {
				m_controlGoalReached = true;
				ROS_INFO_STREAM("RFURCartesianMotionGenerator::update - control goal reached");
			}

		}
		else {
			ROS_INFO_STREAM_THROTTLE(0.5, "RFURCartesianMotionGenerator::update - Not controlling");
			if (m_motionGeneratorInitialized) {
				//Terminate
				m_motionGenerator.terminate();
				m_motionGeneratorInitialized = false;
			}
		}


	}


	void RFURCartesianMotionGenerator::stopping(const ros::Time&) {
		ROS_INFO_STREAM("RFURCartesianMotionGenerator::stopping - Stopping controller");
        if (m_motionGeneratorInitialized){
            m_motionGenerator.terminate();
            m_motionGeneratorInitialized = false;
        }
        controllerIsRunning = false;
        m_actionServerThreadResult.get();
        if (m_actionServer.isActive()) {
            rf_cartesian_motion_generator::MoveLinearCartesianResult result;
            result.success = false;
            m_actionServer.setAborted(result);
        }
        m_actionServer.shutdown();
    // WARNING: DO NOT SEND ZERO VELOCITIES HERE AS IN CASE OF ABORTING DURING MOTION
		// A JUMP TO ZERO WILL BE COMMANDED PUTTING HIGH LOADS ON THE ROBOT. LET THE DEFAULT
		// BUILT-IN STOPPING BEHAVIOR SLOW DOWN THE ROBOT.
	}




	void RFURCartesianMotionGenerator::updateActionServer() {
		ROS_INFO_STREAM_THROTTLE(1, "RFURCartesianMotionGenerator::updateActionServer");
		// Accept or preempt new goals.
		if (!m_motionGeneratorInitialized && !m_control) {
			if (m_actionServer.isNewGoalAvailable()) {
				ROS_INFO_STREAM("RFURCartesianMotionGenerator::updateActionServer - Accepting new goal");
				m_activeGoal = m_actionServer.acceptNewGoal();
				parseGoal();
				m_zeroCommandsCount = 0;
			}
		}

		if (m_actionServer.isActive()) {
			if (m_actionServer.isPreemptRequested()) {
				rf_cartesian_motion_generator::MoveLinearCartesianResult result;
				result.success = false;
				m_actionServer.setPreempted(result);
			}
		}

		//Check if goal is reached
		if (m_controlGoalReached) {
			rf_cartesian_motion_generator::MoveLinearCartesianResult result;
			result.success = true;
			m_actionServer.setSucceeded(result);
			m_controlGoalReached = false;
		}

		if (m_actionServer.isActive()) {
			m_control = true;
		}
		else {
			m_control = false;
		}
	}


	void RFURCartesianMotionGenerator::handleActionServer() {
		ros::Rate rate(m_ACTION_SERVER_UPDATE_RATE);//Hz
		while (controllerIsRunning) {
			updateActionServer();
			rate.sleep();
		}
	}

	geometry_msgs::Twist RFURCartesianMotionGenerator::convertCommandToTwist(std::array<double, 6> command){
		geometry_msgs::Twist twist;
		twist.linear.x = command[0];
		twist.linear.y = command[1];
		twist.linear.z = command[2];

		twist.angular.x = command[3];
		twist.angular.y = command[4];
		twist.angular.z = command[5];

		return twist;
	}

	std::array<double, 16> RFURCartesianMotionGenerator::convertPoseToArray(geometry_msgs::Pose robotPose){
		std::array<double, 16> poseArray;		
		Eigen::Quaterniond quat;
        Eigen::Matrix4d transMat = Eigen::Matrix4d::Identity();

        double x = robotPose.position.x;
        double y = robotPose.position.y;
        double z = robotPose.position.z;
        quat.w() = robotPose.orientation.w;
        quat.x() = robotPose.orientation.x;
        quat.y() = robotPose.orientation.y;
        quat.z() = robotPose.orientation.z;
        transMat(12) = x;
        transMat(13) = y;
        transMat(14) = z;

        transMat.block(0, 0, 3, 3) = quat.toRotationMatrix();
        for (int i = 0; i<poseArray.size(); i++){
            poseArray.at(i) = transMat(i);
	    }

		return poseArray;
	}

	void RFURCartesianMotionGenerator::parseGoal() {
		ROS_INFO_STREAM("RFURCartesianMotionGenerator::parseGoal - Parsing new Goal with parameters:");
		m_initialPose = convertPoseToArray(m_pTwistCommandHandle->getPose());

        Eigen::Matrix4d initialPose(m_initialPose.data());
        Eigen::Matrix4d targetPose(m_activeGoal->O_T_EE_d.data());
		Eigen::Matrix4d referencePoint(m_activeGoal->referencePoint.data());
		Eigen::Matrix4d unit;
		unit.setIdentity();

        if (m_activeGoal->relative) {
			ROS_INFO_STREAM("Relative Motion enabled.");

			if (!referencePoint.isZero(0)) {
				if (m_activeGoal->useEECordiantes) {
					targetPose = referencePoint * targetPose;

				} else {
					targetPose = referencePoint + targetPose;
				}
			} else {
				if (m_activeGoal->useEECordiantes) {
					targetPose = initialPose * targetPose;

				} else {
					targetPose = initialPose + targetPose - unit;
				}
       		}
    	}
		

		ROS_INFO_STREAM("Inital Pose: \n" << initialPose);
		ROS_INFO_STREAM("TargetPose: \n" << targetPose);
		m_parametersMotionGenerator.TF_T_EE_0 = initialPose;
		m_parametersMotionGenerator.TF_T_EE_1 = targetPose;


		Eigen::Vector2d dX_max(m_activeGoal->dX_max[0], m_activeGoal->dX_max[1]);
		Eigen::Vector2d ddX_max(m_activeGoal->ddX_max[0], m_activeGoal->ddX_max[1]);
		ROS_INFO_STREAM("dX_max: " << dX_max);
		ROS_INFO_STREAM("ddX_max: " << ddX_max);
		m_parametersMotionGenerator.dX_max = dX_max;
		m_parametersMotionGenerator.ddX_max = ddX_max;


		Eigen::Vector2d t_scale(m_activeGoal->t_scale[0], m_activeGoal->t_scale[1]);
		ROS_INFO_STREAM("t_scale: " << t_scale);
		m_inputMotionGenerator.t_scale = t_scale;
	}


	bool RFURCartesianMotionGenerator::generateCommand(std::array<double, 6>& command) {
		bool nonZero = false;
		double* data = m_outputMotionGenerator.dX_d.data();
		for (size_t i = 0; i < 6; i++) {
			command[i] = data[i];
			if (!isZero(data[i])) {
				nonZero = true;
			}
		}
		return nonZero;
	}


	bool RFURCartesianMotionGenerator::isZero(double number) {
		if (number > -0.000001 && number < 0.000001) {
			return true;
		}
		return false;
	}



	bool RFURCartesianMotionGenerator::checkSuccess(bool isZeroCommand) {
		if (isZeroCommand) {
			m_zeroCommandsCount = m_zeroCommandsCount + 1;
		}
		else {
			m_zeroCommandsCount = 0;
		}

		if (m_zeroCommandsCount >= m_controlCyclesToSuccess) {
			m_zeroCommandsCount = 0;
			return true;
		}
		else {
			return false;
		}
	}

} /* RF */


PLUGINLIB_EXPORT_CLASS(rf::RFURCartesianMotionGenerator, controller_interface::ControllerBase);
