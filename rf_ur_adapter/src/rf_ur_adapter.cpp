#include "rf_ur_adapter/rf_ur_adapter.h"

namespace rf {

RFUrAdapter::RFUrAdapter()
    : m_errorRecoveryServer(
          m_NodeHandle, "/franka_control/error_recovery",
          boost::bind(&RFUrAdapter::errorRecovery, this, _1),
          false),  
        stop_action_server(m_NodeHandle, "/franka_gripper/stop",
                         boost::bind(&rf::RFUrAdapter::stop, this, _1,
                                     &m_NodeHandle, &stop_action_server),
                         false),
      move_action_server(m_NodeHandle, "/franka_gripper/move",
                         boost::bind(&rf::RFUrAdapter::move, this, _1,
                                     &m_NodeHandle, &move_action_server),
                         false),
      grasp_action_server(m_NodeHandle, "/franka_gripper/grasp",
                          boost::bind(&rf::RFUrAdapter::grasp, this, _1,
                                      &m_NodeHandle, &grasp_action_server),
                          false),
      homing_action_server(m_NodeHandle, "/franka_gripper/homing",
                           boost::bind(&rf::RFUrAdapter::homing, this, _1,
                                       &m_NodeHandle, &homing_action_server),
                           false)
      {
    m_robotModeSubscriber =
        m_NodeHandle.subscribe("/ur_hardware_interface/robot_mode", 1,
                               &RFUrAdapter::cb_robotModeSubscriber, this);
    m_programStateSubscriber = m_NodeHandle.subscribe(
        "/ur_hardware_interface/robot_program_running", 1,
        &RFUrAdapter::cb_programStateSubscriber, this);
    m_safetyModeSubscriber =
        m_NodeHandle.subscribe("/ur_hardware_interface/safety_mode", 1,
                               &RFUrAdapter::cb_safetyModeSubscriber, this);
    m_jointStateSubscriber = m_NodeHandle.subscribe(
        "/joint_states", 1, &RFUrAdapter::cb_jointStateSubscriber, this);
    m_tfSubscriber =
        m_NodeHandle.subscribe("/tf", 1, &RFUrAdapter::cb_tfSubscriber, this);

    m_frankaStatePublisher = m_NodeHandle.advertise<franka_msgs::FrankaState>(
        "/franka_state_controller/franka_states", 1);

    stop_action_server.start();
    move_action_server.start();
    grasp_action_server.start();
    homing_action_server.start();
    m_errorRecoveryServer.start();
}

RFUrAdapter::~RFUrAdapter() {

}

bool RFUrAdapter::errorRecovery(
    const franka_msgs::ErrorRecoveryGoalConstPtr& goal) {
    m_errorRecoveryServer.setSucceeded();
    return true;
}

void RFUrAdapter::cb_robotModeSubscriber(ur_dashboard_msgs::RobotMode msg) {
    m_urRobotMode = msg;
    handleFrankaState();
}

void RFUrAdapter::cb_programStateSubscriber(std_msgs::Bool msg) {
    m_urProgramState = msg;
    handleFrankaState();
}

void RFUrAdapter::cb_safetyModeSubscriber(ur_dashboard_msgs::SafetyMode msg) {
    m_urSafetyMode = msg;
    handleFrankaState();
}

void RFUrAdapter::handleFrankaState() {
    if (m_urRobotMode.mode == m_urRobotMode.RUNNING &&
        m_urSafetyMode.mode == m_urSafetyMode.NORMAL && m_urProgramState.data) {
        m_frankaState.robot_mode = 1;
    } else if (false) {
        m_frankaState.robot_mode = 2;
    } else if (false) {
        m_frankaState.robot_mode = 3;
    } else if (m_urSafetyMode.mode != m_urSafetyMode.NORMAL &&
               m_urSafetyMode.mode != m_urSafetyMode.ROBOT_EMERGENCY_STOP) {
        m_frankaState.robot_mode = 4;
    } else if (m_urSafetyMode.mode == m_urSafetyMode.ROBOT_EMERGENCY_STOP) {
        m_frankaState.robot_mode = 5;
    } else if (false) {
        m_frankaState.robot_mode = 6;
    } else {
        m_frankaState.robot_mode = 0;
    }
    publishFrankaState();
}

void RFUrAdapter::cb_jointStateSubscriber(sensor_msgs::JointState msg) {
    std::vector<double> q;
    q.assign(msg.position.data(), msg.position.data() + 6);
    q.push_back(0);
    
    m_frankaState.q[0] = q[2];
    m_frankaState.q[1] = q[1];
    m_frankaState.q[2] = q[0];
    m_frankaState.q[3] = q[3];
    m_frankaState.q[4] = q[4];
    m_frankaState.q[5] = q[5];
    m_frankaState.q[6] = q[6];
    publishFrankaState();
}

void RFUrAdapter::cb_tfSubscriber(tf2_msgs::TFMessage msg) {
    if (msg.transforms[0].child_frame_id == "tool0_controller") {
        Eigen::Quaterniond quat;
        Eigen::Matrix4d transMat = Eigen::Matrix4d::Identity();

        float x = msg.transforms[0].transform.translation.x;
        float y = msg.transforms[0].transform.translation.y;
        float z = msg.transforms[0].transform.translation.z;
        quat.w() = msg.transforms[0].transform.rotation.w;
        quat.x() = msg.transforms[0].transform.rotation.x;
        quat.y() = msg.transforms[0].transform.rotation.y;
        quat.z() = msg.transforms[0].transform.rotation.z;
        transMat(12) = x;
        transMat(13) = y;
        transMat(14) = z;

        transMat.block(0, 0, 3, 3) = quat.toRotationMatrix();
        boost::array<double, 16UL> O_T_EE;
        for (int i = 0; i<O_T_EE.size(); i++){
            m_frankaState.O_T_EE.at(i) = transMat(i);
        }
        publishFrankaState();
    }
}

void RFUrAdapter::publishFrankaState() {
    m_frankaStatePublisher.publish(m_frankaState);
}

// GRIPPER -- GRIPPER -- GRIPPER -- GRIPPER -- GRIPPER -- GRIPPER -- GRIPPER --
// GRIPPER -- GRIPPER --
bool RFUrAdapter::closeGripper(ros::NodeHandle* node_handle) {
    try {
        ros::ServiceClient client = node_handle->serviceClient<ur_msgs::SetIO>(
            "/ur_hardware_interface/set_io");
           ur_msgs::SetIORequest req;
        req.fun = 1;
        ur_msgs::SetIOResponse res;
        req.pin = 16;
        req.state = 0;
        client.call(req, res);
        req.pin = 17;
        req.state = 1;
        client.call(req, res);
        return true;
    } catch (...) {
        ROS_WARN_STREAM("Could not close the Schunk Gripper.");
        return false;
    }
}

bool RFUrAdapter::openGripper(ros::NodeHandle* node_handle) {
    try {
        ros::ServiceClient client = node_handle->serviceClient<ur_msgs::SetIO>(
            "/ur_hardware_interface/set_io");
        ur_msgs::SetIORequest req;
        req.fun = 1;
        ur_msgs::SetIOResponse res;
        req.pin = 17;
        req.state = 0;
        client.call(req, res);
        req.pin = 16;
        req.state = 1;
        client.call(req, res);
        // ros::Duration(2.0).sleep();
        return true;
    } catch (...) {
        ROS_WARN_STREAM("Could not open the Schunk Gripper.");
        return false;
    }
}

bool RFUrAdapter::stop(
    const franka_gripper::StopGoalConstPtr& goal, ros::NodeHandle* node_handle,
    actionlib::SimpleActionServer<franka_gripper::StopAction>* as_) {
    ros::ServiceClient client = node_handle->serviceClient<ur_msgs::SetIO>(
        "/ur_hardware_interface/set_io");
    ur_msgs::SetIORequest req;
    ur_msgs::SetIOResponse res;
    req.fun = 1;
    req.pin = 16;
    req.state = 0;
    client.call(req, res);
    req.pin = 17;
    client.call(req, res);
    as_->setSucceeded();
    return res.success; 
}

bool RFUrAdapter::move(
    const franka_gripper::MoveGoalConstPtr& goal, ros::NodeHandle* node_handle,
    actionlib::SimpleActionServer<franka_gripper::MoveAction>* as_) {
    franka_gripper::MoveResult result;
    if (goal->width == 0) {
        if (RFUrAdapter::closeGripper(node_handle)) {
            result.success = true;
            as_->setSucceeded(result);
            return true;
        } else {
            result.success = false;
            as_->setAborted(result);
            return false;
        }
    } else {
        if (RFUrAdapter::openGripper(node_handle)) {
            result.success = true;
            as_->setSucceeded(result);
            return true;
        } else {
            result.success = false;
            as_->setAborted(result);
            return false;
        }
    }
}

bool RFUrAdapter::grasp(
    const franka_gripper::GraspGoalConstPtr& goal, ros::NodeHandle* node_handle,
    actionlib::SimpleActionServer<franka_gripper::GraspAction>* as_) {
    if (RFUrAdapter::closeGripper(node_handle)) {
        as_->setSucceeded();
        return true;
    } else {
        as_->setAborted();
        return false;
    }
}

bool RFUrAdapter::homing(
    const franka_gripper::HomingGoalConstPtr& /*goal*/,
    ros::NodeHandle* node_handle,
    actionlib::SimpleActionServer<franka_gripper::HomingAction>* as_) {
    ROS_INFO_STREAM("Homing is not supported for Schunk Gripper.");
    as_->setAborted();
}

}  // namespace rf

int main(int argc, char** argv) {
    ros::init(argc, argv, "ur_adapter");
    ROS_INFO_STREAM("UR-Adapter started");
    rf::RFUrAdapter m_Ur_Adapter;
    ros::spin();
}
