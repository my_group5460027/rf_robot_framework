/// @file rf_ur_adapter.h
///
/// @author Hannes Südkamp
/// @date 14.01.21
///
/// @brief Class definition of RFURAdapter
///
/// rf_ur_adapter_h.h
/// rf_robot_control
///
/// Created by Hannes Südkamp on 14.01.21 for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.
///
///

#ifndef RF_UR_ADAPTER_H
#define RF_UR_ADAPTER_H

#include <sensor_msgs/JointState.h>
#include <std_msgs/Bool.h>
#include <tf/tf.h>
#include <tf2_msgs/TFMessage.h>
#include <ur_dashboard_msgs/RobotMode.h>
#include <ur_dashboard_msgs/SafetyMode.h>
#include <ur_msgs/SetIO.h>

#include <eigen3/Eigen/Dense>
#include <future>
#include <franka_msgs/ErrorRecoveryAction.h>

#include "rf_cartesian_motion_generator/MoveLinearCartesianAction.h"
#include "rf_robot_control/rf_adapter.h"

namespace rf {

class RFUrAdapter : public RFAdapter {
    /// @brief Abstract interface class for Universal Robot adapter
    ///
    /// Inherits from RFAdaper Master Class
    ///
    /// The communication between the adapter and the robot can be
    /// divided into three problems. First, there is the question of
    /// the status of the robot. Here, it is primarily relevant whether
    /// commands can be executed, whether faults are present or in
    /// which joint angle configuration the robot is. This is followed
    /// by the sending of motion commands in joint and task space to
    /// the robot. Last is the opening and closing of the gripper.
    ///
    /// Subscribes the Topics /tf,
    ///                       /joint_angles,
    ///                       /robot_mode,
    ///                       /safety_mode,
    ///                       /robot_program_running
    /// each callback function saves the message and calls
    /// handleFrankaState, which updates FrankaState and calls
    /// publishFrankaState.
    ///

   private:
    /// @name Functions for state conversion and communication
    /// @{

    /// @brief Updates m_frankaState with new infromation
    ///
    /// Looks at m_urRobotMode, m_urSafetyMode and m_urProgramState
    /// and constructs the m_frankazState.robot_mode
    void handleFrankaState();
    /// @brief Publishs m_frankaState
    void publishFrankaState();
    /// @}

    /// @name ActionServer, callbackfunctions aqnd helperfunctions for gripper
    /// movements an
    /// @{
    /// @brief Action Server for "stop"
    actionlib::SimpleActionServer<franka_gripper::StopAction>
        stop_action_server;
    /// @brief Action Server for "move"
    actionlib::SimpleActionServer<franka_gripper::MoveAction>
        move_action_server;
    /// @brief Action Server for "grasp"
    actionlib::SimpleActionServer<franka_gripper::GraspAction>
        grasp_action_server;
    /// @brief Action Server for "homing"
    actionlib::SimpleActionServer<franka_gripper::HomingAction>
        homing_action_server;
    /// @brief Callbackfunction for "homing"
    /// The SchunkGripper has no homing function, so an error message is printed
    bool homing(
        const franka_gripper::HomingGoalConstPtr& goal,
        ros::NodeHandle* node_handle,
        actionlib::SimpleActionServer<franka_gripper::HomingAction>* as_);
    /// @brief Callbackfunction for "grasp"
    /// Calls closeGripper and gives feedback about execution
    bool grasp(const franka_gripper::GraspGoalConstPtr& goal,
               ros::NodeHandle* node_handle,
               actionlib::SimpleActionServer<franka_gripper::GraspAction>* as_);
    /// @brief Callbackfunction for "move"
    /// Calls closeGripper if the action goal->width is set to zero
    /// If another width is send openGripper is called
    bool move(const franka_gripper::MoveGoalConstPtr& goal,
              ros::NodeHandle* node_handle,
              actionlib::SimpleActionServer<franka_gripper::MoveAction>* as_);
    /// @brief Callbackfunction for "stop"
    /// Sets both IO signals of the SchunkGripper to zero.
    bool stop(const franka_gripper::StopGoalConstPtr& goal,
              ros::NodeHandle* node_handle,
              actionlib::SimpleActionServer<franka_gripper::StopAction>* as_);
    /// @brief closes the gripper by calling rosservice
    bool closeGripper(ros::NodeHandle* node_handle);
    /// @brief opens the gripper by calling rosservice
    bool openGripper(ros::NodeHandle* node_handle);
    /// @}

    /// @brief A the callback function of the error recovrey action server, simply sets succeeded
    bool errorRecovery(const franka_msgs::ErrorRecoveryGoalConstPtr& goal);

    /// @brief A simple action server to acknowledge errors that occured during program execution
    actionlib::SimpleActionServer<franka_msgs::ErrorRecoveryAction>
        m_errorRecoveryServer;

    /// @name Subscriber and callback functions for UR-States
    /// @{
    /// @brief Subscriber for "programState"
    ros::Subscriber m_programStateSubscriber;
    /// @brief Subscriber for "robotMode"
    ros::Subscriber m_robotModeSubscriber;
    /// @brief Subscriber for "safetyMode"
    ros::Subscriber m_safetyModeSubscriber;
    /// @brief Subscriber for "jointState"
    ros::Subscriber m_jointStateSubscriber;
    /// @brief Subscriber for "tf"
    ros::Subscriber m_tfSubscriber;
    /// @brief Callbackfuntion for "programState"
    /// Saves msg in m_urProgramState and calls handleFrankaState
    void cb_programStateSubscriber(std_msgs::Bool msg);
    /// @brief Callbackfuntion for "robotMode"
    /// Saves msg in m_urRobotMode
    void cb_robotModeSubscriber(ur_dashboard_msgs::RobotMode msg);
    /// @brief Callbackfuntion for "safetyMode"
    /// Saves msg in m_urRobotMode
    void cb_safetyModeSubscriber(ur_dashboard_msgs::SafetyMode msg);
    /// @brief Callbackfuntion for "jointState"
    /// Writes the joints to m_frankaState.q and calls publishFrankaState
    void cb_jointStateSubscriber(sensor_msgs::JointState msg);
    /// @brief Callbackfuntion for "tf"
    /// Transforms the Task Space position from qauternion to matrix and
    /// writes it to m_frankaState.O_T_EE and calls publishFrankaState
    void cb_tfSubscriber(tf2_msgs::TFMessage msg);
    /// @}

    /// @name Membervariables containing the state messages of UR
    /// @{
    /// @brief holds information from "programState"-Message
    std_msgs::Bool m_urProgramState;
    /// @brief holds information from "RobotMode"-Message
    ur_dashboard_msgs::RobotMode m_urRobotMode;
    /// @brief holds information from "SafetyMode"-Message
    ur_dashboard_msgs::SafetyMode m_urSafetyMode;
    /// @}
    /// @}

   public:
    /// @brief Constructor
    /// starts Subscriber, Publisher and ActionServer
    RFUrAdapter();

    /// @brief Destructor
    /// stops started ActionServer
    ~RFUrAdapter();
};
}  // namespace rf
#endif 
