#include <actionlib/client/simple_action_client.h>
#include <actionlib/client/terminal_state.h>
#include <ros/ros.h>
#include <station_dummys/EolAction.h>
#include <station_dummys/LaserAction.h>

// ###########################################
// callbacks for the eol station
// ###########################################
void eolResultCB(const actionlib::SimpleClientGoalState &state,
                 const station_dummys::EolResultConstPtr &result,
                 int *result_ptr) {
  *result_ptr = result->eol_test_succeeded;
  //   ROS_INFO("Result CB called");
}

void eolActiveCB() {}

void eolFeedbackCB(const station_dummys::EolFeedbackConstPtr &feedback) {
  //   std::cout << "feedback from EOL service is that " <<
  //   feedback->time_remaining
  //             << " s are remaining" << std::endl;
}

// ###########################################
// callbacks for the laser station
// ###########################################
void laserResultCB(const actionlib::SimpleClientGoalState &state,
                   const station_dummys::LaserResultConstPtr &result,
                   int *result_ptr) {
  *result_ptr = result->laser_succeeded;
  //   ROS_INFO("Result CB called");
}

void laserActiveCB() {}

void laserFeedbackCB(const station_dummys::LaserFeedbackConstPtr &feedback) {
  //   std::cout << "feedback from Laser service is that " <<
  //   feedback->time_remaining
  //             << " s are remaining" << std::endl;
}

// ###########################################
// main
// ###########################################

int main(int argc, char **argv) {
  ros::init(argc, argv, "template_client");

  // ###########################################
  // initialization of the action clients;
  // ###########################################

  // create the action clients true causes the client to spin its
  // own thread
  actionlib::SimpleActionClient<station_dummys::EolAction> ac_eol(
      "eol_test_station", true);

  actionlib::SimpleActionClient<station_dummys::LaserAction> ac_laser(
      "laser_station", true);

  ROS_INFO("Waiting for eol action server to start.");
  // wait for the action server to start
  ac_eol.waitForServer(ros::Duration(10.0));  // will wait for 10 s

  ROS_INFO("Waiting for laser action server to start.");
  // wait for the action server to start
  ac_laser.waitForServer(ros::Duration(10.0));  // will wait for 10 s

  // ###########################################
  //   SEND GOAL TO EOL
  //   SIMPLE EOL: How to send a goal and wait for it
  // ###########################################

  ROS_INFO("Action server started, sending goal to eol.");

  // send a goal to the eol action server to start
  station_dummys::EolGoal goal_eol;
  goal_eol.new_esc_loaded = true;
  ac_eol.sendGoal(goal_eol);

  // Waiting for result
  // wait for the action to return
  bool finished_before_timeout = ac_eol.waitForResult(ros::Duration(30.0));
  //   this will return when the server returns a result
  //   or if theac_eol timeout has expired

  if (finished_before_timeout == true) {
    std::cout << " Eol service finished and returned "
              << ac_eol.getResult()->eol_test_succeeded << std::endl;
  } else {
    ROS_ERROR(" Eol service did not finish whithin timeout!");
  }

  // ###########################################
  //   ADVANCED EOL: How to send a goal and use the result and feedback via
  //   callbacks
  //   Comment in and out only the simple or advnaced version!
  // ###########################################
  //   ROS_INFO("Action server started, sending goal to eol.");
  //   station_dummys::EolGoal goal_eol;
  //   goal_eol.new_esc_loaded = true;
  //   int result = 0;

  //   ac_eol.sendGoal(goal_eol, boost::bind(&eolResultCB, _1, _2, &result),
  //                   boost::bind(&eolActiveCB), boost::bind(&eolFeedbackCB,
  //                   _1));

  //   // do whatever you want...
  //   //    the callback and the action server runs in the background!

  //   //   checking if server finished
  //   //   if the server finished the result is > 0
  //   ros::Duration d(1.0);
  //   while (ros::ok() && result == 0) {
  //     ROS_INFO("EOL Server is not finished yet .... waiting");
  //     d.sleep();
  //   }
  //   ROS_INFO("EOL Server succeeded with callback! answer is %i: ", result);

  // ###########################################
  //   SEND GOAL to Laser
  //   SIMPLE Laser: How to send a goal and wait for it
  //   Comment in and out only the simple or advnaced version!
  // ###########################################

  ROS_INFO("Action server started, sending goal to laser.");

  //   send a goal to the eol action server to start
  station_dummys::LaserGoal laser_goal;
  laser_goal.new_esc_loaded = true;
  ac_laser.sendGoal(laser_goal);

  // Waiting for result
  // wait for the action to return
  bool finished_before_timeout_laser =
      ac_laser.waitForResult(ros::Duration(30.0));
  //   this will return when the server returns a result
  //   or if theac_eol timeout has expired

  if (finished_before_timeout_laser == true) {
    std::cout << " Laser service finished and returned "
              << ac_laser.getResult()->laser_succeeded << std::endl;
  } else {
    ROS_ERROR(" Laser service did not finish whithin timeout!");
  }

  // ###########################################
  //   ADVANCED Laser: How to send a goal and use the result and feedback via
  //   callbacks
  //   Comment in and out only the simple or advnaced version!
  // ###########################################
  //   ROS_INFO("Action server started, sending goal to laser.");
  //   station_dummys::LaserGoal laser_goal;
  //   laser_goal.new_esc_loaded = true;
  //   int result_laser = 0;

  //   ac_laser.sendGoal(
  //       laser_goal, boost::bind(&laserResultCB, _1, _2, &result_laser),
  //       boost::bind(&laserActiveCB), boost::bind(&laserFeedbackCB, _1));

  //   // do whatever you want...
  //   //    the callback and the action server runs in the background!

  //   //   checking if server finished
  //   //   if the server finished the result is > 0
  //   ros::Duration d(1.0);
  //   while (ros::ok() && result_laser == 0) {
  //     ROS_INFO("Laser Server is not finished yet .... waiting");
  //     d.sleep();
  //   }
  //   ROS_INFO("Laser Server succeeded with callback! answer is %i: ",
  //            result_laser);

  // exit
  return 0;
}
