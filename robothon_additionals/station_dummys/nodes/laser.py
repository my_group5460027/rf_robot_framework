#!/usr/bin/env python

from __future__ import print_function

import rospy
import actionlib
import time
from  station_dummys.msg import LaserFeedback
from  station_dummys.msg import LaserResult
from  station_dummys.msg import LaserAction
import random

class LaserTest(object):
    # create messages that are used to publish feedback/result
    _feedback = LaserFeedback()
    _result = LaserResult()

    def __init__(self, name):
        self._action_name = name
        self._as = actionlib.SimpleActionServer(
            self._action_name, LaserAction, execute_cb=self.execute_cb, auto_start=False)
        self._as.start()

    def execute_cb(self, goal):
        # helper variables
        r = rospy.Rate(1)
        success = 2
        i = 0

        while(i<10): 
            i = i+1
            self._feedback.time_remaining = 10-i
            self._as.publish_feedback(self._feedback)
            r.sleep()


        self._result.laser_succeeded = success
        self._as.set_succeeded(self._result)


if __name__ == '__main__':
    rospy.init_node('laser')
    server = LaserTest(rospy.get_name())
    rospy.spin()
