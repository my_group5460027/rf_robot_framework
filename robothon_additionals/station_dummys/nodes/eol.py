#!/usr/bin/env python

from __future__ import print_function

import rospy
import actionlib
import time
from  station_dummys.msg import EolFeedback
from  station_dummys.msg import EolResult
from  station_dummys.msg import EolAction



import random

class EolTest(object):
    # create messages that are used to publish feedback/result
    _feedback = EolFeedback()
    _result = EolResult()

    def __init__(self, name):
        self._action_name = name
        self._as = actionlib.SimpleActionServer(
            self._action_name, EolAction, execute_cb=self.execute_cb, auto_start=False)
        self._as.start()

    def execute_cb(self, goal):
        # helper variables
        r = rospy.Rate(1)
        success = 2
        i = 0

        while(i<14): 
            i = i+1
            self._feedback.time_remaining = 14-i
            self._as.publish_feedback(self._feedback)
            r.sleep()


        n = random.randint(0,10)

        if(n<=2):
            success = 1

        self._result.eol_test_succeeded = success
        self._as.set_succeeded(self._result)


if __name__ == '__main__':
    rospy.init_node('eol_test')
    server = EolTest(rospy.get_name())
    rospy.spin()
