# General Information 

This package contains two Action Server Nodes for simulating the stations of the work environment in the wabco lab.

One is the laser station, where something is lasered into the ESC
THe other one is the EOL Test station in which the ESCs are tested.


The EOL station gives back that 20% of the ESCs are not correct, after 14s are passed.
The Laser Station always returns true after 10s 

The action_services return 1 if the action succeed but was not succesfull; and 2 if the action has succeeded and the action was successful.

On the feedback topic, the remaining time is given to you; You can use that to schedule better.


# Start UP
You have to compile the package. Then you can launch it with: `roslaunch station_dummys stations.launch` . This launches both services so that they can be accessed via a ROS interface. Type `rqt_graph` in another terminal to make ourself familiar with the structure.

# Usage in your Code

You can find an example of how to write an action client for the two servers in src/template.cpp. You should only need to copy paste that into your robot program. Then you can alter the callbacks for the result and the feedback to your needs.

Beware that you might need to alter also your cmake list and package.xml of your robot program. 

If you want to know more about action servers look at:

https://docs.ros.org/en/diamondback/api/actionlib/html/classactionlib_1_1SimpleActionClient.html#a0b00afd6df9d8c7bb1d02361ef8cf973

http://wiki.ros.org/actionlib_tutorials 


