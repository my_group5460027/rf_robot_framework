# QR tracker 

This package contains a launch file and param file to launch a node, and a camera to detect and localize QR codes. It is based on the [vision_visp](http://wiki.ros.org/vision_visp) precisely the [visp_auto_tracker](http://wiki.ros.org/visp_auto_tracker) package. 

For using you need to install [visp_auto_tracker](http://wiki.ros.org/visp_auto_tracker) and [realsense_ros](https://github.com/IntelRealSense/realsense-ros) which are the drivers for the camera.

To launch the node simply build this package within you catkin_ws .Then type: `roslaunch qr_tracker camera_and_tracker.launch `


Feel free to change the params of the camera in the launch file and the params of the QR tracker in the `tracker_models/pattern.cfg` file!