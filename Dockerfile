# This Dockerfile build a container based on ubuntu 20.04 with ros noetic.
# It also installes the drivers for the panda and the ur3 robot as well as coppelia sim.
FROM ubuntu:focal

# Suppress an apt-key warning about standard out not being a terminal. Use in this script is safe.
ENV APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE=DontWarn
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections

# Setup timezone
RUN echo 'Etc/UTC' > /etc/timezone && \
    ln -s /usr/share/zoneinfo/Etc/UTC /etc/localtime && \
    apt-get update && \
    apt-get install -q -y --no-install-recommends tzdata && \
    rm -rf /var/lib/apt/lists/*

# Setup environment
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8
ENV ROS_DISTRO noetic
RUN apt-get update && apt-get install -q -y --no-install-recommends \
    dirmngr \
    gnupg2 \
    apt-utils \
    && rm -rf /var/lib/apt/lists/*
RUN apt-get update
RUN apt-get install keyboard-configuration -y

# Install ros noetic
RUN apt-get install -y curl
RUN sh -c 'echo "deb http://packages.ros.org/ros/ubuntu focal main" > /etc/apt/sources.list.d/ros-latest.list'
RUN curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | apt-key add -
RUN apt-get update
RUN apt-get install -y ros-noetic-desktop-full
RUN apt-get install -y python3-rosdep python3-rosinstall python3-rosinstall-generator python3-wstool build-essential 
RUN rosdep init
RUN echo 'source /opt/ros/noetic/setup.bash' >> ~/.bashrc
RUN rosdep update

# Install libfranka and franka-ros
RUN apt-get update
RUN apt-get install -y ros-noetic-franka-ros ros-noetic-libfranka

#Add the robothon workspace
#ADD . /root/catkin_ws/src/

# Install dependencies
# RUN /bin/bash -c ' cd ~/catkin_ws/ && \
#     rosdep install --from-paths src --ignore-src -r -y'
RUN apt-get install -y python3-catkin-pkg python3-rospkg python3-catkin-tools ros-noetic-realsense2-camera libjsonrpccpp-dev libjsonrpccpp-tools \
    ros-noetic-rqt ros-noetic-rqt-common-plugins mesa-utils libgl1-mesa-glx wget ros-noetic-visp-auto-tracker ros-noetic-ros-controllers


# Install ur3 drivers
RUN /bin/bash -c 'source /opt/ros/noetic/setup.bash && \
    mkdir -p /root/ur_ws/src && cd root/ur_ws && \
    git clone https://github.com/UniversalRobots/Universal_Robots_ROS_Driver.git src/Universal_Robots_ROS_Driver && \
    git clone -b calibration_devel https://github.com/fmauch/universal_robot.git src/fmauch_universal_robot && \
    sudo apt-get update -qq && \
    rosdep update && \
    rosdep install --from-paths src --ignore-src -y && \
    catkin build -DCMAKE_BUILD_TYPE=Release'
RUN echo 'source /root/ur_ws/devel/setup.bash' >> ~/.bashrc

# Install coppelia sim
RUN wget https://www.coppeliarobotics.com/files/CoppeliaSim_Edu_V4_3_0_rev12_Ubuntu20_04.tar.xz -P /root/tmp
RUN /bin/bash -c 'mkdir /root/coppelia_sim && \
    cd /root/coppelia_sim && \
    tar -xvf /root/tmp/CoppeliaSim_Edu_V4_3_0_rev12_Ubuntu20_04.tar.xz --strip-components=1 && \
    rm -rf /root/tmp'

# Install b0 library
RUN /bin/bash -c 'cp /root/coppelia_sim/libb0.so /usr/local/lib/libb0.so && \
    ldconfig -n -v /usr/local/lib'


RUN echo 'source /root/catkin_ws/devel/setup.bash' >> ~/.bashrc

# Create aliases for catkin build with debug infos and release version
RUN echo "alias debug='catkin build -DCMAKE_BUILD_TYPE=Debug'" >> ~/.bashrc
RUN echo "alias release='catkin build -DCMAKE_BUILD_TYPE=Release'" >> ~/.bashrc

# Install useful packages
RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install -y vim nano inetutils-ping usbutils iproute2 gdb clang-format htop doxygen

ADD container_preparation.sh /root/container_preparation.sh
RUN chmod +x /root/container_preparation.sh

#Install mogen_p2p library and build the robothon workspace
CMD /root/container_preparation.sh

WORKDIR /root/catkin_ws


######################################################################
# # Install libfranka from source
# RUN apt-get update
# RUN apt-get install -y build-essential cmake git libpoco-dev libeigen3-dev 

# RUN cd ~ && \
#     git clone --recursive https://github.com/frankaemika/libfranka && \
#     cd libfranka && \
#     git checkout 0.8.0 && \
#     git submodule update && \
#     mkdir build && \
#     cd build && \
#     cmake -DCMAKE_BUILD_TYPE=Release .. && \
#     cmake --build . -- -j 32

# # Install franka-ros from source
# RUN /bin/bash -c 'source /opt/ros/noetic/setup.bash && \
#     mkdir -p ~/franka_ros/catkin_ws/src && \
#     cd ~/franka_ros/catkin_ws && \
#     catkin_init_workspace src && \
#     git clone --recursive https://github.com/frankaemika/franka_ros src/franka_ros && \
#     cd src/franka_ros && \
#     git checkout 0.8.0 && \
#     cd ~/franka_ros/catkin_ws && \
#     rosdep install --from-paths src --ignore-src --rosdistro noetic -y --skip-keys libfranka && \
#     catkin_make -DCMAKE_BUILD_TYPE=Release -DFranka_DIR:PATH=/root/libfranka/build'

# # Source the franka_ros workspace
# RUN echo 'source /root/franka_ros/catkin_ws/devel/setup.bash' >> ~/.bashrc
