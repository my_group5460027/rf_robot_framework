/// @file rf_ptp.cpp
///
/// @author Dwayne Steinke
/// @date 21.10.19
///
/// @brief Class implementation of RFPTP
///
/// rf_ptp.cpp
/// rf_robot_control
///
/// Created by Dwayne Steinke on 21.10.19 for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.
///
///

#include "rf_robot_control/rf_ptp.h"

namespace rf {

RFPTP::RFPTP(ros::NodeHandle& nodeHandle)
    : RFCommand(nodeHandle),
      m_pathPrefix(RF_POINTS_PATH_PREFIX),
      m_client(RF_PTP_ACTION_NAME, false),
      m_controller(nodeHandle, RF_PTP_MOTION_GENERATOR_NAME) {
    // std::vector<std::string> jointNames RF_JOINT_NAME_INITIALIZER;
    // m_goal.trajectory.joint_names = jointNames;
    m_executionStatusSubscriber = m_pNodeHandle->subscribe(
        "/rfExecutionStatus", 1, &RFPTP::cb_executionStatusUpdate, this);
    m_controlModePublisher = m_pNodeHandle->advertise<std_msgs::Int16>("/rfControlMode", 1);
}

RFPTP::~RFPTP() {}

void RFPTP::prepare() {
    // get jointNames from parameter server, allows toggle between active robots
    std::vector<std::string> jointNames;
    m_pNodeHandle->getParam("joint_names", jointNames);
    m_goal.trajectory.joint_names = jointNames;

    if (m_point.empty()) {
        ROS_ERROR_STREAM(
            "RFPTP::execute() - m_point is empty. It did not load. Abbort "
            "executing move command.");
        throw std::runtime_error("Preperation failed - no m_point set");
    }

    double globalOverride = 0.1;
    if (!m_pNodeHandle->param(RF_GLOBAL_OVERRIDE_NAME, globalOverride, 0.1)) {
        ROS_WARN_STREAM(
            "RFPTP::prepare - Could not fetch global override from rosparam. "
            "Setting to default value 0.1");
    }

    m_goal.trajectory.points.clear();

    TrajectoryPoint mypoint = constructGoalPoint();

    if (globalOverride < 0.01) {
        globalOverride = 0.1;
        ROS_WARN_STREAM(
            "RFPTP::prepare - global override is to small. Setting to 0.1.");
    }

    mypoint.time_from_start.sec = mypoint.time_from_start.sec / globalOverride;

    m_goal.trajectory.points.push_back(mypoint);

    m_isPrepared = true;
}

void RFPTP::execute() {
    if (!m_isPrepared) {
        try {
            prepare();
        } catch (const std::runtime_error& e) {
            throw;
        }
    }

    try {
        m_controllerDidStartHere = m_controller.startController();
    } catch (const std::runtime_error& e) {
        throw;
    }
    std_msgs::Int16 mode;
    mode.data = 0;
    m_controlModePublisher.publish(mode);
    pursueGoal();

    if (m_controllerDidStartHere) {
        try {
            m_controller.stopController();
        } catch (const std::runtime_error& e) {
            throw;
        }
    }
}

void RFPTP::setPoint(std::string name) {
    m_isPrepared = false;
    m_pointName = name;

    std::string jointSpaceName = m_pathPrefix;
    jointSpaceName.append(m_pointName);
    jointSpaceName.append("/jointspace");

    bool succesfullGet = m_pNodeHandle->getParam(jointSpaceName, m_point);

    if (!succesfullGet) {
        ROS_ERROR_STREAM("RFPTP::toPoint - Point on rosparam with path "
                         << jointSpaceName << " not found.");
        throw std::runtime_error("Could not get point from rosparam");
    }
}

void RFPTP::setDuration(double sec) {
    if (sec < 0.1 || sec > 60) {
        ROS_ERROR_STREAM(
            "RFPTP::setDuration -  New Value out of bounds. Aborting set");
        return;
    }
    m_duration_sec = sec;
}

trajectory_msgs::JointTrajectoryPoint RFPTP::constructGoalPoint() {
    std::vector<double> zeros(m_point.size(), 0);

    trajectory_msgs::JointTrajectoryPoint mypoint;
    mypoint.positions = m_point;
    mypoint.velocities = zeros;
    mypoint.accelerations = zeros;
    mypoint.time_from_start.sec = m_duration_sec;

    return mypoint;
}

void RFPTP::moveToPoint(std::string name, double inSec) {
    setPoint(name);
    setDuration(inSec);
    execute();
}

void RFPTP::moveToPoint(std::string namePrefix, int number, double inSec) {
    namePrefix.append(std::to_string(number));
    moveToPoint(namePrefix, inSec);
}

void RFPTP::pursueGoal() {
    m_client.waitForServer();
    m_client.sendGoal(m_goal);
    bool goalSend = true;
    bool goalCanceled = false;

    ros::Rate r(100);  // Hz;

    while (!m_client.getState().isDone() || !goalSend) {
        if (m_executionStatus == 0) {
            if (!goalSend) {
                m_client.sendGoal(m_goal);
                goalSend = true;
                goalCanceled = false;
            }
        } else if (m_executionStatus == 1) {
            if (!goalCanceled) {
                m_client.cancelAllGoals();
                // Use this approach for pausing:
                // m_client.waitForResult();
                // m_errorRecoveryClient.waitForServer();
                // franka_control::ErrorRecoveryGoal recoveryGoal;
                // m_errorRecoveryClient.sendGoal(recoveryGoal);
                // goalSend = false;
                // goalCanceled = true;
                if (m_controllerDidStartHere) {
                    m_controller.stopController();
                }
                throw std::runtime_error("UserStopError");
                break;
            }
        } else if (m_executionStatus == 2) {
            m_client.cancelAllGoals();
            if (m_controllerDidStartHere) {
                m_controller.stopController();
            }
            throw std::runtime_error("ReflexError");
            break;
        } else {
            m_client.cancelAllGoals();
            if (m_controllerDidStartHere) {
                m_controller.stopController();
            }
            throw std::runtime_error("FatalError");
        }

        r.sleep();
    }
}

void RFPTP::cb_executionStatusUpdate(rf_robot_control::RFExecutionStatus msg) {
    m_executionStatus = msg.status;
}

}  // namespace rf
