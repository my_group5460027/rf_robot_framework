/// @file rf_robot_control_service_presenter.cpp
///
/// @author Dwayne Steinke
/// @date 30.01.20
///
/// @brief
///
/// rf_robot_control_service_presenter.cpp
/// rf_robot_control
///
/// Created by Dwayne Steinke on 30.01.20 for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.
///
///

#include "rf_robot_control/rf_robot_control_service_presenter.h"

namespace rf {

// ==========================
// MARK: Constructors

RFRobotControlServicePresenter::RFRobotControlServicePresenter(
    RFRobotControl *robotControl)
    : m_pRobotControl(robotControl) {
    m_pNodeHandle = m_pRobotControl->getNodeHandle();

    connectThisToController();

    connectControllerToThis();

    advertiseServices();
}

// ==========================
// MARK: - Connecting Functions

void RFRobotControlServicePresenter::connectThisToController() {
    connect(this, &RFRobotControlServicePresenter::sgn_savePoint,
            m_pRobotControl, &RFRobotControl::slot_savePoint);

    connect(this, &RFRobotControlServicePresenter::sgn_moveToPoint,
            m_pRobotControl, &RFRobotControl::slot_moveToPoint);

    connect(this, &RFRobotControlServicePresenter::sgn_runProgram,
            m_pRobotControl, &RFRobotControl::slot_runProgram);

    connect(this, &RFRobotControlServicePresenter::sgn_acknowledgeErrors,
            m_pRobotControl, &RFRobotControl::slot_acknowledgeErrors);

    connect(this, &RFRobotControlServicePresenter::sgn_toggleGripper,
            m_pRobotControl, &RFRobotControl::slot_toggleGripper);

    connect(this, &RFRobotControlServicePresenter::sgn_setGlobalOverride,
            m_pRobotControl, &RFRobotControl::slot_setGlobalOverride);
}

void RFRobotControlServicePresenter::connectControllerToThis() {}

void RFRobotControlServicePresenter::advertiseServices() {
    m_savePointServiceServer = m_pNodeHandle->advertiseService(
        "savePoint", &RFRobotControlServicePresenter::cb_savePoint, this);

    m_moveToPointServiceServer = m_pNodeHandle->advertiseService(
        "moveToPoint", &RFRobotControlServicePresenter::cb_moveToPoint, this);

    m_runProgramServiceServer = m_pNodeHandle->advertiseService(
        "runProgram", &RFRobotControlServicePresenter::cb_runProgram, this);

    m_acknowledgeErrorsServiceServer = m_pNodeHandle->advertiseService(
        "acknowledgeErrors",
        &RFRobotControlServicePresenter::cb_acknowledgeErrors, this);

    m_toggleGripperServiceServer = m_pNodeHandle->advertiseService(
        "toggleGripper", &RFRobotControlServicePresenter::cb_toggleGripper,
        this);

    m_setGlobalOverrideServiceServer = m_pNodeHandle->advertiseService(
        "setGlobalOverride",
        &RFRobotControlServicePresenter::cb_setGlobalOverride, this);
}

bool RFRobotControlServicePresenter::cb_savePoint(
    rf_robot_control::SavePoint::Request &req,
    rf_robot_control::SavePoint::Response &res) {
    emit sgn_savePoint(req.name);
    return true;
}

bool RFRobotControlServicePresenter::cb_moveToPoint(
    rf_robot_control::MoveToPoint::Request &req,
    rf_robot_control::MoveToPoint::Response &res) {
    emit sgn_moveToPoint(req.name);
    return true;
}

bool RFRobotControlServicePresenter::cb_runProgram(
    rf_robot_control::RunProgram::Request &req,
    rf_robot_control::RunProgram::Response &res) {
    emit sgn_runProgram(req.number);
    return true;
}

bool RFRobotControlServicePresenter::cb_acknowledgeErrors(
    std_srvs::Empty::Request &req, std_srvs::Empty::Response &res) {
    emit sgn_acknowledgeErrors();
    return true;
}

bool RFRobotControlServicePresenter::cb_toggleGripper(
    std_srvs::Empty::Request &req, std_srvs::Empty::Response &res) {
    emit sgn_toggleGripper();
    return true;
}

bool RFRobotControlServicePresenter::cb_setGlobalOverride(
    rf_robot_control::SetGlobalOverride::Request &req,
    rf_robot_control::SetGlobalOverride::Response &res) {
    double newOverride = (double)req.newValue;
    newOverride = newOverride / 100.0;
    emit sgn_setGlobalOverride(newOverride);

    return true;
}

// ==========================
// MARK: - Public Slots

void RFRobotControlServicePresenter::slot_teachButtonClicked(
    std::string pointName) {
    emit sgn_savePoint(pointName);
}

}  // namespace rf
