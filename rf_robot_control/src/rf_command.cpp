/// @file rf_command.cpp
///
/// @author Dwayne Steinke
/// @date 21.10.19
///
/// @brief Class implementation of RFCommand
///
/// rf_command.cpp
/// rf_robot_control
///
/// Created by Dwayne Steinke on 21.10.19 for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.
///
///

#include "rf_robot_control/rf_command.h"

namespace rf {

RFCommand::RFCommand(ros::NodeHandle &nodeHandle)
    : m_pNodeHandle(&nodeHandle) {}

RFCommand::~RFCommand() {}

void RFCommand::prepare() { m_isPrepared = true; }

void RFCommand::reloadParams() {}

std::string RFCommand::getName(){
    return m_name;
}

}  // namespace rf
