/// @file rf_robot_control_presenter.cpp
///
/// @author Dwayne Steinke
/// @date 14.10.19
///
/// @brief
///
/// rf_robot_control_presenter.cpp
/// rf_robot_control
///
/// Created by Dwayne Steinke on 14.10.19 for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.
///
///

#include "rf_robot_control/rf_robot_control_presenter.h"

namespace rf {

// ==========================
// MARK: Constructors

RFRobotControlPresenter::RFRobotControlPresenter(MainWindow* window,
                                                 RFRobotControl* robotControl)
    : m_pWindow(window), m_pRobotControl(robotControl) {
    connectWindowToThis();

    connectThisToWindow();

    connectThisToController();

    connectControllerToThis();

    connectControllerToWindow();

    connectWindowToController();

    XmlRpc::XmlRpcValue list;

    ros::param::get("/points", list);

    for (auto it = list.begin(); it != list.end(); it++) {
        emit sgn_addPointName(it->first);
    }
}

// ==========================
// MARK: - Connecting Functions

void RFRobotControlPresenter::connectWindowToThis() {
    connect(m_pWindow, &MainWindow::sgn_teachButtonClicked, this,
            &RFRobotControlPresenter::slot_teachButtonClicked);

    connect(m_pWindow, &MainWindow::sgn_overrideSlider_valueDidChange, this,
            &RFRobotControlPresenter::slot_overrideSlider_valueChanged);
}

void RFRobotControlPresenter::connectThisToWindow() {
    connect(this, &RFRobotControlPresenter::sgn_appendNewStatus, m_pWindow,
            &MainWindow::slot_appendNewStatus);

    connect(this, &RFRobotControlPresenter::sgn_addPointName, m_pWindow,
            &MainWindow::slot_addPointNameBoxItem);
}

void RFRobotControlPresenter::connectThisToController() {
    connect(this, &RFRobotControlPresenter::sgn_savePoint, m_pRobotControl,
            &RFRobotControl::slot_savePoint);

    connect(this, &RFRobotControlPresenter::sgn_setGlobalOverride,
            m_pRobotControl, &RFRobotControl::slot_setGlobalOverride);
}

void RFRobotControlPresenter::connectControllerToThis() {
    connect(m_pRobotControl, &RFRobotControl::sgn_newStatus, this,
            &RFRobotControlPresenter::slot_newRobotStatusMessage);
}

void RFRobotControlPresenter::connectControllerToWindow() {
    connect(m_pRobotControl, &RFRobotControl::sgn_newRobotMode, m_pWindow,
            &MainWindow::slot_displayNewMode);

    connect(m_pRobotControl, &RFRobotControl::sgn_jointPositions, m_pWindow,
            &MainWindow::slot_displayJointPositions);

    connect(m_pRobotControl, &RFRobotControl::sgn_jointNames, m_pWindow,
            &MainWindow::slot_displayJointNames);

    connect(m_pRobotControl, &RFRobotControl::sgn_addPointName, m_pWindow,
            &MainWindow::slot_addPointNameBoxItem);

    connect(m_pRobotControl, &RFRobotControl::sgn_addProgramName, m_pWindow,
            &MainWindow::slot_addProgramNameBoxItem);

}

void RFRobotControlPresenter::connectWindowToController() {
    connect(m_pWindow, &MainWindow::sgn_runProgramButtonClicked,
            m_pRobotControl, &RFRobotControl::slot_runProgram);

    connect(m_pWindow, &MainWindow::sgn_acknowledgeErrors, m_pRobotControl,
            &RFRobotControl::slot_acknowledgeErrors);

    connect(m_pWindow, &MainWindow::sgn_toggleGripper, m_pRobotControl,
            &RFRobotControl::slot_toggleGripper);

    connect(m_pWindow, &MainWindow::sgn_moveToButtonClicked, m_pRobotControl,
            &RFRobotControl::slot_moveToPoint);
    
    connect(m_pWindow, &MainWindow::sgn_startSimulationButton_clicked, m_pRobotControl, &RFRobotControl::slot_startSimulation);

    connect(m_pWindow, &MainWindow::sgn_startSimulationButton_clicked,
            m_pRobotControl, &RFRobotControl::slot_startSimulation);

    connect(m_pWindow, &MainWindow::sgn_stopSimulationButton_clicked,
            m_pRobotControl, &RFRobotControl::slot_stopSimulation);

    connect(m_pWindow, &MainWindow::sgn_guidingModeButton_clicked,
            m_pRobotControl, &RFRobotControl::slot_toggleGuidingMode);

    connect(m_pWindow, &MainWindow::sgn_jointPositionSlider_valueChanged,
            m_pRobotControl, &RFRobotControl::slot_writeJointPosition);

    connect(m_pWindow, &MainWindow::sgn_deletePoint, m_pRobotControl,
            &RFRobotControl::slot_deletePoint);

    connect(m_pWindow, &MainWindow::sgn_updatePointsPath, m_pRobotControl, &RFRobotControl::slot_updatePointsPath);

    connect(m_pWindow, &MainWindow::sgn_savePointsToFile, m_pRobotControl, &RFRobotControl::slot_savePointsToFile);

}

// ==========================
// MARK: - Public Slots

void RFRobotControlPresenter::slot_teachButtonClicked(std::string pointName) {
    /// @todo Look up if window is in jointspace or taskspace mode.
    /// Currently only implemented for jointspace.
    emit sgn_savePoint(pointName);
}

void RFRobotControlPresenter::slot_newRobotStatusMessage(
    RFStatusMessageType type, std::string statusDescription) {
    std::string newMessage;
    switch (type) {
        case RFStatusMessageType::Info:
            newMessage = "[INFO]: ";
            break;
        case RFStatusMessageType::Debug:
            newMessage = "[Debug]: ";
            break;
        case RFStatusMessageType::Warning:
            newMessage = "[Warning]: ";
            break;
        case RFStatusMessageType::Error:
            newMessage = "[Error]: ";
            break;
        case RFStatusMessageType::Fatal:
            newMessage = "[Fatal]: ";
            break;
        default:
            newMessage =
                "[ERROR]: Message type not renderable. Basic Message: ";
    }
    newMessage.append(statusDescription);
    emit sgn_appendNewStatus(newMessage);
}

void RFRobotControlPresenter::slot_overrideSlider_valueChanged(int newValue) {
    double newOverride = (double)newValue;
    newOverride = newOverride / 100.0;
    emit sgn_setGlobalOverride(newOverride);
}

}  // namespace rf
