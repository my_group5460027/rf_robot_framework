/// @file rf_robot_control.cpp
///
/// @author Dwayne Steinke
/// @date 14.10.19
///
/// @brief
///
/// rf_robot_control.cpp
/// rf_robot_control
///
/// Created by Dwayne Steinke on 14.10.19 for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.
///
///

#include "rf_robot_control/rf_robot_control.h"

namespace rf {

// ==================================
// MARK: - Constructors & Init
// ==================================

RFRobotControl::RFRobotControl()
    : m_errorRecoveryClient("/franka_control/error_recovery", false),
      m_gripper(m_nodeHandle, true),
      m_ptp(m_nodeHandle) {
    m_commands.push_back(new RFTestProgram(m_nodeHandle));

    m_frankaStateSubscriber =
        m_nodeHandle.subscribe("/franka_state_controller/franka_states", 1,
                               &RFRobotControl::cb_frankaStateSubscriber, this);

    m_userMsgSubscriber = m_nodeHandle.subscribe(
        "userMsg", 100, &RFRobotControl::cb_userMsgSubscriber, this);

    m_executionStatusPublisher =
        m_nodeHandle.advertise<rf_robot_control::RFExecutionStatus>(
            "/rfExecutionStatus", 1);

    m_programStatePublisher =
        m_nodeHandle.advertise<std_msgs::Int16>("/rfProgramStatus", 1);

    m_simStatePublisher =
        m_nodeHandle.advertise<std_msgs::Int16>("/rfSimStatus", 1);

    m_guidingModePublisher =
        m_nodeHandle.advertise<std_msgs::Bool>("/rfGuidingMode", 1);

    m_guidingCommandPublisher =
        m_nodeHandle.advertise<sensor_msgs::JointState>("rfGuidingCommand", 1);

    initDictionaries();

    m_pointsPath =
        ros::package::getPath("rf_robot_control") + "/config/points.yaml";
    // checks if systemcalls are available
    if (!system(NULL)) {
        ROS_INFO_STREAM(
            "Command processor doesn't exist. No Parameters have been loaded");
    }
    // checks if parameter file exists
    if (access(m_pointsPath.c_str(), F_OK) == -1) {
        ROS_INFO_STREAM(
            "Parameter File doesn't exist. No Parameters have been loaded");
    }
    // loads the parameter file and saves them on the parameter server with
    // prefix "/points"
    else {
        system(("rosparam load " + m_pointsPath + " /points").c_str());
    }

    connect(&m_spinTimer, &QTimer::timeout, this, &RFRobotControl::spinOnce);
    m_spinTimer.start(1);

    connect(&m_programExecution, &QFutureWatcher<void>::finished, this,
            &RFRobotControl::slot_programFinished);

    connect(&m_programExecution, &QFutureWatcher<void>::canceled, this,
            &RFRobotControl::slot_programCanceled);

    // Set the global override to 0.1 at startup.
    m_nodeHandle.setParam(RF_GLOBAL_OVERRIDE_NAME, 0.1);

}

RFRobotControl::~RFRobotControl() {
    for (int i = 0; i < m_commands.size(); i++) {
        delete m_commands.at(i);
    }
    m_commands.clear();

    if (!m_pointsSaved){
        // checks if systemcalls are available
        if (!system(NULL)) {
            ROS_INFO_STREAM(
                "Command processor doesn't exist. No Parameters have been saved");
        }
        // checks if parameter file exists
        if (access(m_pointsPath.c_str(), F_OK) == -1) {
            ROS_INFO_STREAM(
                "Parameter File doesn't exist. No Parameters have been saved");
        }
        // saves the parameter in the namespace "/points" from the server into the
        // yaml file
        else {
            system(("rosparam dump " + m_pointsPath + " /points").c_str());
        }
    }
}

ros::NodeHandle* RFRobotControl::getNodeHandle() { return &m_nodeHandle; }

void RFRobotControl::initDictionaries() {
    m_robotModeDictionary[0] = "OTHER";
    m_robotModeDictionary[1] = "IDLE";
    m_robotModeDictionary[2] = "MOVE";
    m_robotModeDictionary[3] = "GUIDING";
    m_robotModeDictionary[4] = "REFLEX";
    m_robotModeDictionary[5] = "USER_STOPPED";
    m_robotModeDictionary[6] = "AUTOMATIC_ERROR_RECOVERY";
}

// ==================================
// MARK: - ROS Callbacks
// ==================================

void RFRobotControl::spinOnce() {
    ros::getGlobalCallbackQueue()->callAvailable();
    publishExecutionStatus();
}

void RFRobotControl::cb_frankaStateSubscriber(franka_msgs::FrankaState msg) {
    m_frankaState = msg;
    handleFrankaState();
}

void RFRobotControl::cb_userMsgSubscriber(
    rf_robot_control::RFRobotControlUserMsg msg) {
    RFStatusMessageType type = static_cast<RFStatusMessageType>(msg.type);
    emit sgn_newStatus(type, msg.message);
}

void RFRobotControl::slot_programFinished() {
    RF_INFO_STREAM("Program finished");
    m_programState.data = 0;
    m_programStatePublisher.publish(m_programState);
}

void RFRobotControl::slot_programCanceled() {
    RF_ERROR_STREAM("Program was cancelled");
    m_programState.data = 0;
    m_programStatePublisher.publish(m_programState);
}

// ==================================
// MARK: - Property Update Functions
// ==================================

void RFRobotControl::handleFrankaState() {
    updateFrankaRobotMode();
    updateJointPositions();
}

void RFRobotControl::updateJointPositions() {
    std::vector<double> jointPositions(m_frankaState.q.begin(),
                                       m_frankaState.q.end());
    for (int i = 0; i < jointPositions.size(); i++) {
        jointPositions[i] = round(jointPositions[i] * (180.0 / M_PI) * 1000);
    }
    emit sgn_jointPositions(jointPositions);
}



void RFRobotControl::updateFrankaRobotMode() {
    if (m_frankaState.robot_mode != m_robotMode) {
        m_robotMode = m_frankaState.robot_mode;
        // RF_INFO_STREAM("Changed Robot Mode to " <<
        // m_robotModeDictionary[m_robotMode]);

        std::vector<std::string> jointNames;
        m_nodeHandle.getParam("joint_names", jointNames);
        if (jointNames.size() < 7) {
            jointNames.push_back("NONE");
        }
        for (int i = 0; i<m_commands.size(); i++){
            emit sgn_addProgramName(i, m_commands[i]->getName());
        }
        emit sgn_jointNames(jointNames);
        emit sgn_newRobotMode(m_robotModeDictionary[m_robotMode]);
        updateExecutionStatus();
    }
}

void RFRobotControl::updateExecutionStatus() {
    if ((m_robotMode == m_frankaState.ROBOT_MODE_IDLE) ||
        (m_robotMode == m_frankaState.ROBOT_MODE_MOVE)) {
        if (m_errorAcknowledged) {
            m_executionStatus = 0;
        }
    } else if (m_robotMode == m_frankaState.ROBOT_MODE_USER_STOPPED) {
        m_executionStatus = 1;
        m_programState.data = 0;
        if (!m_programExecution.isFinished()) {
            m_errorAcknowledged = false;
            RF_ERROR_STREAM(
                "User stop pressed during program execution. Aborting Program. "
                "To Restart, please acknowledge this error.")
        }
    } else if (m_robotMode == m_frankaState.ROBOT_MODE_REFLEX) {
        m_executionStatus = 2;
        m_programState.data = 0;
        if (!m_programExecution.isFinished()) {
            m_errorAcknowledged = false;
            RF_ERROR_STREAM(
                "Reflex triggered during program execution. Aborting Program. "
                "To Restart, please acknowledge this error.")
        }
    } else if (m_robotMode ==
               m_frankaState.ROBOT_MODE_AUTOMATIC_ERROR_RECOVERY) {
    } else {
        m_programState.data = 0;
        m_executionStatus = 3;
        if (!m_programExecution.isFinished()) {
            m_errorAcknowledged = false;
            RF_ERROR_STREAM(
                "Error during program execution. Aborting Program. To Restart, "
                "please acknowledge this error.")
        }
    }
    publishExecutionStatus();
}

void RFRobotControl::publishExecutionStatus() {
    rf_robot_control::RFExecutionStatus msg;
    msg.status = m_executionStatus;
    m_executionStatusPublisher.publish(msg);
    m_programStatePublisher.publish(m_programState);
}

// ==================================
// MARK: - Public Slots
// ==================================

void RFRobotControl::slot_savePoint(string name) {
    // Construct parameter paths to save on
    string pointName = name;
    name.insert(0, "points/");
    string jointSpaceName = name;
    string taskSpaceName = name;

    jointSpaceName.append("/jointspace");
    taskSpaceName.append("/taskspace");

    // Subscribes to one message and then unsubscribes
    // This could be changed later if rf_robot_control
    // needs to subscribe to the FrankaState permanently.
    franka_msgs::FrankaStateConstPtr msg =
        ros::topic::waitForMessage<franka_msgs::FrankaState>(
            "/franka_state_controller/franka_states", m_nodeHandle,
            ros::Duration(5));

    // Check if message recieved or timed out after 5 seconds.
    if (msg == NULL) {
        RF_ERROR_STREAM("No state data from Franka recieved. Point "
                        << pointName << " not saved.");
        return;
    }

    // Get point data from message
    // Unfortunatly you have to "convert" from boost::array to
    // std::vector.
    std::vector<double> q;
    std::vector<double> O_T_EE;
    // std::vector<std::string> jointNames RF_JOINT_NAME_INITIALIZER;
    std::vector<std::string> jointNames;
    m_nodeHandle.getParam("joint_names", jointNames);
    q.assign((*msg).q.data(), (*msg).q.data() + jointNames.size());
    O_T_EE.assign((*msg).O_T_EE.data(), (*msg).O_T_EE.data() + 16);

    // Write to rosparam
    m_nodeHandle.setParam(jointSpaceName, q);
    m_nodeHandle.setParam(taskSpaceName, O_T_EE);
    emit sgn_addPointName(pointName);
    m_pointsSaved = false;
    RF_INFO_STREAM("Point " << pointName << " saved.")
}

void RFRobotControl::slot_runProgram(int programNumber) {
    m_programState.data = 0;
    if (programNumber < 0 || programNumber >= m_commands.size()) {
        RF_WARN_STREAM("Unknown program number."
                       << std::endl
                       << "New excecution terminated.");
        m_programStatePublisher.publish(m_programState);
        return;
    }

    if (m_programExecution.isFinished()) {
        m_programState.data = 1;
        RF_INFO_STREAM("Running Program");
        // RFTestProgram program(m_nodeHandle);
        QFuture<void> future = QtConcurrent::run(m_commands.at(programNumber),
                                                 &RFCommand::execute);
        m_programExecution.setFuture(future);
    } else {
        RF_WARN_STREAM("Another program is still running."
                       << std::endl
                       << "New excecution terminated.");
    }
    m_programStatePublisher.publish(m_programState);
}

void RFRobotControl::slot_acknowledgeErrors() {
    if (m_robotMode == m_frankaState.ROBOT_MODE_IDLE) {
        m_errorAcknowledged = true;
        m_errorRecoveryClient.waitForServer();
        franka_msgs::ErrorRecoveryGoal recoveryGoal;
        m_errorRecoveryClient.sendGoal(recoveryGoal);
        RF_INFO_STREAM("Error acknowledged");
        updateExecutionStatus();
    } else {
        RF_WARN_STREAM("Cant acknowledge Error: Only allowed in IDLE");
    }
}

void RFRobotControl::slot_toggleGripper() {
    if (m_robotMode == m_frankaState.ROBOT_MODE_IDLE) {
        if (m_programExecution.isFinished()) {
            if (m_gripperIsOpen) {
                m_gripper.close();
                m_gripperIsOpen = false;
                RF_INFO_STREAM("Gripper closed");
            } else {
                m_gripper.open();
                m_gripperIsOpen = true;
                RF_INFO_STREAM("Gripper opened");
            }
        } else {
            RF_WARN_STREAM("Another program is still running."
                           << std::endl
                           << "Gripper toggle terminated.");
        }
    } else {
        RF_WARN_STREAM("Cant toggle Gripper: Only allowed in IDLE");
    }
}

void RFRobotControl::slot_setGlobalOverride(double newValue) {
    if (newValue < 0.09 || newValue > 1.01) {
        RF_ERROR_STREAM(
            "RFRobotControl::slot_setGlobalOverride - newValue out of range. "
            "Aborting set.");
        return;
    }

    if (m_robotMode == m_frankaState.ROBOT_MODE_USER_STOPPED ||
        m_robotMode == m_frankaState.ROBOT_MODE_OTHER) {
        m_nodeHandle.setParam(RF_GLOBAL_OVERRIDE_NAME, newValue);
    } else {
        RF_WARN_STREAM(
            "Cant set global override: Only allowed in USER_STOPPED or OTHER");
    }
}

void RFRobotControl::slot_moveToPoint(string name) {
    m_programState.data = 0;

    if (m_robotMode == m_frankaState.ROBOT_MODE_IDLE) {
        try {
            m_ptp.setPoint(name);
        } catch (...) {
            ROS_ERROR_STREAM(
                "RFRobotControl::slot_moveToPoint - Could not set Point with "
                "name "
                << name << ". Aborting.");
            return;
        }
        m_ptp.setDuration(5);

        if (m_programExecution.isFinished()) {
            std::cout << "Running" << std::endl;
            m_programState.data = 1;
            RF_INFO_STREAM("Running Program");
            // RFTestProgram program(m_nodeHandle);
            QFuture<void> future = QtConcurrent::run(&m_ptp, &RFPTP::execute);
            m_programExecution.setFuture(future);
        } else {
            RF_WARN_STREAM("Another program is still running."
                           << std::endl
                           << "New excecution terminated.");
        }
    } else {
        RF_WARN_STREAM("Cant move to Point: Only allowed in IDLE");
    }

    m_programStatePublisher.publish(m_programState);
}

void RFRobotControl::slot_startSimulation() {
    m_simState.data = 1;
    m_simStatePublisher.publish(m_simState);
}

void RFRobotControl::slot_stopSimulation() {
    m_simState.data = 0;
    m_simStatePublisher.publish(m_simState);
}

void RFRobotControl::slot_toggleGuidingMode(bool checked) {
    std_msgs::Bool mode;
    mode.data = checked;
    m_guidingModePublisher.publish(mode);
}

void RFRobotControl::slot_writeJointPosition(
    std::vector<double> jointPositions) {
    for (int i = 0; i < jointPositions.size(); i++) {
        jointPositions[i] = round(jointPositions[i] * (M_PI / 180.0)) / 1000;
    }

    m_guidingCommand.position = jointPositions;
    m_guidingCommandPublisher.publish(m_guidingCommand);
}

void RFRobotControl::slot_deletePoint(std::string name){
    m_nodeHandle.deleteParam("/points/" + name);
    m_pointsSaved = false;
}

void RFRobotControl::slot_updatePointsPath(std::string path){
    
    if (!m_pointsSaved){
        system(("rosparam dump " + m_pointsPath + " /points").c_str());
    }
    
    m_nodeHandle.deleteParam("/points");
    m_pointsPath = path;
    system(("rosparam load " + m_pointsPath + " /points").c_str());

    m_pointsSaved = true;
}

void RFRobotControl::slot_savePointsToFile(std::string path){
    system(("rosparam dump " + path + " /points").c_str());
       m_pointsSaved = true;
}

}  // namespace rf
