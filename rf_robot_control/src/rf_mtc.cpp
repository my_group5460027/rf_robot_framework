/// @file rf_mtc.cpp
///
/// @author Tom Hattendorf
/// @date 08.10.2021
///
/// @brief Class implementation of RFMTC
///
/// rf_mtc.cpp
/// rf_robot_control
///
/// Created by Tom Hattendorf on 08.10.2021 for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.
///
///

#include "rf_robot_control/rf_mtc.h"

namespace rf {

RFMTC::RFMTC(ros::NodeHandle& nodeHandle)
    : RFCommand(nodeHandle),
      m_client(RF_MTC_ACTION_NAME, false),
      m_controller(nodeHandle, RF_MTC_MOTION_GENERATOR_NAME) {
    m_executionStatusSubscriber = m_pNodeHandle->subscribe(
        "/rfExecutionStatus", 1, &RFMTC::cb_executionStatusUpdate, this);
    m_controlModePublisher =
        m_pNodeHandle->advertise<std_msgs::Int16>("/rfControlMode", 1);
    // Set default values:
    resetToDefaultValues();
}

RFMTC::~RFMTC() {}

void RFMTC::prepare() {
    if (m_override.empty()) {
        ROS_ERROR_STREAM(
            "RFMTC::execute() - m_override is empty. It was not set. Abbort "
            "executing move command. Please contact maintainer.");
        throw std::runtime_error("Preparation failed - no m_override set");
    }

    double globalOverride = 0.1;
    if (!m_pNodeHandle->param(RF_GLOBAL_OVERRIDE_NAME, globalOverride, 0.1)) {
        ROS_WARN_STREAM(
            "RFMTC::prepare - Could not fetch global override from rosparam. "
            "Setting to default value 0.1");
    }

    m_goal.dX_max[0] = m_dX_max.at(0);
    m_goal.dX_max[1] = m_dX_max.at(1);

    m_goal.ddX_max[0] = m_ddX_max.at(0);
    m_goal.ddX_max[1] = m_ddX_max.at(1);

    m_goal.t_scale[0] = globalOverride * m_override.at(0);
    m_goal.t_scale[1] = globalOverride * m_override.at(1);
    m_isPrepared = true;
}

void RFMTC::execute() {
    if (!m_isPrepared) {
        try {
            prepare();
        } catch (const std::runtime_error& e) {
            throw;
        }
    }

    try {
        m_controllerDidStartHere = m_controller.startController();
    } catch (const std::runtime_error& e) {
        throw;
    }
    std_msgs::Int16 mode;
    mode.data = 1;
    m_controlModePublisher.publish(mode);
    pursueGoal();

    if (m_controllerDidStartHere) {
        try {
            m_controller.stopController();
        } catch (const std::runtime_error& e) {
            throw;
        }
    }
}

void RFMTC::moveToContact(double x, double y, double z, double contactForce, bool useEECoordinates) {
    m_goal.direction = {x, y, z};
    m_goal.contact_force = contactForce;
    m_goal.useEECoordinates = useEECoordinates;
    execute();
}

void RFMTC::resetToDefaultValues() {
     // Velocity in m/s and rad/s
    m_dX_max.clear();
    m_dX_max.push_back(0.05);
    m_dX_max.push_back(0.1);

    // Acceleration in m/s² and rad/s²
    m_ddX_max.clear();
    m_ddX_max.push_back(0.5);
    m_ddX_max.push_back(1);
    // Override factor for translation and rotation
    m_override.clear();
    m_override.push_back(1);
    m_override.push_back(1);
}

void RFMTC::setOverride(double translationOverride, double rotationOverride) {
    if (translationOverride < 0.0 || rotationOverride < 0.0) {
        ROS_ERROR_STREAM(
            "RFMTC::setOverride - Negativ overrides can not be handled. "
            "Aborting set.");
        return;
    }
    if (translationOverride > 1.5 || rotationOverride > 1.5) {
        ROS_ERROR_STREAM(
            "RFMTC::setOverride - Overrides larger than 1.5 can not be "
            "handled. Aborting set. Change your acceleration and speed "
            "instead.");
        return;
    }
    if (translationOverride < 0.01 || rotationOverride < 0.01) {
        ROS_WARN_STREAM(
            "RFMTC::setOverride - Overrides smaller than 0.01 are discouraged, "
            "because maybe no motion will be executed at all.");
    }
    if (translationOverride > 1.01 || rotationOverride > 1.01) {
        ROS_WARN_STREAM(
            "RFMTC::setOverride - Overrides larger than 1 are discouraged, "
            "because your motion may be to fast. Change your acceleration and "
            "speed instead.");
    }

    m_override.clear();
    m_override.push_back(translationOverride);
    m_override.push_back(rotationOverride);
    return;
}

void RFMTC::setOverride(double override) { setOverride(override, override); }

// ===============================
// MARK: Private Methods
// ===============================

void RFMTC::pursueGoal() {
    m_client.waitForServer();
    m_client.sendGoal(m_goal);
    bool goalSend = true;
    bool goalCanceled = false;

    ros::Rate r(100);  // Hz;
    while (!m_client.getState().isDone() || !goalSend) {
        if (m_executionStatus == 0) {
            if (!goalSend) {
                m_client.sendGoal(m_goal);
                goalSend = true;
                goalCanceled = false;
            }
        } else if (m_executionStatus == 1) {
            if (!goalCanceled) {
                m_client.cancelAllGoals();
                // Use this approach for pausing:
                // m_client.waitForResult();
                // m_errorRecoveryClient.waitForServer();
                // franka_control::ErrorRecoveryGoal recoveryGoal;
                // m_errorRecoveryClient.sendGoal(recoveryGoal);
                // goalSend = false;
                // goalCanceled = true;
                if (m_controllerDidStartHere) {
                    m_controller.stopController();
                }
                throw std::runtime_error("UserStopError");
                break;
            }
        } else if (m_executionStatus == 2) {
            m_client.cancelAllGoals();
            if (m_controllerDidStartHere) {
                m_controller.stopController();
            }
            throw std::runtime_error("ReflexError");
            break;
        } else {
            m_client.cancelAllGoals();
            if (m_controllerDidStartHere) {
                m_controller.stopController();
            }
            throw std::runtime_error("FatalError");
        }

        r.sleep();
    }
}
void RFMTC::cb_executionStatusUpdate(rf_robot_control::RFExecutionStatus msg) {
    m_executionStatus = msg.status;
}

}  // namespace rf
