/// @file rf_gripper.cpp
///
/// @author Dwayne Steinke
/// @date 15.11.19
///
/// @brief Class implemenatation of RFGripper
///
/// rf_gripper.cpp
/// rf_robot_control
///
/// Created by Dwayne Steinke on 15.11.19 for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.
///
///

#include "rf_robot_control/rf_gripper.h"

namespace rf {

RFGripper::RFGripper(ros::NodeHandle &nodeHandle, bool async)
    : RFCommand(nodeHandle),
      m_moveActionClient("/franka_gripper/move", false),
      m_homingActionClient("/franka_gripper/homing", false),
      m_graspActionClient("/franka_gripper/grasp", false),
      m_stopActionClient("/franka_gripper/stop", false),
      m_ASYNC(async) {}

RFGripper::~RFGripper() {}

void RFGripper::execute() {
    if (!m_isPrepared) {
        prepare();
    }
    // Hier ggf. etwas sinnvolles der unteren Befehle einbinden
    ROS_WARN_STREAM(
        "RFGripper::execute - No generic functionallity to execute. Doing "
        "nothing");
}

void RFGripper::move(double width, double speed) {
    franka_gripper::MoveGoal goal;
    goal.speed = speed;
    goal.width = width;

    m_moveActionClient.waitForServer();
    m_moveActionClient.sendGoal(goal);
    if (!m_ASYNC) {
        m_moveActionClient.waitForResult(ros::Duration(20.0));
        ROS_INFO_STREAM("Action Client State = "
                        << m_moveActionClient.getState().toString());
        franka_gripper::MoveResult result = *m_moveActionClient.getResult();
        if (!result.success) {
            throw std::runtime_error("RFGripper::move failed");
        }
    }
}

void RFGripper::homing() {
    franka_gripper::HomingGoal goal;

    m_homingActionClient.waitForServer();
    m_homingActionClient.sendGoal(goal);
    if (!m_ASYNC) {
        m_homingActionClient.waitForResult(ros::Duration(30.0));
        ROS_INFO_STREAM("Action Client State = "
                        << m_homingActionClient.getState().toString());
        franka_gripper::HomingResult result = *m_homingActionClient.getResult();
        if (!result.success) {
            throw std::runtime_error("RFGripper::homing failed");
        }
    }
}

void RFGripper::grasp(double width, double speed, double force,
                      double epsilon_inner, double epsilon_outer) {
    franka_gripper::GraspGoal goal;

    goal.width = width;
    goal.speed = speed;
    goal.force = force;
    goal.epsilon.inner = epsilon_inner;
    goal.epsilon.outer = epsilon_outer;

    m_graspActionClient.waitForServer();
    m_graspActionClient.sendGoal(goal);
    if (!m_ASYNC) {
        m_graspActionClient.waitForResult(ros::Duration(20.0));
        ROS_INFO_STREAM("Action Client State Grasp = "
                        << m_graspActionClient.getState().toString());
        franka_gripper::GraspResult result = *m_graspActionClient.getResult();
        if (!result.success) {
            throw std::runtime_error("RFGripper::grasp failed");
        }
    }
}

void RFGripper::stop() {
    franka_gripper::StopGoal goal;

    m_stopActionClient.waitForServer();
    m_stopActionClient.sendGoal(goal);
    if (!m_ASYNC) {
        m_stopActionClient.waitForResult(ros::Duration(20.0));
        ROS_INFO_STREAM("Action Client State = "
                        << m_stopActionClient.getState().toString());
        franka_gripper::StopResult result = *m_stopActionClient.getResult();
        if (!result.success) {
            throw std::runtime_error("RFGripper::stop failed");
        }
    }
}

void RFGripper::open(double speed) {
    if (speed < 0) {
        speed = m_DEFAULT_SPEED;
    }
    move(m_MAX_OPENING, speed);
}

void RFGripper::close(double force, double speed) {
    if (speed < 0) {
        speed = m_DEFAULT_SPEED;
    }
    if (force < 0) {
        force = m_DEFAULT_FORCE;
    }

    grasp(0, speed, force, 0, m_MAX_OPENING);
}

}  // namespace rf
