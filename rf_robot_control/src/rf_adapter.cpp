#include "rf_robot_control/rf_adapter.h"

namespace rf {

RFAdapter::RFAdapter()
    : stop_action_server(m_NodeHandle, "/franka_gripper/stop",
                         boost::bind(&rf::RFAdapter::stop, this, _1,
                                     &m_NodeHandle, &stop_action_server),
                         false),
      move_action_server(m_NodeHandle, "/franka_gripper/move",
                         boost::bind(&rf::RFAdapter::move, this, _1,
                                     &m_NodeHandle, &move_action_server),
                         false),
      grasp_action_server(m_NodeHandle, "/franka_gripper/grasp",
                          boost::bind(&rf::RFAdapter::grasp, this, _1,
                                      &m_NodeHandle, &grasp_action_server),
                          false),
      homing_action_server(m_NodeHandle, "/franka_gripper/homing",
                           boost::bind(&rf::RFAdapter::homing, this, _1,
                                       &m_NodeHandle, &homing_action_server),
                           false) {
    m_frankaStatePublisher = m_NodeHandle.advertise<franka_msgs::FrankaState>(
        "/franka_state_controller/franka_states", 1);

    stop_action_server.start();
    move_action_server.start();
    grasp_action_server.start();
    homing_action_server.start();
}

RFAdapter::~RFAdapter() {
    stop_action_server.shutdown();
    move_action_server.shutdown();
    grasp_action_server.shutdown();
    homing_action_server.shutdown();
}

void RFAdapter::handleFrankaState() { publishFrankaState(); }

void RFAdapter::publishFrankaState() {
    m_frankaStatePublisher.publish(m_frankaState);
}
bool RFAdapter::stop(
    const franka_gripper::StopGoalConstPtr& goal, ros::NodeHandle* node_handle,
    actionlib::SimpleActionServer<franka_gripper::StopAction>* as_) {}

bool RFAdapter::move(
    const franka_gripper::MoveGoalConstPtr& goal, ros::NodeHandle* node_handle,
    actionlib::SimpleActionServer<franka_gripper::MoveAction>* as_) {}

bool RFAdapter::grasp(
    const franka_gripper::GraspGoalConstPtr& goal, ros::NodeHandle* node_handle,
    actionlib::SimpleActionServer<franka_gripper::GraspAction>* as_) {}

bool RFAdapter::homing(
    const franka_gripper::HomingGoalConstPtr& /*goal*/,
    ros::NodeHandle* node_handle,
    actionlib::SimpleActionServer<franka_gripper::HomingAction>* as_) {}

}  // namespace rf
