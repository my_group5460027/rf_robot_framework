/// @file rf_testprogram.cpp
///
/// @author Tom Hattendorf
/// @date 27.10.21
///
/// @brief
///
/// rf_testprogram.cpp
/// rf_robot_control
///
/// Created by Tom Hattendorf on 27.10.21 for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.
///
///

#include "rf_robot_control/rf_testprogram.h"

namespace rf {

RFTestProgram::RFTestProgram(ros::NodeHandle &nodeHandle)
    : RFCommand(nodeHandle) {
		m_name = "TestProgram";
	}

RFTestProgram::~RFTestProgram() {}
void RFTestProgram::execute() {
	RFController ptpController(*m_pNodeHandle, RF_PTP_MOTION_GENERATOR_NAME);
    RFController linController(*m_pNodeHandle, RF_LIN_MOTION_GENERATOR_NAME);
    RFController mtcController(*m_pNodeHandle, RF_MTC_MOTION_GENERATOR_NAME);
    RFPTP ptp(*m_pNodeHandle);
    RFLIN lin(*m_pNodeHandle);
	RFMTC mtc(*m_pNodeHandle);
	RFGripper gripper(*m_pNodeHandle);

	ptp.moveToPoint("Home", 2);
	lin.setSpeed(0.5,1.5);

	// // Perform Joint Motion to previously taught points
	// for (size_t i = 1; i <= 2; i++) {
	// 	ptp.moveToPoint("P", i, 2);
	// }

	// // Perform a Gripper Motion
	// gripper.close();
	
	// lin.moveToPoint("P3");

	// ptp.moveToPoint("P4", 2);
	// lin.moveToPoint("P5");
	// gripper.open();

	// lin.moveToPoint("P4");
	// ptp.moveToPoint("P6", 2);

	// ptp.moveToPoint("Home", 2);

	// Move to a relative point in EE frame, stop immediately if a force limit is exceeded. (if base frame is desired, set flag to false)
	//mtc.moveToContact(0, 0, 0.05, 15.0); // In z direction
	//mtc.moveToContact(0.05, 0, 0.05, 15.0); // In x and z direction
	//mtc.moveToContact(0.05, 0.05, 0.05, 15.0); // In x,y,z direction

	//ros::Duration(1).sleep();

	// Move to a relative point in EE frame (if base frame is desired, set flag to false)
	lin.moveRelativeToPoint(-0.05, 0, 0);
	lin.moveRelativeToPoint(0, -0.05, 0);
	lin.moveRelativeToPoint(0, 0, -0.05);

	lin.moveRelativeToPoint(0.1, 0, 0, false);

}

}  // namespace rf
