/// @file rf_lin.cpp
///
/// @author Dwayne Steinke
/// @date 19.12.19
///
/// @brief Class implementation of RFLIN
///
/// rf_lin.cpp
/// rf_robot_control
///
/// Created by Dwayne Steinke on 19.12.19 for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.
///
///

#include "rf_robot_control/rf_lin.h"

namespace rf
{

    RFLIN::RFLIN(ros::NodeHandle &nodeHandle)
        : RFCommand(nodeHandle),
          m_pathPrefix(RF_POINTS_PATH_PREFIX),
          m_client(RF_LIN_ACTION_NAME, false),
          m_controller(nodeHandle, RF_LIN_MOTION_GENERATOR_NAME)
    {
        m_executionStatusSubscriber = m_pNodeHandle->subscribe(
            "/rfExecutionStatus", 1, &RFLIN::cb_executionStatusUpdate, this);
        m_controlModePublisher =
            m_pNodeHandle->advertise<std_msgs::Int16>("/rfControlMode", 1);
        // Set default values:
        resetToDefaultValues();
    }

    RFLIN::~RFLIN() {}

    void RFLIN::prepare()
    {
        if (m_point.empty())
        {
            ROS_ERROR_STREAM(
                "RFPTP::execute() - m_point is empty. It did not load. Abbort "
                "executing move command.");
            throw std::runtime_error("Preparation failed - no m_point set");
        }
        if (m_dX_max.empty())
        {
            ROS_ERROR_STREAM(
                "RFPTP::execute() - m_dX_max is empty. It was not set. Abbort "
                "executing move command. Please contact maintainer.");
            throw std::runtime_error("Preparation failed - no m_dX_max set");
        }
        if (m_ddX_max.empty())
        {
            ROS_ERROR_STREAM(
                "RFPTP::execute() - m_ddX_max is empty. It was not set. Abbort "
                "executing move command. Please contact maintainer.");
            throw std::runtime_error("Preparation failed - no m_ddX_max set");
        }
        if (m_override.empty())
        {
            ROS_ERROR_STREAM(
                "RFPTP::execute() - m_override is empty. It was not set. Abbort "
                "executing move command. Please contact maintainer.");
            throw std::runtime_error("Preparation failed - no m_override set");
        }

        double globalOverride = 0.1;
        if (!m_pNodeHandle->param(RF_GLOBAL_OVERRIDE_NAME, globalOverride, 0.1))
        {
            ROS_WARN_STREAM(
                "RFLIN::prepare - Could not fetch global override from rosparam. "
                "Setting to default value 0.1");
        }

        for (size_t i = 0; i < 16; i++)
        {
            m_goal.O_T_EE_d[i] = m_point.at(i);
            m_goal.referencePoint[i] = m_referencePoint.at(i);
        }

        m_goal.relative = m_pointIsRelative;
        m_goal.useEECordiantes = m_pointUsesEECoordinates;

        m_goal.dX_max[0] = m_dX_max.at(0);
        m_goal.dX_max[1] = m_dX_max.at(1);

        m_goal.ddX_max[0] = m_ddX_max.at(0);
        m_goal.ddX_max[1] = m_ddX_max.at(1);

        m_goal.t_scale[0] = globalOverride * m_override.at(0);
        m_goal.t_scale[1] = globalOverride * m_override.at(1);

        m_isPrepared = true;
    }

    void RFLIN::execute()
    {
        if (!m_isPrepared)
        {
            try
            {
                prepare();
            }
            catch (const std::runtime_error &e)
            {
                throw;
            }
        }

        try
        {
            m_controllerDidStartHere = m_controller.startController();
        }
        catch (const std::runtime_error &e)
        {
            throw;
        }
        std_msgs::Int16 mode;
        mode.data = 1;
        m_controlModePublisher.publish(mode);
        pursueGoal();

        if (m_controllerDidStartHere)
        {
            try
            {
                m_controller.stopController();
            }
            catch (const std::runtime_error &e)
            {
                throw;
            }
        }
    }

    void RFLIN::setPoint(std::string name)
    {
        m_isPrepared = false;
        m_pointName = name;

        std::string taskSpaceName = m_pathPrefix;
        taskSpaceName.append(m_pointName);
        taskSpaceName.append("/taskspace");

        bool succesfullGet = m_pNodeHandle->getParam(taskSpaceName, m_point);
        if (!succesfullGet)
        {
            ROS_ERROR_STREAM("RFLIN::setPoint - Point on rosparam with path "
                             << taskSpaceName << " not found.");
            throw std::runtime_error("Could not get point from rosparam");
        }

        m_referencePoint.clear();
        for (size_t i = 0; i < 16; i++)
        {
            m_referencePoint.push_back(0);
        }

        m_pointIsRelative = false;
    }

    void RFLIN::transformPoint(double x, double y, double z, double roll_deg,
                               double pitch_deg, double yaw_deg)
    {
        double roll_rad = (roll_deg / 180) * M_PI;
        double pitch_rad = (pitch_deg / 180) * M_PI;
        double yaw_rad = (yaw_deg / 180) * M_PI;

        Eigen::AngleAxisd rollAngle(roll_rad, Eigen::Vector3d::UnitX());
        Eigen::AngleAxisd pitchAngle(pitch_rad, Eigen::Vector3d::UnitY());
        Eigen::AngleAxisd yawAngle(yaw_rad, Eigen::Vector3d::UnitZ());

        Eigen::Quaternion<double> q = (rollAngle * pitchAngle) * yawAngle;

        Eigen::Matrix3d rotationMatrix = q.matrix();
        m_point[0] = rotationMatrix(0, 0);
        m_point[1] = rotationMatrix(1, 0);
        m_point[2] = rotationMatrix(2, 0);
        m_point[4] = rotationMatrix(0, 1);
        m_point[5] = rotationMatrix(1, 1);
        m_point[6] = rotationMatrix(2, 1);
        m_point[8] = rotationMatrix(0, 2);
        m_point[9] = rotationMatrix(1, 2);
        m_point[10] = rotationMatrix(2, 2);
        m_point[12] = x;
        m_point[13] = y;
        m_point[14] = z;
        m_point[15] = 1.0;
    }

    void RFLIN::setRelativePoint(double x, double y, double z,
                                 bool useEECoordinates, double roll_deg,
                                 double pitch_deg, double yaw_deg)
    {
        m_isPrepared = false;
        m_pointName = "";

        if (std::abs(x) > 1 || std::abs(y) > 1 || std::abs(z) > 1)
        {
            ROS_ERROR_STREAM(
                "DHLIN::setRelativePoint - Your offsets exceed the valid limit  of "
                "1m. Aborting set of Point.");
            throw std::runtime_error("Limit exceeded.");
        }

        m_point.clear();
        m_referencePoint.clear();
        for (size_t i = 0; i < 16; i++)
        {
            m_point.push_back(0);
            m_referencePoint.push_back(0);
        }
        transformPoint(x, y, z, roll_deg, pitch_deg, yaw_deg);
        m_pointIsRelative = true;
        m_pointUsesEECoordinates = useEECoordinates;
    }

    void RFLIN::setRelativePoint(std::string name, double x, double y, double z,
                                 bool useEECoordinates, double roll_deg,
                                 double pitch_deg, double yaw_deg)
    {
        m_isPrepared = false;
        m_pointName = name;

        std::string taskSpaceName = m_pathPrefix;
        taskSpaceName.append(m_pointName);
        taskSpaceName.append("/taskspace");

        bool succesfullGet =
            m_pNodeHandle->getParam(taskSpaceName, m_referencePoint);
        if (!succesfullGet)
        {
            ROS_ERROR_STREAM("RFLIN::setPoint - Point on rosparam with path "
                             << taskSpaceName << " not found.");
            throw std::runtime_error("Could not get point from rosparam");
        }

        m_point.clear();
        for (size_t i = 0; i < 16; i++)
        {
            m_point.push_back(0);
        }

        transformPoint(x, y, z, roll_deg, pitch_deg, yaw_deg);
        m_pointIsRelative = true;
        m_pointUsesEECoordinates = useEECoordinates;
    }

    void RFLIN::moveToPoint(std::string name)
    {
        setPoint(name);
        execute();
    }

    void RFLIN::moveToPoint(std::string namePrefix, int number)
    {
        namePrefix.append(std::to_string(number));
        moveToPoint(namePrefix);
    }

    void RFLIN::moveRelativeToPoint(double x, double y, double z,
                                    bool useEECoordinates, double roll_deg,
                                    double pitch_deg, double yaw_deg)
    {
        setRelativePoint(x, y, z, useEECoordinates, roll_deg, pitch_deg, yaw_deg);
        execute();
    }

    void RFLIN::moveRelativeToPoint(std::string name, double x, double y, double z,
                                    bool useEECoordinates, double roll_deg,
                                    double pitch_deg, double yaw_deg)
    {
        setRelativePoint(name, x, y, z, useEECoordinates, roll_deg, pitch_deg,
                         yaw_deg);
        execute();
    }

    void RFLIN::resetToDefaultValues()
    {
        // Velocity in m/s and rad/s
        m_dX_max.clear();
        m_dX_max.push_back(0.25);
        m_dX_max.push_back(0.5);

        // Acceleration in m/s² and rad/s²
        m_ddX_max.clear();
        m_ddX_max.push_back(1);
        m_ddX_max.push_back(2);

        // Override factor for translation and rotation
        m_override.clear();
        m_override.push_back(1);
        m_override.push_back(1);
    }

    void RFLIN::setOverride(double translationOverride, double rotationOverride)
    {
        if (translationOverride < 0.0 || rotationOverride < 0.0)
        {
            ROS_ERROR_STREAM(
                "RFLIN::setOverride - Negativ overrides can not be handled. "
                "Aborting set.");
            return;
        }
        if (translationOverride > 1.5 || rotationOverride > 1.5)
        {
            ROS_ERROR_STREAM(
                "RFLIN::setOverride - Overrides larger than 1.5 can not be "
                "handled. Aborting set. Change your acceleration and speed "
                "instead.");
            return;
        }
        if (translationOverride < 0.01 || rotationOverride < 0.01)
        {
            ROS_WARN_STREAM(
                "RFLIN::setOverride - Overrides smaller than 0.01 are discouraged, "
                "because maybe no motion will be executed at all.");
        }
        if (translationOverride > 1.01 || rotationOverride > 1.01)
        {
            ROS_WARN_STREAM(
                "RFLIN::setOverride - Overrides larger than 1 are discouraged, "
                "because your motion may be to fast. Change your acceleration and "
                "speed instead.");
        }

        m_override.clear();
        m_override.push_back(translationOverride);
        m_override.push_back(rotationOverride);
        return;
    }

    void RFLIN::setOverride(double override) { setOverride(override, override); }

    void RFLIN::setSpeed(double translationSpeed, double rotationSpeed)
    {
        if (translationSpeed < 0.0 || rotationSpeed < 0.0)
        {
            ROS_ERROR_STREAM(
                "RFLIN::setSpeed - Negativ speeds can not be handled. Aborting "
                "set.");
            return;
        }
        if (translationSpeed > 1.7 || rotationSpeed > 2.5)
        {
            ROS_ERROR_STREAM(
                "RFLIN::setSpeed - Speed are beyond specification limits. Aborting "
                "set.");
            return;
        }
        if (translationSpeed > 1 || rotationSpeed > 1.5)
        {
            ROS_WARN_STREAM(
                "RFLIN::setSpeed - Speeds are almost at the specification limits. "
                "I hope you know what you are doing.");
        }

        m_dX_max.clear();
        m_dX_max.push_back(translationSpeed);
        m_dX_max.push_back(rotationSpeed);
        return;
    }

    void RFLIN::setAcceleration(double translationAcc, double rotationAcc)
    {
        if (translationAcc < 0.0 || rotationAcc < 0.0)
        {
            ROS_ERROR_STREAM(
                "RFLIN::setAcceleration - Negativ accelerations can not be "
                "handled. Aborting set.");
            return;
        }
        if (translationAcc > 13 || rotationAcc > 25)
        {
            ROS_ERROR_STREAM(
                "RFLIN::setAcceleration - Accelerations are beyond specification "
                "limits. Aborting set.");
            return;
        }
        if (translationAcc > 7 || rotationAcc > 13)
        {
            ROS_WARN_STREAM(
                "RFLIN::setAcceleration - Speeds are almost at the specification "
                "limits. I hope you know what you are doing.");
        }

        m_ddX_max.clear();
        m_ddX_max.push_back(translationAcc);
        m_ddX_max.push_back(rotationAcc);
        return;
    }

    // ===============================
    // MARK: Private Methods
    // ===============================

    void RFLIN::pursueGoal()
    {
        m_client.waitForServer();
        m_client.sendGoal(m_goal);
        bool goalSend = true;
        bool goalCanceled = false;

        ros::Rate r(100); // Hz;
        while (!m_client.getState().isDone() || !goalSend)
        {
            if (m_executionStatus == 0)
            {
                if (!goalSend)
                {
                    m_client.sendGoal(m_goal);
                    goalSend = true;
                    goalCanceled = false;
                }
            }
            else if (m_executionStatus == 1)
            {
                if (!goalCanceled)
                {
                    m_client.cancelAllGoals();
                    // Use this approach for pausing:
                    // m_client.waitForResult();
                    // m_errorRecoveryClient.waitForServer();
                    // franka_control::ErrorRecoveryGoal recoveryGoal;
                    // m_errorRecoveryClient.sendGoal(recoveryGoal);
                    // goalSend = false;
                    // goalCanceled = true;
                    if (m_controllerDidStartHere)
                    {
                        m_controller.stopController();
                    }
                    throw std::runtime_error("UserStopError");
                    break;
                }
            }
            else if (m_executionStatus == 2)
            {
                m_client.cancelAllGoals();
                if (m_controllerDidStartHere)
                {
                    m_controller.stopController();
                }
                throw std::runtime_error("ReflexError");
                break;
            }
            else
            {
                m_client.cancelAllGoals();
                if (m_controllerDidStartHere)
                {
                    m_controller.stopController();
                }
                throw std::runtime_error("FatalError");
            }

            r.sleep();
        }
    }

    void RFLIN::cb_executionStatusUpdate(rf_robot_control::RFExecutionStatus msg)
    {
        m_executionStatus = msg.status;
    }

} // namespace rf
