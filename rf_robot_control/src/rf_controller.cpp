/// @file rf_controller.cpp
///
/// @author Dwayne Steinke
/// @date 15.11.19
///
/// @brief Class implementation of RFController
///
/// rf_controller.cpp
/// rf_robot_control
///
/// Created by Dwayne Steinke on 15.11.19 for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.
///
///

#include "rf_robot_control/rf_controller.h"

namespace rf {

RFController::RFController(ros::NodeHandle& nodeHandle,
                           std::string controllerName, bool safeStart)
    : RFCommand(nodeHandle),
      m_CONTROLLER_NAME(controllerName),
      m_useSafeStart(safeStart) {}

RFController::~RFController() {
    if (m_controllerDidStartHere) {
        stopController();
    }
}

void RFController::execute() {
    bool controllerRunning = controllerIsRunning();
    try {
        if (controllerRunning) {
            stopController();
        } else {
            startController();
        }
    } catch (const std::runtime_error& e) {
        throw;
    }
}

//
// void RFController::loadController(){
// 	ControllerStates controllers = getControllerStates();
// 	bool controllerLoaded = false;
//
// 	for (size_t i = 0; i < controllers.size(); i++) {
// 		if (controllers[i].name == m_CONTROLLER_NAME) {
// 			controllerLoaded = true;
// 		}
// 	}
//
// 	m_controllerDidLoadHere = false;
//
// 	if (!controllerLoaded) {
// 		controller_manager_msgs::LoadController loadSrv;
// 		loadSrv.request.name = m_CONTROLLER_NAME;
// 		ros::service::call("/controller_manager/load_controller",
// loadSrv); 		if (!loadSrv.response.ok){ 			throw
// std::runtime_error("RFController::loadController - Controller could not be
// loaded!");
// 		}
// 		m_controllerDidLoadHere = true;
// 	}
// }

bool RFController::startController(bool async) {
    bool controllerRunning = controllerIsRunning();

    m_controllerDidStartHere = false;

    bool retryStart = true;
    while (retryStart) {
        retryStart = false;

        if (!controllerRunning) {
            controller_manager_msgs::SwitchController srv;
            srv.request.start_controllers.push_back(m_CONTROLLER_NAME);
            srv.request.strictness = srv.request.BEST_EFFORT;
            ros::service::call("/controller_manager/switch_controller", srv);
            ROS_INFO_STREAM(
                "RFController::startController - Starting Controller: "
                << m_CONTROLLER_NAME);
            if (!srv.response.ok) {
                throw std::runtime_error(
                    "RFController::startController - Controller could not be "
                    "started!");
            }
            m_controllerDidStartHere = true;
            if (!async) {
                waitForControllerToStart();
                if (m_useSafeStart) {
                    ros::Duration(0.5).sleep();
                    controllerRunning = controllerIsRunning();
                    retryStart = true;
                }
            }
        }
    }
    m_useSafeStart = false;
    return m_controllerDidStartHere;
}

std::vector<controller_manager_msgs::ControllerState>
RFController::getControllerStates() {
    controller_manager_msgs::ListControllers srv;
    ros::service::call("/controller_manager/list_controllers", srv);
    return srv.response.controller;
}

void RFController::waitForControllerToStart() {
    ros::Rate r(100);  // Hz;
    bool controllerRunning = false;
    while (!controllerRunning) {
        controllerRunning = controllerIsRunning();
        r.sleep();
    }
}

void RFController::waitForControllerToStop() {
    ros::Rate r(100);  // Hz;
    bool controllerRunning = true;
    while (controllerRunning) {
        controllerRunning = controllerIsRunning();
        r.sleep();
    }
}

void RFController::stopController(bool async) {
    bool controllerRunning = controllerIsRunning();

    if (controllerRunning) {
        controller_manager_msgs::SwitchController srv;
        srv.request.stop_controllers.push_back(m_CONTROLLER_NAME);
        srv.request.strictness = srv.request.BEST_EFFORT;
        ros::service::call("/controller_manager/switch_controller", srv);
        ROS_DEBUG_STREAM("Stopping Controller");
        if (!srv.response.ok) {
            throw std::runtime_error(
                "RFController::stopController - Controller could not be "
                "stopped!");
        }
        if (!async) {
            waitForControllerToStop();
        }
    }
}

bool RFController::controllerIsRunning() {
    ControllerStates controllers = getControllerStates();
    bool controllerRunning = false;
    for (size_t i = 0; i < controllers.size(); i++) {
        // ROS_INFO_STREAM("RFController::controllerIsRunning: " <<
        // controllers[i].name << " " << controllers[i].state);
        if ((controllers[i].name == m_CONTROLLER_NAME) &&
            (controllers[i].state == "running")) {
            controllerRunning = true;
        }
    }
    return controllerRunning;
}

}  // namespace rf
