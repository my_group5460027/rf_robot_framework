/// @file main.cpp
///
/// @author Dwayne Steinke
/// @date 08.10.19
///
/// @brief Programm main, start from here
///
/// main.cpp
/// rf_robot_control
///
/// Created by Dwayne Steinke on 08.10.19 for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.
///

#include <ros/ros.h>

#include <QApplication>

#include "rf_robot_control/rf_robot_control.h"
#include "rf_robot_control/rf_robot_control_presenter.h"
#include "rf_robot_control/rf_robot_control_service_presenter.h"
#include "rf_robot_control/mainwindow.h"

int main(int argc, char** argv) {
    ros::init(argc, argv, "rf_robot_control");

    QApplication a(argc, argv);

    // ROS_INFO_STREAM("Ideal Thread Count:" << QThread::idealThreadCount());
    rf::RFRobotControl robotControl;
    MainWindow w;

    rf::RFRobotControlPresenter presenter(&w, &robotControl);
    rf::RFRobotControlServicePresenter servicePresenter(&robotControl);
    w.show();
    return a.exec();
}
