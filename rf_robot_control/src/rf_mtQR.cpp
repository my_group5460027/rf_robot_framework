/// @file rf_mtQR.cpp
///
/// @author Anais Millan Cerezo
/// @date 21.07.22
///
/// @brief
///
/// rf_mtQR.cpp
/// rf_robot_control
///
/// Created by Anais Millan Cerezo on 21.07.2022 for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.
///

#include "rf_robot_control/rf_mtQR.h"

namespace rf{

    RFMTQR::RFMTQR(ros::NodeHandle &nodeHandle)
        : RFCommand(nodeHandle)
    {
        m_name= "MtQR";
        postion_sub = m_pNodeHandle->subscribe("/visp_auto_tracker/object_position", 10, &RFMTQR::PoseCallback, this);
        status_sub = m_pNodeHandle->subscribe("/visp_auto_tracker/status", 100, &RFMTQR::StatusCallback, this);
    }
    RFMTQR::~RFMTQR(){}

    // ####### Callbacks for the QR_Tracker ########//
    void RFMTQR::PoseCallback (const geometry_msgs::PoseStamped::ConstPtr &msg){
        // ROS_INFO_STREAM("Received pose: " << msg);
		x_current = msg->pose.position.x;
		y_current = msg->pose.position.y;
		z_current = msg->pose.position.z;
		x_quat = msg->pose.orientation.x;
		y_quat = msg->pose.orientation.y;
		z_quat = msg->pose.orientation.z;
		w_quat = msg->pose.orientation.w;
    }
    void RFMTQR::StatusCallback (const std_msgs::Int16 &status){
        camera_feedback = status.data; // gives feedback whether the camera recognizes a QR_code
    }
    
    // ######## Helper Functions ########## //

    void RFMTQR::CAMERA_REC(){
        if(camera_feedback == 1){
            camera_status = false; 
            ROS_INFO_STREAM("QR Code not recognized, move the robot ");
        }else{
            camera_status = true;
            ROS_INFO_STREAM("QR Code recognized");
        }
        // Rostopic /visp_auto_tracker/status  -> 1: No QR-code recognized. 3||4: QR-Code recognized
    }

    void RFMTQR::EE_T_QR(double x_cam, double y_cam, double z_cam){
        double x_EE_CAM = 0.042; // Offset camera - EE in x direction 
        double y_EE_CAM = -0.012; // Offset camera - EE in y direction 
        double z_EE_CAM = -0.065; // Offset camera - EE in z direction 

        Eigen::Matrix4d EE_T_Camera; // Transformation from EE to Camera; 
        EE_T_Camera <<  0.0, -1.0, 0.0, x_EE_CAM,
			            1.0, 0.0, 0.0, y_EE_CAM,
		            	0.0, 0.0, 1.0, z_EE_CAM,
			            0.0, 0.0, 0.0, 1.0;
        
        Eigen::Quaterniond QUA_CAM_QR;// Object Position Quaternion ( From Camera )
            QUA_CAM_QR.x() = x_quat;
            QUA_CAM_QR.y() = y_quat;
            QUA_CAM_QR.z() = z_quat;
            QUA_CAM_QR.w() = w_quat;
        
        Eigen::Matrix3d R_CAM_QR = QUA_CAM_QR.normalized().toRotationMatrix();// Object Position Rotationsmatrix 
        
        Eigen::Matrix4d QR_CAM; // Transformation Camera to QR Code
		    QR_CAM << R_CAM_QR(0, 0), R_CAM_QR(0, 1), R_CAM_QR(0, 2), x_cam,
                      R_CAM_QR(1, 0), R_CAM_QR(1, 1), R_CAM_QR(1, 2), y_cam,
                      R_CAM_QR(2, 0), R_CAM_QR(2, 1), R_CAM_QR(2, 2), z_cam,
                      0.0, 0.0, 0.0, 1.0;

        Eigen::Matrix<double ,4,1> P_QR(0,0,0,1);
    
        EE_QR = EE_T_Camera * QR_CAM * P_QR;

        pitch_deg = asin(QR_CAM(2, 0)) * (180.0 / M_PI); // Rotation der x Achse 
		yaw_deg = atan2(QR_CAM(1, 0), QR_CAM(0, 0)) * (180.0 / M_PI); // Rotation der y achse
		roll_deg = atan2(QR_CAM(2, 1), QR_CAM(2, 2)) * (180.0 / M_PI); // Rotation z Achse 

    }

}//namespace rf