/// @file rf_robot_control_utility.cpp
///
/// @author Dwayne Steinke
/// @date 23.01.20
///
/// @brief
///
/// rf_robot_control_utility.cpp
/// rf_robot_control
///
/// Created by Dwayne Steinke on 23.01.20 for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.
///
///

#include "rf_robot_control/rf_robot_control_utility.h"


namespace rf {


RFRobotControlUtility::RFRobotControlUtility(ros::NodeHandle &nodeHandle):RFCommand(nodeHandle){

	m_jointImpedanceClient = nodeHandle.serviceClient<franka_msgs::SetJointImpedance>("franka_control/set_joint_impedance");

	m_cartesianImpedanceClient = nodeHandle.serviceClient<franka_msgs::SetCartesianImpedance>("franka_control/set_cartesian_impedance");

	m_EEFrameClient = nodeHandle.serviceClient<franka_msgs::SetEEFrame>("franka_control/set_EE_frame");

	m_KFrameClient = nodeHandle.serviceClient<franka_msgs::SetKFrame>("franka_control/set_K_frame");

	m_forceTorqueCollisionBehaviorClient = nodeHandle.serviceClient<franka_msgs::SetForceTorqueCollisionBehavior>("franka_control/set_force_torque_collision_behavior");

	m_fullCollisionBehaviorClient = nodeHandle.serviceClient<franka_msgs::SetFullCollisionBehavior>("franka_control/set_full_collision_behavior");

	m_loadClient = nodeHandle.serviceClient<franka_msgs::SetLoad>("franka_control/set_load");
}



RFRobotControlUtility::~RFRobotControlUtility(){

}




void RFRobotControlUtility::execute(){
	ROS_WARN_STREAM("RFRobotControlUtility::execute - No function for execute implemented. Doing nothing.");
}


void RFRobotControlUtility::setJointImpedance(boost::array<double,7> newImpedances){

	for (size_t i = 0; i < 7; i++) {
		if (newImpedances[i] < 0 || newImpedances[i] > 5000) {
			ROS_ERROR_STREAM("RFRobotControlUtility::setJointImpedance - Values out of range. Aborting set.");
			throw std::runtime_error("RFRobotControlUtility::setJointImpedance - Values out of range.");
		}
	}

	if (!m_jointImpedanceClient.exists()){
		ROS_ERROR_STREAM("RFRobotControlUtility::setJointImpedance service call does not exist! It is either not advertised or available.");
		throw std::runtime_error("RFRobotControlUtility::setJointImpedance service call does not exist!");
	}

	franka_msgs::SetJointImpedance srv;
	srv.request.joint_stiffness = newImpedances;
	m_jointImpedanceClient.call(srv);

	if (!srv.response.success) {
		ROS_ERROR_STREAM("RFRobotControlUtility::setJointImpedance - Service call not succesfull. Reason: " << srv.response.error);
		throw std::runtime_error("RFRobotControlUtility::setJointImpedance - Service call not succesfull.");
	}
}




void RFRobotControlUtility::setCartesianImpedance(boost::array<double,6> newImpedances){
	double upperLimit = 3000;

	for (size_t i = 0; i < 3; i++) {
		if (i >= 3) {
			upperLimit = 500;
		}
		if (newImpedances[i] < 0 || newImpedances[i] > upperLimit) {
			ROS_ERROR_STREAM("RFRobotControlUtility::setCartesianImpedance - Values out of range. Aborting set.");
			throw std::runtime_error("RFRobotControlUtility::setCartesianImpedance - Values out of range.");
		}
	}

	if (!m_cartesianImpedanceClient.exists()){
		ROS_ERROR_STREAM("RFRobotControlUtility::setCartesianImpedance - Service call does not exist! It is either not advertised or available.");
		throw std::runtime_error("RFRobotControlUtility::setCartesianImpedance - Service call does not exist.");
	}

	franka_msgs::SetCartesianImpedance srv;
	srv.request.cartesian_stiffness = newImpedances;
	m_cartesianImpedanceClient.call(srv);

	if (!srv.response.success) {
		ROS_ERROR_STREAM("RFRobotControlUtility::setCartesianImpedance - Service call not succesfull. Reason: " << srv.response.error);
		throw std::runtime_error("RFRobotControlUtility::setCartesianImpedance - Service call not succesfull.");
	}
}







void RFRobotControlUtility::setCollisionBehaviour(boost::array<double,7> lowerTorqueThresholds, boost::array<double,7> upperTorqueThresholds, boost::array<double,6> lowerForceThresholds, boost::array<double,6> upperForceThresholds){

	for (size_t i = 0; i < 7; i++) {
		if (lowerTorqueThresholds[i] < 0 || lowerTorqueThresholds[i] > upperTorqueThresholds[i]) {
			ROS_ERROR_STREAM("RFRobotControlUtility::setCollisionBehaviour - lowerTorqueThresholds values out of range. Aborting set.");
			throw std::runtime_error("RFRobotControlUtility::setCollisionBehaviour - lowerTorqueThresholds values out of range.");
		}
	}

	// Limit values for upperTorqueThresholds as in Desk:
	boost::array<double,7> minTorqueLimits = {3, 3, 3, 2, 2, 1, 1};
	boost::array<double,7> maxTorqueLimits = {100,100,100,80,80,40,40};

	for (size_t i = 0; i < 7; i++) {
		if (upperTorqueThresholds[i] < minTorqueLimits[i] || upperTorqueThresholds[i] > maxTorqueLimits[i]) {
			ROS_ERROR_STREAM("RFRobotControlUtility::setCollisionBehaviour - upperTorqueThresholds values out of range. Aborting set.");
			throw std::runtime_error("RFRobotControlUtility::setCollisionBehaviour - upperTorqueThresholds values out of range.");
		}
	}



	for (size_t i = 0; i < 6; i++) {
		if (lowerForceThresholds[i] < 0 || lowerForceThresholds[i] > upperForceThresholds[i]) {
			ROS_ERROR_STREAM("RFRobotControlUtility::setCollisionBehaviour - lowerForceThresholds values out of range. Aborting set.");
			throw std::runtime_error("RFRobotControlUtility::setCollisionBehaviour - lowerForceThresholds values out of range.");
		}
	}

	// Limit values for upperTorqueThresholds as in Desk:
	boost::array<double,6> minForceLimits = {3, 3, 3, 1, 1, 1};
	boost::array<double,6> maxForceLimits = {100,100,100,30,30,30};

	for (size_t i = 0; i < 6; i++) {
		if (upperForceThresholds[i] < minForceLimits[i] || upperForceThresholds[i] > maxForceLimits[i]) {
			ROS_ERROR_STREAM("RFRobotControlUtility::setCollisionBehaviour - upperForceThresholds values out of range. Aborting set.");
			throw std::runtime_error("RFRobotControlUtility::setCollisionBehaviour - upperForceThresholds values out of range.");
		}
	}

	if (!m_forceTorqueCollisionBehaviorClient.exists()){
		ROS_ERROR_STREAM("RFRobotControlUtility::setCollisionBehaviour - Service call does not exist! It is either not advertised or available.");
		throw std::runtime_error("RFRobotControlUtility::setCollisionBehaviour - Service call does not exist.");
	}

	franka_msgs::SetForceTorqueCollisionBehavior srv;
	srv.request.lower_torque_thresholds_nominal = lowerTorqueThresholds;
	srv.request.upper_torque_thresholds_nominal = upperTorqueThresholds;
	srv.request.lower_force_thresholds_nominal = lowerForceThresholds;
	srv.request.upper_force_thresholds_nominal = upperForceThresholds;
	m_forceTorqueCollisionBehaviorClient.call(srv);

	if (!srv.response.success) {
		ROS_ERROR_STREAM("RFRobotControlUtility::setCollisionBehaviour - Service call not succesfull. Reason: " << srv.response.error);
		throw std::runtime_error("RFRobotControlUtility::setCollisionBehaviour - Service call not succesfull.");
	}
}




} // namespace dh
