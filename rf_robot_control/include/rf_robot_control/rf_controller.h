/// @file rf_controller.h
///
/// @author Dwayne Steinke
/// @date 15.11.19
///
/// @brief Class decleration of RFController
///
/// rf_controller.h
/// rf_robot_control
///
/// Created by Dwayne Steinke on 15.11.19 for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.
///
///

#ifndef RF_CONTROLLER_H
#define RF_CONTROLLER_H

#include <ros/ros.h>

#include "controller_manager_msgs/ListControllers.h"
#include "controller_manager_msgs/LoadController.h"
#include "controller_manager_msgs/SwitchController.h"
#include "rf_robot_control/rf_command.h"

namespace rf {

/// @brief Manage one single controller with service calls through the
/// ros_control controller manager
///
/// This is a client for the services advertised by the ros_control controller
/// manager.
///
/// It calls the services to start and stop controllers at runtime.
/// At the moment, all controllers that should be handled
/// through this class, have to be loaded (e.g. in a launchfile).
///
/// By design, objects of this class support autocleanup.
/// This means that controllers that are loaded/started through
/// an object of this class will be stopped/unloaded automaticly
/// in the Destructor. Note however, that loaded/started means an
/// executed service call to the controller manager.
/// If you just call startController() and the desired controller
/// is already running (e.g. another RFController started it earlier
/// in your program), it will not be stopped at the destruction
/// of this object.
///
/// Think of it like you use new and delete in C++. The RFController
/// that first starts a controller, should stop it.
class RFController : public RFCommand {
    typedef std::vector<controller_manager_msgs::ControllerState>
        ControllerStates;

   private:
    /// @brief Name of the controller to start and stop
    const std::string m_CONTROLLER_NAME;

    /// @brief Use a safe start
    ///
    /// The safe start waits 0.5s after a successfull startup and checks if the
    /// controller is still running.
    ///
    /// @note This behaviour is implemented due to a bug in the controller
    /// manager in combination with franka_control. A controller will usually
    /// be started, then stoped immediately and then started again.
    /// This can result in undefined behaviour in combination with rf_controller
    /// because it may occur that two controllers think they started the same
    /// controller
    bool m_useSafeStart = false;

    /// @name Lifecycle Trigger
    /// @{
    // @brief Controller was loaded from within this class
    // bool m_controllerDidLoadHere = false;

    /// @brief Controller was startedt from within this class
    bool m_controllerDidStartHere = false;

    /// @}

   public:
    /// @brief Konstruktor
    ///
    /// @param safeStart Set m_useSafeStart to the specified status. @see
    /// startController()
    RFController(ros::NodeHandle &nodeHandle, std::string controllerName,
                 bool safeStart = false);
    virtual ~RFController();

    /// @brief Toggle between start and stop of the controller
    ///
    /// If the controller is running, it will be stopped
    /// If the controller is stopped, it will be started
    ///
    /// @throws std::runtime_error see startController() and stopController()
    virtual void execute() override;

    // @brief Load the PTP-Motion-Generator if necessary
    //
    // This function asks the controller manager if the defined
    // controller with #rf_PTP_MOTION_GENERATOR_NAME is loaded
    // and tries to load it if not.
    //
    // @throws std::runtime_error Controller could not be loaded!
    // void loadController();

    /// @brief Start the Controller if necessary
    ///
    /// This function asks the controller manager if the desired
    /// controller with #m_CONTROLLER_NAME is started
    /// and tries to start it if not.
    ///
    /// If the argument async is false, the function does only
    /// return after a start request if the controller is active and started.
    ///
    /// @attention If the argument async is true, this functions
    /// returns immediatly after sending a start request without
    /// checking if the controller is running. No safeStart procedure is
    /// performed. This does not throw an error. Therefore it is possible to
    /// send goals to a controller that is not yet running. The reaction to this
    /// depends on the controller. Most of the time the goals will be preempted.
    ///
    /// Debending on m_useSafeStart the start routine waits 0.5s after
    /// a successfull startup and checks if the
    /// controller is still running.
    ///
    /// @attention m_useSafeStart is always reset to false after a succesfull
    /// start.
    ///
    /// @note This behaviour is implemented due to a bug in the controller
    /// manager in combination with franka_control. A controller will usually
    /// be started, then stoped immediately and then started again.
    /// This can result in undefined behaviour in combination with rf_controller
    /// because it may occur that two controllers think they started the same
    /// controller
    ///
    /// @param async Optional: Use async mode (default = false)
    /// @returns true, if controller was started through this call, false if it
    /// was already running.
    /// @throws std::runtime_error Controller could not be started!
    bool startController(bool async = false);

    /// @brief Stop the Controller if necessary
    ///
    /// This function asks the controller manager if the desired
    /// controller with #m_CONTROLLER_NAME is stopped
    /// and tries to stop it if not.
    ///
    /// If the argument async is false, the function does only
    /// return after a stop request if the controller is stopped.
    ///
    /// @attention If the argument async is true, this functions
    /// returns immediatly after sending a stop request without
    /// checking if the controller is stopped.
    /// This does not throw an error.
    ///
    /// @param async Optional: Use async mode (default = false)
    /// @throws std::runtime_error Controller could not be stopped!
    void stopController(bool async = false);

    /// @brief Get the ControllerStates of the controller manager
    ///
    /// This includes only loaded controllers.
    ///
    /// @return A std::vector with controllerStates
    /// (name,state,type,claimed_resources)
    ControllerStates getControllerStates();

    /// @brief Returns when the desired controller is active and started
    ///
    /// This member function can be called after startController()
    /// to wait with the program execution until the controller is
    /// up and running.
    ///
    /// @todo Add optional timeout
    void waitForControllerToStart();

    /// @brief Returns when the desired controller is inactive and stopped
    ///
    /// This member function can be called after stopController()
    /// to wait with the program execution until the controller is
    /// inactive and stopped.
    ///
    /// @todo Add optional timeout
    void waitForControllerToStop();

    /// @brief Check if the desired controller is running
    ///
    /// @returns true if the controller is active and started, false otherwise
    bool controllerIsRunning();
};

}  // namespace dh

#endif
