/// @file rf_ptp.h
///
/// @author Dwayne Steinke
/// @date 21.10.19
///
/// @brief Class decleration of RFPTP
///
/// rf_ptp.h
/// rf_robot_control
///
/// Created by Dwayne Steinke on 21.10.19 for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.
///
///

#ifndef RF_PTP_H
#define RF_PTP_H

#include <actionlib/client/simple_action_client.h>
#include <control_msgs/FollowJointTrajectoryAction.h>
#include <math.h>
#include <ros/ros.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Int16.h>

#include <trajectory_msgs/JointTrajectoryPoint.h>

#include <stdexcept>

#include "rf_robot_control/RFExecutionStatus.h"
#include "rf_robot_control/rf_command.h"
#include "rf_robot_control/rf_config_defines.h"
#include "rf_robot_control/rf_controller.h"

namespace rf {

/// @brief Move direct from one point to another in jointspace
///
/// This class wraps service calls to the ros_control controller_manager
/// and action calls to a JointTrajectoryController.
///
/// @attention This class requires, that the controller manager is running
/// and a JointTrajectoryController is loaded.
///
/// It supports movement to absolute poses in jointspace.
///
/// The duration of the motion depends on the m_duration_sec parameter.
/// Furthermore, the global override parameter is supported.
class RFPTP : public RFCommand {
    typedef actionlib::SimpleActionClient<
        control_msgs::FollowJointTrajectoryAction>
        Client;

    typedef trajectory_msgs::JointTrajectoryPoint TrajectoryPoint;

    typedef std::vector<controller_manager_msgs::ControllerState>
        ControllerStates;

    /// @name Motion execution parameters
    /// @{

    /// @brief Name of the loaded point
    std::string m_pointName;

    /// @brief The used Path prefix on rosparam to load points from
    /// Search path for the point is:
    /// m_pathPrefix<name>/jointspace
    std::string m_pathPrefix;

    /// @brief Loaded point in jointspace [7]
    std::vector<double> m_point;

    /// @brief Movement duration in sec.
    double m_duration_sec = 5;

    /// @}

    /// @name Execution managment and lifecycle
    /// @{

    /// @brief A SimpleActionClient to talk to the motion generator
    Client m_client;

    /// @brief The pursued goal
    control_msgs::FollowJointTrajectoryGoal m_goal;

    /// @brief The controller was launched through the #m_controller
    /// of this class
    ///
    /// If m_controllerDidStartHere it will stop the launched
    /// controller if the execution is done.
    bool m_controllerDidStartHere = false;

    /// @brief Controller configurator to talk to the ros_control controller
    /// manager
    RFController m_controller;

    /// @brief Subscriber of the execution Status
    /// This makes it possible to abort the execution e.g.
    /// if a user stopped is pressed.
    ros::Subscriber m_executionStatusSubscriber;

    /// @brief Publisher of the control mode. Required for using the simulation environment
    ros::Publisher m_controlModePublisher;


    /// @brief The latest executionStatus recieved from the
    /// #m_executionStatusSubscriber
    ///
    /// @see RFExecutionStatus.msg for a description of possible status
    uint8_t m_executionStatus = -1;

    /// @}

    /// @brief Send the latest m_goal and wait for a result
    ///
    /// This function also checks m_executionStatus with 100 Hz
    /// and aborts the goal if m_executionStatus != OK.
    /// If this is the case it throws an std::runtime error
    /// and stops the controller if necessary.
    ///
    /// @throws std::runtime_error
    void pursueGoal();

    /// @brief Callback function for the execution status
    ///
    /// Writes the latest status to m_executionStatus;
    ///
    /// @see m_executionStatusSubscriber
    void cb_executionStatusUpdate(rf_robot_control::RFExecutionStatus msg);

   public:
    /// @brief Konstruktor
    ///
    ///
    /// Uses Macros rf_POINTS_PATH_PREFIX, rf_PTP_ACTION_NAME and
    /// rf_PTP_MOTION_GENERATOR_NAME: @see rf_config_defines.h
    RFPTP(ros::NodeHandle &nodeHandle);

    virtual ~RFPTP();

    /// @brief Prepare the exceution of the motion command
    ///
    /// Check if all required parameters are set and write
    /// them into m_goal
    ///
    /// Also loads globalOverride from rosparam and
    /// divides the time_from_start.
    ///
    /// @throws std::runtime_error Parameters not set
    virtual void prepare() override;

    /// @brief Execute motion command with latest configuration
    ///
    /// Calls prepare() if unprepared, starts the controller if necessary,
    /// sends and pursues a goal and stopps the controller if necessary
    ///
    /// Thows an std::runtime error if something of the steps
    /// failed and the motion was either not started or
    /// not fully finished. (e.g. controller not startable,
    /// Reflex or user stop triggerd etc.)
    ///
    /// @throws std::runtime_error Motion failed
    virtual void execute() override;

    /// @name Target point configuration and move commands
    /// @{

    /// @brief Get Point with name from rosparam
    ///
    /// Search path for the point is:
    /// points/<name>/jointspace
    ///
    /// @param name Pointname without any /
    ///
    /// @throws std::runtime_error Could not get point from rosparam
    void setPoint(std::string name);

    /// @brief Set the desired duration of the motion
    ///
    /// Duration has to be between 0.1 and 60 seconds.
    ///
    /// @param sec new duration in seconds
    void setDuration(double sec);

    /// @brief Convert a m_point to a TrajectoryPoint
    ///
    /// positions = m_point;
    /// velocities and accelerations = 0;
    TrajectoryPoint constructGoalPoint();

    /// @brief Execute a motion to a point
    ///
    /// Combines setPoint() and execute()
    ///
    /// @param name Pointname without any /
    /// @param inSec Desired duration in seconds
    ///
    /// @throws std::runtime_error See setPoint() and execute()
    void moveToPoint(std::string name, double inSec);

    /// @brief Execute a motion to a point named with a prefix and number
    ///
    /// Combines setPoint() and execute()
    ///
    /// The pointname is namePrefix + number (example: P1)
    /// This function can be used to iterate over motion commands in a loop,
    /// if all Points share the same prefix.
    ///
    /// @param name Pointname prefix withoutany /
    /// @param number Pointnumber added to prefix
    /// @param inSec Desired duration in seconds
    ///
    /// @throws std::runtime_error See setPoint() and execute()
    void moveToPoint(std::string namePrefix, int number, double inSec);

    /// @}
};

}  // namespace dh
#endif /* rf_ptp_hpp */
