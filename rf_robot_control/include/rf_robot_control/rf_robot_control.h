/// @file rf_robot_control.h
///
/// @author Dwayne Steinke
/// @date 14.10.19
///
/// @brief
///
/// rf_robot_control.hpp
/// rf_robot_control
///
/// Created by Dwayne Steinke on 14.10.19 for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.
///
///

#ifndef RF_ROBOT_CONTROL_H
#define RF_ROBOT_CONTROL_H

#include <actionlib/client/simple_action_client.h>
#include <franka_msgs/FrankaState.h>
#include <ros/callback_queue.h>
#include <ros/package.h>
#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int16.h>

#include <iostream>
#include <fstream>

#include <QObject>
#include <QTimer>
#include <QtConcurrent>

#include "franka_msgs/ErrorRecoveryAction.h"
#include "rf_robot_control/RFExecutionStatus.h"
#include "rf_robot_control/RFRobotControlUserMsg.h"
#include "rf_robot_control/rf_command.h"
#include "rf_robot_control/rf_config_defines.h"
#include "rf_robot_control/rf_gripper.h"
#include "rf_robot_control/rf_ptp.h"
#include "rf_robot_control/rf_status_message_type.h"
#include "rf_robot_control/rf_stream.h"
#include "rf_robot_control/rf_testprogram.h"

namespace rf {

using std::string;
using std::vector;

/// @brief main object of the ros node, model of Qt MVP
///
///
/// About threads, callbacks and ros::spin():
/// An application with this class uses two threads.
/// The default GUI thread and a worker thread for program
/// executions. The reference to the worker thread however is not "static"
/// (e.g. as a property). The thread is only used through the
/// #QtConcurrent API if necessary.
///
/// If you are not familiar with QT. A QT app basically implements its own
/// ros::spin() (called Qt Event Loop) to service callbacks between classes
/// within the app. These callbacks are connected via so called signals and
/// slots. If one class emits a signal and this signal is connected to a slot,
/// the call to this slot will be added to the Qt Event Loop. It will be called
/// when the Event Loop/Queue is serviced.
///
/// @see https://doc.qt.io/qt-5/signalsandslots.html
/// on an introduction to Signals and Slots
/// @see https://doc.qt.io/qt-5/threads-qobject.html
/// on an introduction to the QT Event Loop
///
/// Because Qt has its own Event Loop, ros::spin() is not used in this
/// class.
/// Instead the ROScallbackQueue of the node is serviced manually with
/// RFRobotControl::spinOnce. This member function is called through a QTimer
/// every ms. This should be sufficient for the administrativ tasks the control
/// executes.
///
///
///
/// About program interupts and robot failures:
/// This class publishes an executionStatus. It is calculated from the
/// m_frankaState.robot_mode. Every movement command
/// is expected to subscribe to this topic. After their goal was send
/// to the ActionServer the commands should listen eather for the ActionResult
/// or for the executionStatus. If the executionStatus is not OK
/// (e.g. user stop pressed) they are expected to cancel all their goals,
/// stop their controller if necessary and then throw an std::runtime error.
///
/// Usually libfranka throws an error (and ignores all new controller values)
/// prior to the commands noticing it. Therefore the controllers (and libfranka)
/// have to be reset manually prior to executing a new program. This
/// is done through slot_acknowledgeErrors().
///
class RFRobotControl : public QObject {
    Q_OBJECT

    typedef actionlib::SimpleActionClient<franka_msgs::ErrorRecoveryAction>
        ErrorRecoveryClient;

    // ==================================
    // MARK: - Private Properties
    // ==================================

    /// @name Ros Utility
    /// Properties for ros interaction
    /// @{

    /// @brief The nodes nodeHandle
    ros::NodeHandle m_nodeHandle;

    /// @brief  A subscriber for the franka_states topic
    ros::Subscriber m_frankaStateSubscriber;

    /// @brief A subscriber for the user info topic
    ///
    /// The messages on this topic will be visualized in the GUI status
    /// text field.
    ros::Subscriber m_userMsgSubscriber;

    ros::Publisher m_executionStatusPublisher;

    ros::Publisher m_programStatePublisher;

    ros::Publisher m_simStatePublisher;

    ros::Publisher m_guidingModePublisher;

    ros::Publisher m_guidingCommandPublisher;

    std_msgs::Int16 m_programState;

    std_msgs::Int16 m_simState;

    ErrorRecoveryClient m_errorRecoveryClient;

    sensor_msgs::JointState m_guidingCommand;

    /// @brief A Timer to call the ros callback queue periodicly
    QTimer m_spinTimer;

    /// @}

    /// @brief QFutureWatcher of the task programm running in another thread
    ///
    /// If you run a task programm with RFCommands
    /// (e.g. pressing execute in the GUI)
    /// it will be executed in another temporary worker thread.
    ///
    /// This param guards and observes the running execution.
    /// Only one task programm can be run at a time.
    QFutureWatcher<void> m_programExecution;

    /// @brief List of commands that can be specificly called from
    /// RFRobotControl Use this vector to load Programms in the constructor of
    /// this class
    std::vector<RFCommand*> m_commands;

    /// @brief The last FrankaState message recieved by m_frankaStateSubscriber
    franka_msgs::FrankaState m_frankaState;

    /// @brief robotMode as defined in franka_msgs::FrankaState
    ///
    /// uint8 ROBOT_MODE_OTHER=0
    /// uint8 ROBOT_MODE_IDLE=1
    /// uint8 ROBOT_MODE_MOVE=2
    /// uint8 ROBOT_MODE_GUIDING=3
    /// uint8 ROBOT_MODE_REFLEX=4
    /// uint8 ROBOT_MODE_USER_STOPPED=5
    /// uint8 ROBOT_MODE_AUTOMATIC_ERROR_RECOVERY=6
    uint8_t m_robotMode = -1;

    /// @brief Maps the uint8 codes of #m_robotMode to readable Strings
    ///
    /// @see #m_robotMode for translations
    /// @see initDictionaries() on how the map is initialized
    std::map<uint8_t, std::string> m_robotModeDictionary;

    /// @brief executionStatus for move commands as in RFExecutionStatus
    ///
    /// uint8 TYPE_Ok=0
    /// uint8 TYPE_Pause=1
    /// uint8 TYPE_Reflex=2
    /// uint8 TYPE_Fatal=3
    uint8_t m_executionStatus = -1;

    /// @brief An error of the program execution is acknowldged by the user
    bool m_errorAcknowledged = true;

    /// @brief Gripper command to manually toggle gripper
    ///
    /// This can be used if no program is running.
    RFGripper m_gripper;

    /// @brief Expected status of the gripper
    bool m_gripperIsOpen = true;

    /// @brief PTP command to manually move to a single point
    ///
    /// This can be used if no program is running.
    RFPTP m_ptp;

    /// @brief Bool flag indicating if the points, that are currently loaded on the parameter server, are saved to file
    bool m_pointsSaved;

    /// @brief Current path for points to be loaded from and saved to
    std::string m_pointsPath;

    // ==================================
    // MARK: - Private Functions
    // ==================================

    /// @name Init Functions
    /// Functions that can/should be called in a consturctor
    /// @{

    /// @brief Initializes all Dictionaries aka. std::map
    /// This function should be called in the constructor
    void initDictionaries();

    /// @}

    /// @name Ros Callbacks
    /// Callback functions for ROS
    /// @{

    /// @brief Get the ROScallbackQueue and call its functions
    ///
    /// This function is called periodicly from the #m_spinTimer
    void spinOnce();

    /// @brief Callback function of the #frankaStateSubscriber
    ///
    /// Saves the message in m_frankaState and calls
    /// handleFrankaState()
    void cb_frankaStateSubscriber(franka_msgs::FrankaState msg);

    /// @brief Callback function of the #userMsgSubscriber
    ///
    /// Parses the message and emits sgn_newStatus() to display
    /// the recieved message in the GUI.
    void cb_userMsgSubscriber(rf_robot_control::RFRobotControlUserMsg msg);

    /// @brief Slot to be called when a program is finished, prints message
    void slot_programFinished();

    /// @brief Slot to be called when a program is cancelled, prints message
    void slot_programCanceled();


    /// @}

    /// @name Property Update Functions
    /// Functions that react to a propertie update
    /// These are usually called from a callback function
    /// @{

    /// @brief Handle the reaction to an update of m_frankaState
    ///
    /// This function should be called after m_frankaState was updated
    /// Calls handleFrankaRobotMode().
    void handleFrankaState();

    /// @brief Update m_robotMode adn the GUI after m_frankaState was updated
    ///
    /// Changes the m_robotMode if a new mode is published in m_frankaState.
    /// Calls GUI update signals if necessary.
    void updateFrankaRobotMode();

    /// @brief Updates and publishes the execution status depending on the robot state from the FrankaState message
    void updateExecutionStatus();

    /// @brief Converts the current joint positions from radians to degrees and emits them
    void updateJointPositions();

    /// @brief Publish the latest m_executionStatus in a
    void publishExecutionStatus();
    /// @}

   public:
    // ==================================
    // MARK: - Public Functions
    // ==================================
    /// @brief Konstruktor
    ///
    /// Loads the yaml-file points.yaml to rosparam
    ///
    ///
    RFRobotControl();

    /// @brief Destruktor
    /// Safes /points from roparam in points.yaml
    /// points.yaml can be found in rf_robot_control/config
    ///
    /// @attention
    /// Mögliche Probleme:
    /// Die Yaml-File wird jedes Mal überschrieben wenn RC geschlossen wird.
    /// Werden Punkte aktiv vom Parameterserver gelöscht verschwinden diese auch
    /// in der Yaml-File. Wird RC nicht beendet, bevor der roscore beendet wird,
    /// findet keine Sicherung der Punkte statt.
    virtual ~RFRobotControl();

    ros::NodeHandle* getNodeHandle();

    // ==================================
    // MARK: - Public Signals
    // ==================================
   signals:

    /// @brief Signal for a new robot Status for Visualization in GUI
    ///
    /// @param type One of the RFStatusMessageType
    /// (Info,Debug,Warning,Error,Fatal)
    /// @param statusDescription New Info to display
    void sgn_newStatus(RFStatusMessageType type, std::string statusDescription);

    /// @brief Signal that the robot is in a new mode
    ///
    /// @param mode Name of the mode as in #m_robotModeDictionary
    void sgn_newRobotMode(std::string mode);

    /// @brief Signal that emits the current joint positions
    ///
    /// @param jointPositions Vector containing the current joint positions to be emitted
    void sgn_jointPositions(std::vector<double> jointPositions);

    /// @brief Signal that emits the current joint names
    ///
    /// @param jointNames Vector containing the joint names to be emitted
    void sgn_jointNames(std::vector<string> jointNames);

    /// @brief Signal that emits the name of a point
    /// 
    /// @param pointName Name of the point to be emitted
    void sgn_addPointName(std::string pointName);

    /// @brief Signal that emits the name of the selected program as well as its index in the m_commands vector
    ///
    /// @param index Index in the m_commands vector
    /// @param programName Name of the program
    void sgn_addProgramName(int index, std::string programName);

    // ==================================
    // MARK: - Public Slots
    // ==================================
   public slots:

    /// @brief Writes the current joint positions from the GUI to the robot
    ///
    /// @param jointPositions Vector containing the joint positions
    void slot_writeJointPosition(std::vector<double> jointPositions);

    /// @brief Gets the status of the guiding mode toggle switch from the GUI and publishes it to the guidingMode topic
    void slot_toggleGuidingMode(bool checked);

    /// @brief Publishes the running state of the simulation to the simState topic
    void slot_startSimulation();

    /// @brief Publishes the stoppped state of the simulation to the simState topic
    void slot_stopSimulation();

    /// @brief Saves the current position of the robot to the parameter server
    ///
    /// @param name Name of the saved point?
    ///
    /// New Parameter Point will be saved under
    /// /points/<name>/jointspace and /points/<name>/taskspace
    ///
    /// Reads topic /franka_state_controller/franka_states
    void slot_savePoint(string name);

    /// @brief Runs the program in m_commands with index programNumber
    ///
    /// Index bounds checking is included. No program will be
    /// excetued if programNumber is out of bounds.
    ///
    /// @param programNumber Index of the program as in m_commands.
    /// Pleas keep in mind that the indices start with 0.
    void slot_runProgram(int programNumber);

    /// @brief The user tries to acknowledge an error
    ///
    /// Only accepted if in Mode IDLE.
    void slot_acknowledgeErrors();

    /// @brief Toggle the gripper manually if no programm is running
    ///
    /// Gripper execution is asynchronous: Success or failure are not checked.
    void slot_toggleGripper();

    /// @brief Sets the global override factor on rosparam to a new value
    ///
    /// Change only allowed in USER_STOPPED
    ///
    /// The speeds of move commands are multiplied by this factor.
    /// Therefore valid values are between 0.1 and 1
    ///
    /// @param newValue New global override value
    void slot_setGlobalOverride(double newValue);

    /// @brief Move PTP to a single teached point
    ///
    /// This function is a utility function for teaching
    /// new points. Simply move via PTP to a point you
    /// already know and start teaching from there
    ///
    /// @param name Name of the point on rosparam
    void slot_moveToPoint(string name);

    /// @brief Deletes a given point from the ROS parameter server
    ///
    /// @param pointName Name of the point to be deleted
    void slot_deletePoint(std::string pointName);

    /// @brief Loads points from a given path to the parameter server. If the current points have not been saved to file,
    /// this is done to the current file path.
    ///
    /// @param path New path to load points from
    void slot_updatePointsPath(std::string path);

    /// @brief Save points loaded to the parameter server to file specified by a given path.
    ///
    /// @param path File path to save points to
    void slot_savePointsToFile(std::string path);

};

}  // namespace rf

#endif /* rf_robot_control_hpp */
