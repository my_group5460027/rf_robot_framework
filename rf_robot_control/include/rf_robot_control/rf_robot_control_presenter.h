/// @file rf_robot_control_presenter.h
///
/// @author Dwayne Steinke
/// @date 14.10.19
///
/// @brief
///
/// rf_robot_control_presenter.h
/// rf_robot_control
///
/// Created by Dwayne Steinke on 14.10.19 for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.
///
///

#ifndef RF_ROBOT_CONTROL_PRESENTER_H
#define RF_ROBOT_CONTROL_PRESENTER_H

#include <QObject>

#include "rf_robot_control/rf_robot_control.h"
#include "rf_robot_control/rf_status_message_type.h"
#include "rf_robot_control/mainwindow.h"

namespace rf {

/// @brief Presenter of a robot control
///
/// Connects window buttons and widgets with the robot control
class RFRobotControlPresenter : public QObject {
    Q_OBJECT

    // ==========================
    // MARK: - Private Properties
   private:
    /// @brief Window/View the presenter is presenting
    MainWindow* m_pWindow;

    /// @brief model of the presenter
    RFRobotControl* m_pRobotControl;

    // ==========================
    // MARK: - Private Functions
   private:
    /// @brief Connects signals of the window with slot functions of this
    /// presenter
    void connectWindowToThis();

    /// @brief Connects signals of this presenter with slot functions of the
    /// window
    void connectThisToWindow();

    /// @brief Connects signals of this presenter with slot functions of the
    /// controller model
    void connectThisToController();

    /// @brief Connects signals of the controller with slot functions of this
    /// presenter
    void connectControllerToThis();

    /// @brief Connects signals of the controller with slot functions of the
    /// window
    void connectControllerToWindow();

    /// @brief Connects signals of the window with slot functions of the
    /// controller
    void connectWindowToController();

    // ==========================
    // MARK: - Public Functions
   public:
    /// @brief Konstruiert a presenter between a window and a robot control
    RFRobotControlPresenter(MainWindow* window, RFRobotControl* robotControl);

    // ==========================
    // MARK: - Public Signals
   signals:

    // View Signals
    // For Documentation see MainWindow::Slot-Functions.
    void sgn_appendNewStatus(std::string newStatus);

    // Model Signals
    // For Documentation see RFRobotControl::Slot-Functions.
    void sgn_savePoint(std::string name);

    void sgn_setGlobalOverride(double newValue);

    void sgn_addPointName(string pointName);

    // ==========================
    // MARK: - Public Slots
   public slots:

    void slot_teachButtonClicked(std::string pointName);

    void slot_newRobotStatusMessage(RFStatusMessageType type,
                                    std::string statusDescription);

    void slot_overrideSlider_valueChanged(int newValue);
};

}  // namespace rf

#endif /* rf_robot_control_presenter_hpp */
