/// @file rf_gripper.h
///
/// @author Dwayne Steinke
/// @date 15.11.19
///
/// @brief Class decleration of RFGripper
///
/// rf_gripper.h
/// rf_robot_control
///
/// Created by Dwayne Steinke on 15.11.19 for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.
///
///

#ifndef RF_GRIPPER_H
#define RF_GRIPPER_H

#include <actionlib/client/simple_action_client.h>
#include <ros/ros.h>

#include <stdexcept>

#include "rf_robot_control/rf_command.h"
#include "franka_gripper/GraspAction.h"
#include "franka_gripper/HomingAction.h"
#include "franka_gripper/MoveAction.h"
#include "franka_gripper/StopAction.h"

namespace rf {

/// @brief Class to interact with a loaded franka gripper node
///
/// This class implements SimpleActionServers to interact with
/// a franka_gripper_node of franka_ros.
///
/// It mimics the commands from libfranka.
///
class RFGripper : public RFCommand {
    typedef actionlib::SimpleActionClient<franka_gripper::MoveAction>
        MoveActionClient;

    typedef actionlib::SimpleActionClient<franka_gripper::HomingAction>
        HomingActionClient;

    typedef actionlib::SimpleActionClient<franka_gripper::GraspAction>
        GraspActionClient;

    typedef actionlib::SimpleActionClient<franka_gripper::StopAction>
        StopActionClient;

   private:
    MoveActionClient m_moveActionClient;
    HomingActionClient m_homingActionClient;
    GraspActionClient m_graspActionClient;
    StopActionClient m_stopActionClient;

    ///@brief Maximum Openening width in m
    const double m_MAX_OPENING = 0.08;

    /// @brief Default opening, closing speed in m/s
    const double m_DEFAULT_SPEED = 0.05;

    /// @brief Default applied force whilst grasping/closing in N
    const double m_DEFAULT_FORCE = 20;

    /// @brief Use gripper in async mode
    /// In async mode the results of the actions are not checked.
    /// Function calls return immediately.
    const bool m_ASYNC;

   public:
    /// @brief Constructor
    ///
    /// @param nodeHandle The nodeHandle to use
    /// @param async Use async mode. In async mode the
    /// results of the actions are not checked.
    /// Function calls return immediately.
    RFGripper(ros::NodeHandle &nodeHandle, bool async = false);

    virtual ~RFGripper();

    /// @brief Call this function if you want to execute the command with the
    /// set parameters
    ///
    /// @todo Currently does nothing
    virtual void execute() override;

    /// @brief Moves the gripper fingers to a specified width.
    ///
    /// If the width is not reachable the command throws
    /// an error. Don't use this command to grasp an object.
    /// @see grasp().
    ///
    /// @param width Intended opening width. [m]
    /// @param speed Closing speed. [m/s]
    ///
    /// @todo What are the bounds of the parameters?
    ///
    /// @throws std::runtime_error RFGripper::move failed
    void move(double width, double speed);

    /// @brief Homes the Gripper and sets min and max values
    ///
    /// Dont call this in the middle of the program execution
    /// unless you changed your gripper.
    ///
    /// The gripper will open and close once. Make shure nothing
    /// is between the gripper.
    ///
    /// @throws std::runtime_error RFGripper::homing failed
    void homing();

    /// @brief Grasp an Object expected with a certain width and apply a certain
    /// force
    ///
    /// Moves the gripper with a desired speed to the
    /// desired width and applies a desired force.
    /// The operation is successful if the distance
    /// d between the gripper fingers is:
    /// width−epsilon_inner<d<width+epsilon_outer.
    ///
    /// @param width Expected object width in m
    /// @param speed Speed in m/s to move with
    /// @param force Force in N.
    /// @param epsilon_inner Lower success boundary in m (default: 0.005)
    /// @param epsilon_outer Upper success boundary in m (default: 0.005)
    ///
    /// @throws std::runtime_error RFGripper::grasp failed
    void grasp(double width, double speed, double force,
               double epsilon_inner = 0.005, double epsilon_outer = 0.005);

    /// @brief Stop current applied gripper command
    ///
    /// This command can be used for example
    /// to stop applying forces to an object after
    /// grasp.
    ///
    /// @throws std::runtime_error RFGripper::stop failed
    void stop();

    /// @brief Open the gripper to width m_MAX_OPENING
    ///
    /// A negativ speed is treated as m_DEFAULT_SPEED
    ///
    /// @param speed Optional speed in m/s to move with (Default:
    /// m_DEFAULT_SPEED)
    ///
    /// @throws std::runtime_error RFGripper::move failed
    void open(double speed = -1.0);

    /// @brief Close the gripper and grasp an object
    ///
    /// Use this function if you want to grasp an object
    /// and dont know or care about correct size.
    ///
    /// @attention The grasp intervall will be the whole
    /// gripper range. Therefore the execution will
    /// accept all widths at which a force feedback to the gripper
    /// is detected as a success. If you know your object and
    /// want to make shure your object is grasp properly, better
    /// use grasp().
    ///
    /// A negativ speed or force is treated
    /// as m_DEFAULT_SPEED or m_DEFAULT_FORCE.
    ///
    /// @param force Optional force in N (Default: m_DEFAULT_FORCE)
    /// @param speed Optional speed in m/s to move with (Default:
    /// m_DEFAULT_SPEED)
    ///
    /// @throws std::runtime_error RFGripper::grasp failed
    void close(double force = -1.0, double speed = -1.0);
};

}  // namespace dh

#endif
