/// @file rf_testprogram.h
///
/// @author Tom Hattendorf
/// @date 08.10.2021
///
/// @brief
///
/// rf_testprogram.h
/// rf_robot_control
///
/// Created by Tom Hattendorf on 08.10.2021 for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.
///
///

#ifndef RF_TESTPROGRAM_H
#define RF_TESTPROGRAM_H

#include <ros/ros.h>

#include "geometry_msgs/PoseStamped.h"
#include "rf_command.h"
#include "rf_robot_control/rf_controller.h"
#include "rf_robot_control/rf_gripper.h"
#include "rf_robot_control/rf_lin.h"
#include "rf_robot_control/rf_mtc.h"
#include "rf_robot_control/rf_ptp.h"
#include <actionlib/client/simple_action_client.h>

namespace rf
{

    /// @brief test program for development
    ///
    /// This RFCommand is only for testing purposes
    /// and will be changed a lot.
    class RFTestProgram : public RFCommand
    {

    public:
        RFTestProgram(ros::NodeHandle &nodeHandle);
        virtual ~RFTestProgram();
        void execute() override;
    };

} // namespace rf

#endif /* rf_testprogram_h */
