/// @file rf_mtc.h
///
/// @author Tom Hattendorf
/// @date 08.10.2021
///
/// @brief Class decleration of RFMTC
///
/// rf_mtc.h
/// rf_robot_control
///
/// Created by Tom Hattendorf on 08.10.2021 for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.
///
///

#ifndef RF_MTC_H
#define RF_MTC_H

#include <actionlib/client/simple_action_client.h>
#include <math.h>
#include <ros/ros.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Int16.h>

#include <stdexcept>

#include "rf_move_to_contact/MoveToContactAction.h"
#include "rf_robot_control/RFExecutionStatus.h"
#include "rf_robot_control/rf_command.h"
#include "rf_robot_control/rf_config_defines.h"
#include "rf_robot_control/rf_controller.h"

namespace rf {

/// @brief Move linear from one cartesian point to another.
///
/// This class wraps service calls to the ros_control controller_manager
/// and action calls to a RFCartesianMotionGenerator.
///
/// @attention This class requires, that the controller manager is running
/// and a RFCartesianMotionGenerator is loaded.
///
/// It supports movement to absolute poses or relative movement
/// from the current position.
///
/// The duration of the motion depends on the parameters
/// for maximum velocity, maximum acceleration and override.
/// Furthermore, the global override parameter is supported.
class RFMTC : public RFCommand {
    typedef actionlib::SimpleActionClient<
        rf_move_to_contact::MoveToContactAction>
        Client;

    typedef std::vector<controller_manager_msgs::ControllerState>
        ControllerStates;

    /// @name Motion execution parameters
    /// @{


    /// @brief Time scale/override for translation and rotation
    std::vector<double> m_override;

    /// @brief Max speed for translation [m/s] and rotation [rad/s]
    std::vector<double> m_dX_max;

    /// @brief Max acceleration for translation [m/s²] and rotation [rad/s²]
    std::vector<double> m_ddX_max;

    /// @name Execution managment and lifecycle
    /// @{

    /// @brief A SimpleActionClient to talk to the motion generator
    Client m_client;

    /// @brief The pursued goal
    rf_move_to_contact::MoveToContactGoal m_goal;

    /// @brief The controller was launched through the #m_controller
    /// of this class
    ///
    /// If m_controllerDidStartHere it will stop the launched
    /// controller if the execution is done.
    bool m_controllerDidStartHere = false;

    /// @brief Controller configurator to talk to the ros_control controller
    /// manager
    RFController m_controller;

    /// @brief Subscriber of the execution Status
    /// This makes it possible to abort the execution e.g.
    /// if a user stopped is pressed.
    ros::Subscriber m_executionStatusSubscriber;

    /// @brief Publisher of the control mode. Required for using the simulation environment
    ros::Publisher m_controlModePublisher;

    /// @brief The latest executionStatus recieved from the
    /// #m_executionStatusSubscriber
    ///
    /// @see RFExecutionStatus.msg for a description of possible status
    uint8_t m_executionStatus = -1;

    /// @brief Bool flag if motion is considered in EE-frame or base frame
    bool m_useEECoordinates = true;

    /// @}

    /// @brief Send the latest m_goal and wait for a result
    ///
    /// This function also checks m_executionStatus with 100 Hz
    /// and aborts the goal if m_executionStatus != OK.
    /// If this is the case it throws an std::runtime error
    /// and stops the controller if necessary.
    ///
    /// @throws std::runtime_error
    void pursueGoal();

    /// @brief Callback function for the execution status
    ///
    /// Writes the latest status to m_executionStatus;
    ///
    /// @see m_executionStatusSubscriber
    void cb_executionStatusUpdate(rf_robot_control::RFExecutionStatus msg);

   public:
    /// @brief Konstruktor
    ///
    /// Sets all parameters to default values @see  resetToDefaultValues()
    ///
    /// Uses Macros rf_POINTS_PATH_PREFIX, rf_LIN_ACTION_NAME and
    /// rf_LIN_MOTION_GENERATOR_NAME: @see rf_config_defines.h
    RFMTC(ros::NodeHandle &nodeHandle);

    /// @brief Destruktor
    virtual ~RFMTC();

    /// @brief Prepare the exceution of the motion command
    ///
    /// Check if all required parameters are set and write
    /// them into m_goal
    ///
    /// Also loads globalOverride from rosparam and
    /// multiplies it with the explicitly set local override.
    ///
    /// @throws std::runtime_error Parameters not set
    virtual void prepare() override;

    /// @brief Execute motion command with latest configuration
    ///
    /// Calls prepare() if unprepared, starts the controller if necessary,
    /// sends and pursues a goal and stopps the controller if necessary
    ///
    /// Thows an std::runtime error if something of the steps
    /// failed and the motion was either not started or
    /// not fully finished. (e.g. controller not startable,
    /// Reflex or user stop triggerd etc.)
    ///
    /// @throws std::runtime_error Motion failed
    virtual void execute() override;

    ///@name Setting values
    ///@{

    /// @brief Set a MTC motion in a desired direction, that is to be performed until a force threshold is exceeded
    ///
    /// @param x Movement distance in x direction
    /// @param y Movement distance in y direction
    /// @param z Movement distance in z direction
    /// @param contactForce Contact force threshold (absolute value of all directions considered)
    /// @param useEECoordinates Bool flag, wether the input directions are given in EE-frame or base frame (default: EE frame)
    void moveToContact(double x, double y, double z, double contactForce, bool useEECoordinates = true);

    /// @brief Reset velocity, acceleration and override to their default
    /// values;
    ///
    /// The default values are:
    /// vmax = [0.25 m/s, 0.5 rad/s]
    /// amax = [1 m/s², 2 m/s²]
    /// override = [1,1]
    void resetToDefaultValues();

    /// @brief Set the override parameters
    ///
    /// The override factor is a percentaged value from 0 to 1,
    /// that scales the acceleration and speed.
    ///
    /// @attention It is accepted to set the override values greater
    /// than 1. However overrides larger than 1 are discouraged,
    /// because your motion may be to fast.
    /// Change your acceleration and speed instead.
    /// Overrides greater than 1.5 are not accepted.
    ///
    /// Value of 1 => Normal
    /// Value of 0.5 => Half as fast
    void setOverride(double translationOverride, double rotationOverride);

    ///@brief Set both override parameters to the same value
    void setOverride(double override);

};

}  // namespace dh
#endif /* rf_ptp_hpp */
