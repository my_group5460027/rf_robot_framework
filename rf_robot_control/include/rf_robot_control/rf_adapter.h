/// @file rf_adapter.hpp
///
/// @author Hannes Südkamp
/// @date 14.01.21
///
/// @brief Class definition of RFAdapter
///
/// rf_adapter_h.h
/// rf_robot_control
///
/// Created by Hannes Südkamp on 14.01.21 for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.
///
///

#ifndef rf_adapter_h
#define rf_adapter_h

#include <actionlib/server/simple_action_server.h>
#include <franka_gripper/GraspAction.h>
#include <franka_gripper/HomingAction.h>
#include <franka_gripper/MoveAction.h>
#include <franka_gripper/StopAction.h>
#include <franka_msgs/FrankaState.h>
#include <ros/ros.h>

namespace rf {
/// @brief Abstract interface class for all possible adapters
///
/// This class is an abstract interface for all adapters that are
/// needed to connect to the franka-ros-framework
///
/// It provides gerneric functions and parameters that every adapter
/// needs, to communicate with the franka-ros-framework. But there
/// is no template for communication with the new robot. Furthermore,
/// there is no implementation of move commands, since these are only
/// needed if the controllers are not compatible with each other.
///
/// Only the constuctor of the class can be called, this starts the
/// action server and subscribes the necessary topics. Each adapter
/// must start the action servers for the gripper functions. The
/// implementation of the callback functions is adapter specific.
/// Furthermore, a publisher for /franka_state is mandatory.
///
/// Two functions are set as a guideline for converting to FrankaState.
/// handleFrankaState assembles the individual pieces of information
/// needed for FrankaState. publishFrankaState publishs the message.
class RFAdapter {
   protected:
    /// @brief A ros node handle for interaction with topics and services
    ros::NodeHandle m_NodeHandle;
    /// @brief A ros publisher for interaction FrankaState
    ros::Publisher m_frankaStatePublisher;
    /// @brief FrankaState which get published on m_frankaStatePublisher
    franka_msgs::FrankaState m_frankaState;

   private:
    /// @brief Manages the update of FrankaState
    ///
    /// Saves the message in m_frankaState and calls publishFrankaState()
    void handleFrankaState();
    /// @brief publish FrankaState
    void publishFrankaState();

    /// @brief action server for the stop action
    actionlib::SimpleActionServer<franka_gripper::StopAction>
        stop_action_server;
    /// @brief action server for the move action
    actionlib::SimpleActionServer<franka_gripper::MoveAction>
        move_action_server;
    /// @brief action server for the grasp action
    actionlib::SimpleActionServer<franka_gripper::GraspAction>
        grasp_action_server;
    /// @brief action server for the homing action
    actionlib::SimpleActionServer<franka_gripper::HomingAction>
        homing_action_server;

    /// @brief callbackfunction for the stop action
    virtual bool homing(
        const franka_gripper::HomingGoalConstPtr& goal,
        ros::NodeHandle* node_handle,
        actionlib::SimpleActionServer<franka_gripper::HomingAction>* as_);
    /// @brief callbackfunction for the move action
    virtual bool move(
        const franka_gripper::MoveGoalConstPtr& goal,
        ros::NodeHandle* node_handle,
        actionlib::SimpleActionServer<franka_gripper::MoveAction>* as_);
    /// @brief callbackfunction for the grasp action
    virtual bool grasp(
        const franka_gripper::GraspGoalConstPtr& goal,
        ros::NodeHandle* node_handle,
        actionlib::SimpleActionServer<franka_gripper::GraspAction>* as_);
    /// @brief callbackfunction for the homing action
    virtual bool stop(
        const franka_gripper::StopGoalConstPtr& goal,
        ros::NodeHandle* node_handle,
        actionlib::SimpleActionServer<franka_gripper::StopAction>* as_);

   public:
    /// @brief Constructor - Initialize and start Action Server, Publisher und
    /// Subscriber here
    RFAdapter();
    /// @brief Destructor - Stop Action Server
    ~RFAdapter();
};
}  // namespace dh
#endif /* rf_ur_adapter_hpp */
