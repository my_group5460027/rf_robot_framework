/// @file rf_stream.h
///
/// @author Dwayne Steinke
/// @date 13.11.19
///
/// @brief Preprocessor macros for use within RFRobotControl
///
/// rf_stream.h
/// rf_robot_control
///
/// Created by Dwayne Steinke on 14.10.19 for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.
///
///
///
/// In this file are preprocessor macros in the style of
/// ROS_..._STREAM
/// @attention These macros shall only be used inside the class RFRobotControl
///
/// All of these macros call ROS_..._STREAM with the argument stream.
/// They also print the stream in the status message text field of the GUI.

#ifndef RF_STREAM_H
#define RF_STREAM_H

#define RF_STREAM(stream)           \
    std::stringstream stateMessage; \
    stateMessage << stream;         \
    emit sgn_newStatus(type, stateMessage.str());

#define RF_DEBUG_STREAM(stream)                                \
    {                                                          \
        ROS_DEBUG_STREAM(stream);                              \
        RFStatusMessageType type = RFStatusMessageType::Debug; \
        RF_STREAM(stream);                                     \
    }

#define RF_INFO_STREAM(stream)                                \
    {                                                         \
        ROS_INFO_STREAM(stream);                              \
        RFStatusMessageType type = RFStatusMessageType::Info; \
        RF_STREAM(stream);                                    \
    }

#define RF_WARN_STREAM(stream)                                   \
    {                                                            \
        ROS_WARN_STREAM(stream);                                 \
        RFStatusMessageType type = RFStatusMessageType::Warning; \
        RF_STREAM(stream);                                       \
    }

#define RF_ERROR_STREAM(stream)                                \
    {                                                          \
        ROS_ERROR_STREAM(stream);                              \
        RFStatusMessageType type = RFStatusMessageType::Error; \
        RF_STREAM(stream);                                     \
    }

#define RF_FATAL_STREAM(stream)                                \
    {                                                          \
        ROS_FATAL_STREAM(stream);                              \
        RFStatusMessageType type = RFStatusMessageType::Fatal; \
        RF_STREAM(stream);                                     \
    }

#endif
