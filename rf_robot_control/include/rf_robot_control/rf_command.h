/// @file rf_command.hpp
///
/// @author Dwayne Steinke
/// @date 21.10.19
///
/// @brief Class definition of RFCommand
///
/// rf_command.h
/// rf_robot_control
///
/// Created by Dwayne Steinke on 21.10.19 for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.
///
///

#ifndef rf_command_hpp
#define rf_command_hpp

#include <ros/ros.h>
#include <stdio.h>

namespace rf {

/// @brief Abstract interface class for all possible commands
///
/// This class is an abstract interface for all commands that are
/// executable with RFRobotControl.
///
/// It provides gerneric functions and parameters that every commands
/// needs.
///
/// The two main functions are execute() and prepare().
/// Every Command has to implement its execute() function.
/// This function is called when the command should be really
/// executed.
///
/// The prepare() function is optional in a derived class and
/// should handle preperation befor the execute() function is called.
/// If you design you prepare() function well,
/// it can be called in advance in another thread without blocking
/// the program.
///
/// execute() and prepare() don't have parameters to be generic.
/// If you need specific parameters for you command, implement
/// set and get functions in the subclass.
///
/// Summary: If you use/design a command always expect the call order
/// off prepare() => execute().
///
///
///
/// You could also implement an automatic prepare() call in execute()
/// if prepare() is not computation heavy.
/// If its just a simple command its also possible to not use prepare()
/// at all.
///
/// @attention Keep in mind that you have to guard
/// your paramters with Mutexes, if you use callback functions
/// in subscription or services.
/// This is a multithreaded application.
/// The callback queue and the node handle may live in another thread
/// and therefore can call these callbacks whilst
/// execute() and prepare() are running.
class RFCommand {
   protected:
    /// @brief A ros node handle for interaction with topics and services
    ros::NodeHandle *m_pNodeHandle;

    // @brief Prepare function was called;
    bool m_isPrepared = false;

    std::string m_name;

   public:
    /// @brief Constuctor - Does nothing special
    RFCommand(ros::NodeHandle &nodeHandle);

    /// @brief Destruktor - Does nothing special
    virtual ~RFCommand();

    /// @brief Call this function if you want to execute the command with the
    /// set parameters
    virtual void execute() = 0;

    /// @brief Prepare execution - default: do nothing
    virtual void prepare();

    /// @brief Reload all params from rosparam
    ///
    /// @attention This function is not implemented.
    virtual void reloadParams();

    std::string getName();
};

}  // namespace dh

#endif /* rf_command_hpp */
