/// @file mtQR.h
///
/// @author Anais Millan Cerezo
/// @date 21.07.22
///
/// @brief
///
/// rf_mtQR.cpp
/// rf_robot_control
///
/// Created by Anais Millan Cerezo on 21.07.2022 for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.
///

#ifndef RF_MTQR_H
#define RF_MTQR_H

#include <ros/ros.h>
#include "geometry_msgs/PoseStamped.h"
#include "rf_command.h"
#include "rf_robot_control/rf_controller.h"
#include "rf_robot_control/rf_gripper.h"
#include "rf_robot_control/rf_lin.h"
#include "rf_robot_control/rf_mtc.h"
#include "rf_robot_control/rf_ptp.h"
#include <actionlib/client/simple_action_client.h>
#include <vector>

namespace rf{

/// @brief //Move to QR code

class RFMTQR : public RFCommand{

private: 
    ros::Subscriber postion_sub;
    ros::Subscriber status_sub; 
    int camera_feedback;
    bool camera_status; 
    double x_current = 0;
    double y_current = 0;
    double z_current = 0;
    double x_quat = 0;
    double y_quat = 0;
    double z_quat = 0;
    double w_quat = 0;
public:
    RFMTQR(ros::NodeHandle &nodeHandle);
    virtual ~RFMTQR();
    void execute(){return;} 
    std::vector<geometry_msgs::PoseStamped::ConstPtr> pose;
    void PoseCallback(const geometry_msgs::PoseStamped::ConstPtr &msg);
    void StatusCallback(const std_msgs::Int16 &status);
    void CAMERA_REC();
    void EE_T_QR(double x_cam, double y_cam, double z_cam);//Transformation von EE to QR code bestimmen
    Eigen::Matrix<double, 4, 1> EE_QR; // Translation von EE to QR 
    double roll_deg; //Rotation in degree around fixed x axis
    double pitch_deg; //Rotation in degree around fixed y axis
    double yaw_deg; //Rotation in degree around fixed z axis
    };
}// namespace rf


#endif/*rf_mtQR_h*/