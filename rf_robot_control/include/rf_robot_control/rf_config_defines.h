/// @file rf_config_defines.h
///
/// @author Dwayne Steinke
/// @date 06.11.19
///
/// @brief Preprocessor parameter definitions for compilation
///
/// rf_config_defines.h
/// rf_robot_control
///
/// Created by Dwayne Steinke on 06.11.19 for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.
///
///

#ifndef RF_CONFIG_DEFINES_H
#define RF_CONFIG_DEFINES_H

/// @brief Name of the MotionGenerator RFPTP uses
/// It will ask the controller manager to load a controller with this name.
#define RF_PTP_MOTION_GENERATOR_NAME "rf_ptp_motion_generator"

/// @brief Name of the Action the ActionClient of RFPTP uses to send commands to
/// the controller
#define RF_PTP_ACTION_NAME "/rf_ptp_motion_generator/follow_joint_trajectory"

/// @brief Rosparam path where point save are found
///
/// Expected with trailing slash
#define RF_POINTS_PATH_PREFIX "points/"

/// @brief Name of the joints to initialise a std::vector<std::string>
//#define RF_JOINT_NAME_INITIALIZER {"elbow_joint", "shoulder_lift_joint", \
    "shoulder_pan_joint", "wrist_1_joint", "wrist_2_joint", "wrist_3_joint"}
// #define RF_JOINT_NAME_INITIALIZER {"panda_joint1", "panda_joint2", "panda_joint3", "panda_joint4", \
//      "panda_joint5", "panda_joint6", "panda_joint7"};

/// @brief Name of the MotionGenerator RFLIN uses
/// It will ask the controller manager to load a controller with this name.
#define RF_LIN_MOTION_GENERATOR_NAME "rf_lin_motion_generator"

/// @brief Name of the Action the ActionClient of RFLIN uses to send commands to
/// the controller
#define RF_LIN_ACTION_NAME "MoveLinearCartesian"

/// @brief Name of the MotionGenerator RFMTC uses
/// It will ask the controller manager to load a controller with this name.
#define RF_MTC_MOTION_GENERATOR_NAME "rf_move_to_contact"

/// @brief Name of the Action the ActionClient of RFMTC uses to send commands to
/// the controller
#define RF_MTC_ACTION_NAME "MoveToContact"

/// @brief Name of the global override parameter on rosparam
#define RF_GLOBAL_OVERRIDE_NAME "/override"

#endif
