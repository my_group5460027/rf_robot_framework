/// @file rf_robot_control_utility.h
///
/// @author Dwayne Steinke
/// @date 23.01.20
///
/// @brief
///
/// rf_robot_control_utility.h
/// rf_robot_control
///
/// Created by Dwayne Steinke on 23.01.20 for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.
///
///

#ifndef RF_ROBOT_CONTROL_UTILITY_H
#define RF_ROBOT_CONTROL_UTILITY_H

#include "rf_command.h"
#include <ros/ros.h>
#include <stdexcept>

#include "franka_msgs/SetJointImpedance.h"
#include "franka_msgs/SetCartesianImpedance.h"
#include "franka_msgs/SetEEFrame.h"
#include "franka_msgs/SetKFrame.h"
#include "franka_msgs/SetForceTorqueCollisionBehavior.h"
#include "franka_msgs/SetFullCollisionBehavior.h"
#include "franka_msgs/SetLoad.h"

namespace rf {



class RFRobotControlUtility : public RFCommand {

	ros::ServiceClient m_jointImpedanceClient;

	ros::ServiceClient m_cartesianImpedanceClient;

	ros::ServiceClient m_EEFrameClient;

	ros::ServiceClient m_KFrameClient;

	ros::ServiceClient m_forceTorqueCollisionBehaviorClient;

	ros::ServiceClient m_fullCollisionBehaviorClient;

	ros::ServiceClient m_loadClient;


public:
	RFRobotControlUtility(ros::NodeHandle &nodeHandle);
	virtual ~RFRobotControlUtility();

	void execute() override;


	/// @brief Set the Joint Impedance in the internal controller
	///
	/// Accepted values are between 0 and 5000
	/// Estimated control default values are 1000
	/// @todo which unit is that in?
	///
	/// @attention Pressing the user stop seems to resets the impedance values
	/// Also keep in mind that very small values lead to a enourmous path deviation.
	///
	/// libfranka will also check the arguments and reject them, if they are
	/// invalid.
	///
	/// @param Joint impedance values for q1 to q7
	///
	/// @throws std::runtime_error("RFRobotControlUtility::setJointImpedance - Values out of range.");
	/// @throws std::runtime_error("RFRobotControlUtility::setJointImpedance service call does not exist!")
	/// @throws std::runtime_error("RFRobotControlUtility::setJointImpedance - Service call not succesfull.")
	void setJointImpedance(boost::array<double,7> newImpedances);



	/// @brief Set the Cartesian Impedance for (x, y, z, roll, pitch, yaw) in the internal controller.
	///
	/// User-provided torques are not affected by this setting.
	///
	/// Accepted values are between 0 and 3000 for translation and
	/// 0 an 500 for rotation.
	///
	/// Good standard values are between 1000 and 2000 for translation.
	///
	/// @todo which unit is that in?
	/// Limits according to Marvins research.
	///
	/// @param Cartesian impedance values (x, y, z, roll, pitch, yaw)
	///
	///
	/// @throws std::runtime_error("RFRobotControlUtility::setCartesianImpedance - Values out of range.")
	/// @throws std::runtime_error("RFRobotControlUtility::setCartesianImpedance - Service call does not exist.")
	/// @throws std::runtime_error("RFRobotControlUtility::setCartesianImpedance - Service call not succesfull.")
	void setCartesianImpedance(boost::array<double,6> newImpedances);


	/// @brief Changes the collision behavior.
	///
	/// Set common torque and force boundaries for
	/// acceleration/deceleration and constant velocity movement phases.
	///
	/// Forces or torques between lower and upper threshold are shown as
	/// contacts in the RobotState. Forces or torques above the upper
	/// threshold are registered as collision and cause the robot to stop moving.
	///
	/// Limit values for upperTorqueThresholds as in Desk:
	/// minTorqueLimit = {3, 3, 3, 2, 2, 1, 1};
	/// maxTorqueLimit = {100,100,100,80,80,40,40};
	///
	/// Limit values for upperForceThresholds as in Desk:
	/// boost::array<double,6> minForceLimits = {3, 3, 3, 1, 1, 1};
	/// boost::array<double,6> maxForceLimits = {100,100,100,30,30,30};
	///
	/// @param lowerTorqueThresholds Contact torque thresholds for each joint in [Nm]
	/// @param upperTorqueThresholds Collision torque thresholds for each joint in [Nm]
	/// @param lowerForceThresholds Contact force thresholds for (x,y,z,R,P,Y) in [N] / [Nm]
	/// @param upperForceThresholds Collision force thresholds for (x,y,z,R,P,Y) in [N]/[Nm]
	///
	///
	/// @throws std::runtime_error("RFRobotControlUtility::setCollisionBehaviour - lowerTorqueThresholds values out of range.")
	/// @throws std::runtime_error("RFRobotControlUtility::setCollisionBehaviour - upperTorqueThresholds values out of range.")
	/// @throws std::runtime_error("RFRobotControlUtility::setCollisionBehaviour - lowerForceThresholds values out of range.")
	/// @throws std::runtime_error("RFRobotControlUtility::setCollisionBehaviour - upperForceThresholds values out of range.")
	/// @throws std::runtime_error("RFRobotControlUtility::setCollisionBehaviour - Service call does not exist.")
	/// @throws std::runtime_error("RFRobotControlUtility::setCollisionBehaviour - Service call not succesfull.")
	///
	void setCollisionBehaviour(boost::array<double,7> lowerTorqueThresholds, boost::array<double,7> upperTorqueThresholds, boost::array<double,6> lowerForceThresholds, boost::array<double,6> upperForceThresholds);



};






} // namespace RF



#endif /* rf_ROBOT_CONTROL_UTILITY_H */
