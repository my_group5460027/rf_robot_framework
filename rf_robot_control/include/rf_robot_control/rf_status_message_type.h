/// @file rf_status_message_type.h
///
/// @author Dwayne Steinke
/// @date 11.11.19
///
/// @brief
///
///  rf_status_message_type.h
/// rf_robot_control
///
/// Created by Dwayne Steinke on 11.11.19 for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.
///
///

#ifndef RF_STATUS_MESSAGE_TYPE_H
#define RF_STATUS_MESSAGE_TYPE_H

enum class RFStatusMessageType { Debug, Info, Warning, Error, Fatal };

#endif
