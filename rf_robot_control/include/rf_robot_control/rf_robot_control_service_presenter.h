/// @file rf_robot_control_service_presenter.h
///
/// @author Dwayne Steinke
/// @date 30.01.20
///
/// @brief
///
/// rf_robot_control_service_presenter.h
/// rf_robot_control
///
/// Created by Dwayne Steinke on 30.01.20 for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.
///
///

#ifndef RF_ROBOT_CONTROL_SERVICE_PRESENTER_H
#define RF_ROBOT_CONTROL_SERVICE_PRESENTER_H

#include <QObject>

#include "rf_robot_control/rf_robot_control.h"

// Service includes
#include "rf_robot_control/MoveToPoint.h"
#include "rf_robot_control/RunProgram.h"
#include "rf_robot_control/SavePoint.h"
#include "rf_robot_control/SetGlobalOverride.h"
#include "std_srvs/Empty.h"

namespace rf {

/// @brief Presenter of a robot control
///
/// Connects and implements ros services  with the robot control
///
/// This presenter can be used as an alternative to
/// RFRobtoControlPresenter if no UI is required.
class RFRobotControlServicePresenter : public QObject {
    Q_OBJECT

    // ==========================
    // MARK: - Private Properties
   private:
    /// @brief model of the presenter
    RFRobotControl *m_pRobotControl;

    /// @brief The nodes nodeHandle
    ros::NodeHandle *m_pNodeHandle;

    ros::ServiceServer m_savePointServiceServer;

    ros::ServiceServer m_moveToPointServiceServer;

    ros::ServiceServer m_runProgramServiceServer;

    ros::ServiceServer m_acknowledgeErrorsServiceServer;

    ros::ServiceServer m_toggleGripperServiceServer;

    ros::ServiceServer m_setGlobalOverrideServiceServer;

    // ==========================
    // MARK: - Private Functions
   private:
    /// @brief Connects signals of this presenter with slot functions of the
    /// controller model
    void connectThisToController();

    /// @brief Connects signals of the controller with slot functions of this
    /// presenter
    void connectControllerToThis();

    void advertiseServices();

    /// @name ros-service-callbacks
    /// @{

    bool cb_savePoint(rf_robot_control::SavePoint::Request &req,
                      rf_robot_control::SavePoint::Response &res);

    bool cb_moveToPoint(rf_robot_control::MoveToPoint::Request &req,
                        rf_robot_control::MoveToPoint::Response &res);

    bool cb_runProgram(rf_robot_control::RunProgram::Request &req,
                       rf_robot_control::RunProgram::Response &res);

    bool cb_acknowledgeErrors(std_srvs::Empty::Request &req,
                              std_srvs::Empty::Response &res);

    bool cb_toggleGripper(std_srvs::Empty::Request &req,
                          std_srvs::Empty::Response &res);

    bool cb_setGlobalOverride(
        rf_robot_control::SetGlobalOverride::Request &req,
        rf_robot_control::SetGlobalOverride::Response &res);

    /// @}

    // ==========================
    // MARK: - Public Functions
   public:
    /// @brief Konstruiert a presenter between a window and a robot control
    RFRobotControlServicePresenter(RFRobotControl *robotControl);

    // ==========================
    // MARK: - Public Signals
   signals:

    // Model Signals
    // For Documentation see RFRobotControl::Slot-Functions.
    void sgn_savePoint(std::string name);

    void sgn_moveToPoint(std::string name);

    void sgn_runProgram(int number);

    void sgn_acknowledgeErrors();

    void sgn_toggleGripper();

    void sgn_setGlobalOverride(double newValue);

    // ==========================
    // MARK: - Public Slots
   public slots:

    void slot_teachButtonClicked(std::string pointName);
};

}  // namespace rf

#endif /* rf_ROBOT_CONTROL_SERVICE_PRESENTER_H */
