/// @file rf_lin.h
///
/// @author Dwayne Steinke
/// @date 19.12.19
///
/// @brief Class decleration of RFLIN
///
/// rf_lin.h
/// rf_robot_control
///
/// Created by Dwayne Steinke on 19.12.19 for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.
///
///

#ifndef RF_LIN_H
#define RF_LIN_H

#include <actionlib/client/simple_action_client.h>
#include <math.h>
#include <ros/ros.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Int16.h>

#include <stdexcept>
#include <Eigen/Core>
#include <Eigen/Geometry>

#include "rf_cartesian_motion_generator/MoveLinearCartesianAction.h"
#include "rf_robot_control/RFExecutionStatus.h"
#include "rf_robot_control/rf_command.h"
#include "rf_robot_control/rf_config_defines.h"
#include "rf_robot_control/rf_controller.h"

namespace rf
{

    /// @brief Move linear from one cartesian point to another.
    ///
    /// This class wraps service calls to the ros_control controller_manager
    /// and action calls to a RFCartesianMotionGenerator.
    ///
    /// @attention This class requires, that the controller manager is running
    /// and a RFCartesianMotionGenerator is loaded.
    ///
    /// It supports movement to absolute poses or relative movement
    /// from the current position.
    ///
    /// The duration of the motion depends on the parameters
    /// for maximum velocity, maximum acceleration and override.
    /// Furthermore, the global override parameter is supported.
    class RFLIN : public RFCommand
    {
        typedef actionlib::SimpleActionClient<
            rf_cartesian_motion_generator::MoveLinearCartesianAction>
            Client;

        typedef std::vector<controller_manager_msgs::ControllerState>
            ControllerStates;

        /// @name Motion execution parameters
        /// @{

        /// @brief Name of the loaded point
        /// Empty if point has no name eg. relative motion
        std::string m_pointName;

        /// @brief The used Path prefix on rosparam to load points from
        /// Search path for the point is:
        /// m_pathPrefix<name>/taskspace
        std::string m_pathPrefix;

        /// @brief Loaded point (Transformation Matrix O_T_EE) in column-major
        /// order
        std::vector<double> m_point;

        /// @brief Max speed for translation [m/s] and rotation [rad/s]
        std::vector<double> m_dX_max;

        /// @brief Max acceleration for translation [m/s²] and rotation [rad/s²]
        std::vector<double> m_ddX_max;

        /// @brief Time scale/override for translation and rotation
        std::vector<double> m_override;

        /// @brief Is the saved point a relativ offset or absolute coordinates
        bool m_pointIsRelative = false;

        /// @brief Is the releative offset in EECoordinates or global
        bool m_pointUsesEECoordinates = true;

        /// @brief Reference point for relative motion
        std::vector<double> m_referencePoint;

        /// @}

        /// @name Execution managment and lifecycle
        /// @{

        /// @brief A SimpleActionClient to talk to the motion generator
        Client m_client;

        /// @brief The pursued goal
        rf_cartesian_motion_generator::MoveLinearCartesianGoal m_goal;

        /// @brief The controller was launched through the #m_controller
        /// of this class
        ///
        /// If m_controllerDidStartHere it will stop the launched
        /// controller if the execution is done.
        bool m_controllerDidStartHere = false;

        /// @brief Controller configurator to talk to the ros_control controller
        /// manager
        RFController m_controller;

        /// @brief Subscriber of the execution Status
        /// This makes it possible to abort the execution e.g.
        /// if a user stopped is pressed.
        ros::Subscriber m_executionStatusSubscriber;

        /// @brief Publisher of the control mode. Required for using the simulation environment
        ros::Publisher m_controlModePublisher;

        /// @brief The latest executionStatus recieved from the
        /// #m_executionStatusSubscriber
        ///
        /// @see RFExecutionStatus.msg for a description of possible status
        uint8_t m_executionStatus = -1;

        /// @}

        /// @brief Send the latest m_goal and wait for a result
        ///
        /// This function also checks m_executionStatus with 100 Hz
        /// and aborts the goal if m_executionStatus != OK.
        /// If this is the case it throws an std::runtime error
        /// and stops the controller if necessary.
        ///
        /// @throws std::runtime_error
        void pursueGoal();

        /// @brief Callback function for the execution status
        ///
        /// Writes the latest status to m_executionStatus;
        ///
        /// @see m_executionStatusSubscriber
        void cb_executionStatusUpdate(rf_robot_control::RFExecutionStatus msg);

    public:
        /// @brief Konstruktor
        ///
        /// Sets all parameters to default values @see  resetToDefaultValues()
        ///
        /// Uses Macros rf_POINTS_PATH_PREFIX, rf_LIN_ACTION_NAME and
        /// rf_LIN_MOTION_GENERATOR_NAME: @see rf_config_defines.h
        RFLIN(ros::NodeHandle &nodeHandle);

        /// @brief Destruktor
        virtual ~RFLIN();

        /// @brief Prepare the exceution of the motion command
        ///
        /// Check if all required parameters are set and write
        /// them into m_goal
        ///
        /// Also loads globalOverride from rosparam and
        /// multiplies it with the explicitly set local override.
        ///
        /// @throws std::runtime_error Parameters not set
        virtual void prepare() override;

        /// @brief Execute motion command with latest configuration
        ///
        /// Calls prepare() if unprepared, starts the controller if necessary,
        /// sends and pursues a goal and stopps the controller if necessary
        ///
        /// Thows an std::runtime error if something of the steps
        /// failed and the motion was either not started or
        /// not fully finished. (e.g. controller not startable,
        /// Reflex or user stop triggerd etc.)
        ///
        /// @throws std::runtime_error Motion failed
        virtual void execute() override;

        /// @name Target point configuration and move commands
        /// @{

        /// @brief Get Point with name from rosparam to move to in execute()
        ///
        /// Search path for the point is:
        /// points/<name>/taskspace
        ///
        /// @param name Pointname without any /
        ///
        /// @throws std::runtime_error Could not get point from rosparam
        void setPoint(std::string name);

        /// @brief Transform relative movements into transformation matrix and convert degrees to radians
        ///
        /// @param x Relative x offset in m
        /// @param y Relative y offset in m
        /// @param z Relative z offset in m
        /// @param roll_deg Rotation in degree around fixed x axis
        /// @param pitch_deg Rotation in degree around fixed y axis
        /// @param yaw_deg Rotation  in degree around fixed z axis
        ///
        void transformPoint(double x, double y, double z, double roll_deg = 0.0, double pitch_deg = 0.0, double yaw_deg = 0.0);

        /// @brief Set the point relative to the latest position
        ///
        /// The reference position is the latest position at the
        /// time this command is invoked.
        ///
        /// The accepted offsets are limited to +- 1m for saftey reasons.
        ///
        /// Rotation convention used: z-y-x
        ///
        /// @param x Relative x offset in m
        /// @param y Relative y offset in m
        /// @param z Relative z offset in m
        /// @param useEECoordinates Are the oofsets specified in KS_0 or KS_EE?
        /// @param roll_deg Rotation in degree around fixed x axis
        /// @param pitch_deg Rotation in degree around fixed y axis
        /// @param yaw_deg Rotation  in degree around fixed z axis
        ///
        /// @throws std::runtime_error Limit exceeded
        void setRelativePoint(double x, double y, double z, bool useEECoordinates = true, double roll_deg = 0.0, double pitch_deg = 0.0, double yaw_deg = 0.0);

        /// @brief Set the point relative to a desired point
        ///
        /// The reference position is definded by the selected point
        ///
        /// The accepted offsets are limited to +- 1m for saftey reasons.
        ///
        /// Rotation convention used: z-y-x
        ///
        /// @param x Relative x offset in m
        /// @param y Relative y offset in m
        /// @param z Relative z offset in m
        /// @param useEECoordinates Are the oofsets specified in KS_0 or KS_EE?
        /// @param roll_deg Rotation in degree around fixed x axis
        /// @param pitch_deg Rotation in degree around fixed y axis
        /// @param yaw_deg Rotation  in degree around fixed z axis
        ///
        /// @throws std::runtime_error Limit exceeded
        void setRelativePoint(std::string name, double x, double y, double z, bool useEECoordinates = true, double roll_deg = 0.0, double pitch_deg = 0.0, double yaw_deg = 0.0);

        /// @brief Execute a motion to a point
        ///
        /// Combines setPoint() and execute()
        ///
        /// @param name Pointname without any /
        ///
        /// @throws std::runtime_error See setPoint() and execute()
        void moveToPoint(std::string name);

        /// @brief Execute a motion to a point named with a prefix and number
        ///
        /// Combines setPoint() and execute()
        ///
        /// The pointname is namePrefix + number (example: P1)
        /// This function can be used to iterate over motion commands in a loop,
        /// if all Points share the same prefix.
        ///
        /// @param name Pointname prefix withoutany /
        /// @param number Pointnumber added to prefix
        ///
        /// @throws std::runtime_error See setPoint() and execute()
        void moveToPoint(std::string namePrefix, int number);

        /// @brief Execute a relative motion
        ///
        /// Combines setRelativePoint() and execute()
        ///
        /// The accepted offsets are limited to +- 1m for saftey reasons.
        ///
        /// @param x Relative x offset in m
        /// @param y Relative y offset in m
        /// @param z Relative z offset in m
        /// @param useEECoordinates Are the oofsets specified in KS_0 or KS_EE?
        /// @param roll_deg Rotation in degree around fixed x axis
        /// @param pitch_deg Rotation in degree around fixed y axis
        /// @param yaw_deg Rotation  in degree around fixed z axis
        ///
        /// @throws std::runtime_error See setRelativePoint() and execute()
        void moveRelativeToPoint(double x, double y, double z, bool useEECoordinates = true, double roll_deg = 0.0, double pitch_deg = 0.0, double yaw_deg = 0.0);

        /// @brief Execute a relative motion
        ///
        /// Combines setRelativePoint() and execute()
        ///
        /// The accepted offsets are limited to +- 1m for saftey reasons.
        ///
        /// @param name Name of the taught point to move relative to
        /// @param x Relative x offset in m
        /// @param y Relative y offset in m
        /// @param z Relative z offset in m
        /// @param useEECoordinates Are the oofsets specified in KS_0 or KS_EE?
        /// @param roll_deg Rotation in degree around fixed x axis
        /// @param pitch_deg Rotation in degree around fixed y axis
        /// @param yaw_deg Rotation  in degree around fixed z axis
        ///
        /// @throws std::runtime_error See setRelativePoint() and execute()
        void moveRelativeToPoint(std::string name, double x, double y, double z, bool useEECoordinates = true, double roll_deg = 0.0, double pitch_deg = 0.0, double yaw_deg = 0.0);

        /// @}

        /// @}

        ///@name Setting values
        ///@{

        /// @brief Reset velocity, acceleration and override to their default
        /// values;
        ///
        /// The default values are:
        /// vmax = [0.25 m/s, 0.5 rad/s]
        /// amax = [1 m/s², 2 m/s²]
        /// override = [1,1]
        void resetToDefaultValues();

        /// @brief Set the override parameters
        ///
        /// The override factor is a percentaged value from 0 to 1,
        /// that scales the acceleration and speed.
        ///
        /// @attention It is accepted to set the override values greater
        /// than 1. However overrides larger than 1 are discouraged,
        /// because your motion may be to fast.
        /// Change your acceleration and speed instead.
        /// Overrides greater than 1.5 are not accepted.
        ///
        /// Value of 1 => Normal
        /// Value of 0.5 => Half as fast
        void setOverride(double translationOverride, double rotationOverride);

        ///@brief Set both override parameters to the same value
        void setOverride(double override);

        /// @brief Set the maximum speeds of the movement
        ///
        /// The speeds are set seperatly for translation and rotation.
        /// The translationSpeed is measured in m/s and the rotationSpeed in rad/s;
        /// Depending on the distance of the path and the acceleration,
        /// these speed values may never be reached.
        ///
        /// Limits as in https://frankaemika.github.io/docs/control_parameters.html
        ///
        /// translationSpeed < 1.7 m/s
        /// rotationSpeed < 2.5 rad/s
        ///
        ///
        void setSpeed(double translationSpeed, double rotationSpeed);

        /// @brief Set the maximum accelerations of the movement
        ///
        /// The accelerations are set speratly for translation and rotation.
        /// The translationAcc is measured in m/s² and the rotationAcc in rad/s²;
        /// Depending on the distance of the path,
        /// these acceleration values may never be reached.
        ///
        /// Limits as in https://frankaemika.github.io/docs/control_parameters.html
        ///
        /// translationAcc < 13 m/s²
        /// rotationAcc < 25 rad/s²
        ///
        ///
        void setAcceleration(double translationAcc, double rotationAcc);

        ///@}
    };

} // namespace dh
#endif /* rf_ptp_hpp */
