/// @file rf_coppelia_hardware_interface_node.h
///
/// @author Tom Hattendorf
/// @date 18.08.2021
///
/// @brief Node for RFCoppeliaHardwareInterface
///
/// rf_coppelia_hardware_interface_node.h
/// rf_coppeliasim_connectors
///
/// Created by Tom Hattendorf on 18.08.2021 for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.

#include <controller_manager/controller_manager.h>

#include "rf_coppeliasim_connector/rf_coppelia_hardware_interface.h"

rf::RFRobot robot;
std::string controlMode;
int simState;
bool guiding = false;

void guidingMode_CB(std_msgs::Bool msg) {
    guiding = msg.data;
    if (guiding) {
        controlMode = "GUIDING";
    } else {
        controlMode = "NONE";
    }
}

void simState_CB(std_msgs::Int16 msg) {
    if (msg.data == 1) {
        robot.startSimulation();
    } else {
        robot.stopSimulation();
    }
}

void controlMode_CB(std_msgs::Int16 msg) {
    if (guiding) {
        controlMode = "GUIDING";
    } else {
        if (msg.data == 0) {
            controlMode = "PTP";
        } else if (msg.data == 1) {
            controlMode = "LIN";
        } else {
            controlMode = "NONE";
        }
    }
}

int main(int argc, char** argv) {
    ros::init(argc, argv, "coppelia_hw_interface");

    ros::NodeHandle nh;

    ros::Subscriber simstateSubscriber =
        nh.subscribe("/rfSimStatus", 1, simState_CB);
    ros::Subscriber controlModeSubscriber =
        nh.subscribe("/rfControlMode", 1, controlMode_CB);
    ros::Subscriber guidingModeSubscriber =
        nh.subscribe("/rfGuidingMode", 1, guidingMode_CB);

    std::vector<std::string> jointNames;
    std::string tcpName;
    std::string baseName;
    nh.getParam("joint_names", jointNames);
    nh.getParam("tcp_name", tcpName);
    nh.getParam("base_name", baseName);
    robot.init(jointNames, tcpName, baseName);

    rf::RFCoppeliaHardwareInterface hw_interface(nh, robot);

    ros::AsyncSpinner spinner(0);
    spinner.start();

    controller_manager::ControllerManager cm(&hw_interface, nh);

    ros::Time timestamp;
    ros::Duration period;
    auto stopwatch_last = std::chrono::steady_clock::now();
    auto stopwatch_now = stopwatch_last;
    while (ros::ok()) {
        hw_interface.read(controlMode);

        timestamp = ros::Time::now();
        stopwatch_now = std::chrono::steady_clock::now();
        period.fromSec(
            std::chrono::duration_cast<std::chrono::duration<double>>(
                stopwatch_now - stopwatch_last)
                .count());
        stopwatch_last = stopwatch_now;

        if (controlMode == "LIN") {
            // Update CM 10x to simulate loop of 100 Hz
            for (int i = 0; i < 10; i++) {
                cm.update(timestamp, period);
            }
        } else {
            cm.update(timestamp, period);
        }
        hw_interface.write();

        robot.pingServer();  // Avoid shutdown from inactivity
        ros::Rate(100).sleep();
    }
    spinner.stop();

    return 0;
}