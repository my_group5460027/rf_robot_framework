/// @file rf_coppelia_hardware_interface.cpp
///
/// @author Tom Hattendorf
/// @date 18.08.2021
///
/// @brief Class implementation of RFCoppeliaHardwareInterface
///
/// rf_coppelia_hardware_interface.cpp
/// rf_coppeliasim_connector
///
/// Created by Tom Hattendorf for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.

#include "rf_coppeliasim_connector/rf_coppelia_hardware_interface.h"

namespace rf {

RFCoppeliaHardwareInterface::RFCoppeliaHardwareInterface(ros::NodeHandle& nh,
                                                         RFRobot& robot)
    : m_nodeHandle(nh),
      m_robot(robot),
      m_errorRecoveryServer(
          m_nodeHandle, "/franka_control/error_recovery",
          boost::bind(&RFCoppeliaHardwareInterface::errorRecovery, this, _1),
          false) {
    init();
}

RFCoppeliaHardwareInterface::~RFCoppeliaHardwareInterface() {
    m_robot.stopSimulation();
}

void RFCoppeliaHardwareInterface::init() {
    m_frankaStatePublisher.init(m_nodeHandle,
                                "/franka_state_controller/franka_states", 1);
    m_errorRecoveryServer.start();
    m_programStateSubscriber = m_nodeHandle.subscribe(
        "/rfProgramStatus", 1,
        &RFCoppeliaHardwareInterface::programStateSubscriber_CB, this);
    m_guidingCommandSubscriber = m_nodeHandle.subscribe(
        "/rfGuidingCommand", 1,
        &RFCoppeliaHardwareInterface::guidingCommandSubscriber_CB, this);

    m_nodeHandle.getParam("arm_id", m_armId);
    m_nodeHandle.getParam("joint_names", m_jointNames);

    m_numJoints = m_jointNames.size();

    setToZero();
    setupJointInterface();
    setupFrankaInterface();
    m_robot.runSynchronous();
}

bool RFCoppeliaHardwareInterface::errorRecovery(
    const franka_msgs::ErrorRecoveryGoalConstPtr& goal) {
    m_errorRecoveryServer.setSucceeded();
    return true;
}

void RFCoppeliaHardwareInterface::programStateSubscriber_CB(
    std_msgs::Int16 msg) {
    m_programState = msg.data;
}

void RFCoppeliaHardwareInterface::guidingCommandSubscriber_CB(
    sensor_msgs::JointState msg) {
    m_guidingCommand = msg.position;
}

void RFCoppeliaHardwareInterface::setToZero() {
    m_robotState.q[m_numJoints] = {0.0};
    m_robotState.dq[m_numJoints] = {0.0};
    m_cartesianVelocities[6] = {0.0};
    m_robotState.O_T_EE[16] = {0.0};
    m_programState = 0;
    m_guidingCommand.resize(m_numJoints);

    m_jointPositionsCommand.resize(m_numJoints);
    velInterface_dq.resize(m_numJoints);
    velInterface_q.resize(m_numJoints);
    for (int i = 0; i < m_numJoints; i++) {
        velInterface_dq.data[i] = 0.0;
    }
}

void RFCoppeliaHardwareInterface::setupJointInterface() {
    for (int i = 0; i < m_numJoints; i++) {
        hardware_interface::JointStateHandle jointStateHandle(
            m_jointNames[i], &m_robotState.q[i], &m_robotState.dq[i],
            &m_robotState.tau_J[i]);
        m_jointstateInterface.registerHandle(jointStateHandle);

        hardware_interface::JointHandle jointPositionHandle(
            jointStateHandle, &m_robotState.q_d[i]);
        m_positionJointInterface.registerHandle(jointPositionHandle);

        hardware_interface::JointHandle jointVelocityHandle(
            jointStateHandle, &m_robotState.dq_d[i]);
        m_velocityJointInterface.registerHandle(jointVelocityHandle);

        hardware_interface::JointHandle jointEffortHandle(
            jointStateHandle, &m_robotState.tau_J_d[i]);
        m_effortJointInterface.registerHandle(jointEffortHandle);
    }
    registerInterface(&m_jointstateInterface);
    registerInterface(&m_positionJointInterface);
    registerInterface(&m_velocityJointInterface);
    registerInterface(&m_effortJointInterface);
}

void RFCoppeliaHardwareInterface::setupFrankaInterface() {
    franka_hw::FrankaStateHandle franka_state_handle(m_armId + "_robot",
                                                     m_robotState);
    franka_state_interface.registerHandle(franka_state_handle);
    registerInterface(&franka_state_interface);

    franka_hw::FrankaCartesianVelocityHandle franka_cartesian_velocity_handle(
        franka_state_handle, m_cartesianVelocities, m_elbow);
    franka_velocity_interface.registerHandle(franka_cartesian_velocity_handle);

    registerInterface(&franka_velocity_interface);
}

void RFCoppeliaHardwareInterface::read(std::string controlMode) {
    m_robot.spinOnce();
    m_jointPositions = m_robot.getJointPositions();
    m_EePose = m_robot.getEndEffectorPose();
    m_simState = m_robot.getRobotMode();
    m_controlMode = controlMode;

    std::move(m_jointPositions.begin(), m_jointPositions.end(),
              m_robotState.q.begin());

    Eigen::Matrix3f mat3 =
        Eigen::Quaternionf(m_EePose[6], m_EePose[3], m_EePose[4], m_EePose[5])
            .toRotationMatrix();
    Eigen::Matrix4f mat4 = Eigen::Matrix4f::Identity();
    Eigen::Vector3f trans(m_EePose[0], m_EePose[1], m_EePose[2]);
    mat4.block(0, 0, 3, 3) = mat3;
    mat4.block<3, 1>(0, 3) = trans;

    for (int i = 0; i < 16; i++) {
        m_robotState.O_T_EE[i] = mat4(i);
        m_frankaState.O_T_EE[i] = mat4(i);
    }

    if (m_controlMode != "LIN") {
        for (int i = 0; i < m_numJoints; i++) {
            velInterface_q.data[i] = m_robotState.q[i];
        }
    }
    handleFrankaState();
}

void RFCoppeliaHardwareInterface::handleFrankaState() {
    for (int i = 0; i < m_numJoints; i++) {
        m_frankaState.q[i] = m_robotState.q[i];
        m_frankaState.q_d[i] = m_robotState.q_d[i];
        m_frankaState.dq[i] = m_robotState.dq[i];
        m_frankaState.dq_d[i] = m_robotState.dq_d[i];
        m_frankaState.tau_J[i] = m_robotState.tau_J[i];
    }

    if (m_simState == 0 && m_controlMode != "GUIDING") {
        m_frankaState.robot_mode = 0;
    } else if (m_simState == 16 && m_programState == 0) {
        m_frankaState.robot_mode = 1;
    } else if (m_simState == 16 && m_programState == 1) {
        m_frankaState.robot_mode = 2;
    } else if (m_simState == 0 && m_controlMode == "GUIDING") {
        m_frankaState.robot_mode = 3;
    } else if (m_simState == 8) {
        m_frankaState.robot_mode = 5;
    } else {
        m_frankaState.robot_mode = 0;
    }

    if (m_frankaStatePublisher.trylock()) {
        m_frankaStatePublisher.msg_ = m_frankaState;
        m_frankaStatePublisher.unlockAndPublish();
    }
}

void RFCoppeliaHardwareInterface::updateVelocity() {
    for (int k = 0; k < 3; k++) {
        velInterface_v.vel.data[k] = m_cartesianVelocities[k];
        velInterface_v.rot.data[k] = m_cartesianVelocities[k + 3];
    }
    m_kinematics.getVelocity(velInterface_q, velInterface_v, velInterface_dq);

    for (int i = 0; i < m_numJoints; i++) {
        velInterface_q.data[i] =
            velInterface_q.data[i] + velInterface_dq.data[i] * 0.01;
    }
}

void RFCoppeliaHardwareInterface::setJoints() {
    if (m_simState == 16) {
        m_robot.setJointTargetPositions(m_jointPositionsCommand);
        m_robot.stepSimulation();
    } else if (m_simState == 0) {
        m_robot.setJointPositions(m_jointPositionsCommand);
    }
}

void RFCoppeliaHardwareInterface::write() {
    if (m_controlMode == "PTP") {
        m_jointPositionsCommand.assign(m_robotState.q_d.begin(),
                                       m_robotState.q_d.end());
    } else if (m_controlMode == "LIN") {
        updateVelocity();
        for (int i = 0; i < m_numJoints; i++) {
            m_jointPositionsCommand[i] = velInterface_q.data[i];
        }
    } else if (m_controlMode == "GUIDING") {
        m_jointPositionsCommand = m_guidingCommand;
    }
    setJoints();
}

}  // namespace rf