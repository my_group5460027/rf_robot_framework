/// @file rf_coppelia_gripper_interface.cpp
///
/// @author Tom Hattendorf
/// @date 18.08.2021
///
/// @brief Class implementation of RFCoppeliaGripperInterface
///
/// rf_coppelia_gripper_interface.cpp
/// rf_coppeliasim_connector
///
/// Created by Tom Hattendorf for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.

#include "rf_coppeliasim_connector/rf_coppelia_gripper_interface.h"
namespace rf {

RFCoppeliaGripperInterface::RFCoppeliaGripperInterface(ros::NodeHandle& nh,
                                                       RFRobot& robot)
    : nh(nh),
      robot(robot),
      stop_action_server(
          nh, "/franka_gripper/stop",
          boost::bind(&RFCoppeliaGripperInterface::stop, this, _1), false),
      move_action_server(
          nh, "/franka_gripper/move",
          boost::bind(&RFCoppeliaGripperInterface::move, this, _1), false),
      grasp_action_server(
          nh, "/franka_gripper/grasp",
          boost::bind(&RFCoppeliaGripperInterface::grasp, this, _1), false),
      homing_action_server(
          nh, "/franka_gripper/homing",
          boost::bind(&RFCoppeliaGripperInterface::homing, this, _1), false) {
    stop_action_server.start();
    move_action_server.start();
    grasp_action_server.start();
    homing_action_server.start();
    robot.setJointPositions(m_maxOpening);
    robot.setJointTargetPositions(m_maxOpening);
    m_activeGoal = false;
}

RFCoppeliaGripperInterface::~RFCoppeliaGripperInterface() {}

bool RFCoppeliaGripperInterface::stop(
    const franka_gripper::StopGoalConstPtr& goal) {
    m_activeGoal = false;
    franka_gripper::StopResult result;
    result.success = true;
    stop_action_server.setSucceeded(result);
    return true;
}

bool RFCoppeliaGripperInterface::move(
    const franka_gripper::MoveGoalConstPtr& goal) {
    m_activeGoal = true;
    m_activeGoalPosition = goal->width / 2.0;
    m_activeGoalSpeed = goal->speed;
    robot.spinOnce();
    m_gripperPosition = robot.getJointPositions()[0];
    m_gripperStep = m_activeGoalSpeed * 0.01;
    franka_gripper::MoveResult result;

    if (actuateGripper() == true) {
        result.success = true;
        move_action_server.setSucceeded(result);
        return true;
    } else {
        result.error = true;
        move_action_server.setAborted();
        return false;
    }
}

bool RFCoppeliaGripperInterface::grasp(
    const franka_gripper::GraspGoalConstPtr& goal) {
    m_activeGoal = true;
    m_activeGoalPosition = goal->width / 2.0;
    m_activeGoalSpeed = goal->speed;
    robot.spinOnce();
    m_gripperPosition = robot.getJointPositions()[0];
    m_gripperStep = -m_activeGoalSpeed * 0.01;
    franka_gripper::GraspResult result;

    if (actuateGripper() == true) {
        result.success = true;
        grasp_action_server.setSucceeded(result);
        return true;
    } else {
        result.error = true;
        grasp_action_server.setAborted();
        return false;
    }
}

bool RFCoppeliaGripperInterface::homing(
    const franka_gripper::HomingGoalConstPtr& goal) {
    ROS_INFO_STREAM("HOMING IS NOT SUPPORTED YET");
    homing_action_server.setAborted();
    return false;
}
double RFCoppeliaGripperInterface::roundOff(double val, int prec) {
    float pow_10 = pow(10.0f, (float)prec);
    return round(val * pow_10) / pow_10;
}

bool RFCoppeliaGripperInterface::actuateGripper() {
    try {
        while (m_activeGoal == true) {
            
            m_gripperPosition = m_gripperPosition + m_gripperStep;
            if (roundOff(m_gripperPosition, 4) <= 0){
                m_gripperPosition = 0;
                m_activeGoal = false;
            } else if (roundOff(m_gripperPosition, 4) >= m_maxOpening) {
                m_gripperPosition = m_activeGoalPosition;
                m_activeGoal = false;
            }

            robot.setJointTargetPositions(m_gripperPosition);
            ros::Rate(100).sleep();
        }
        ros::Rate(1).sleep();
        return true;
    } catch (...) {
        ROS_WARN_STREAM("Could not actuate the gripper");
        return false;
    }
}
}  // namespace rf
