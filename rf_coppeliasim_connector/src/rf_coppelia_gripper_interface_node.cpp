/// @file rf_coppelia_gripper_interface_node.h
///
/// @author Tom Hattendorf
/// @date 18.08.2021
///
/// @brief Node for RFCoppeliaGripperInterface
///
/// rf_coppelia_gripper_interface_node.h
/// rf_coppeliasim_connector
///
/// Created by Tom Hattendorf on 18.08.2021 for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.

#include <ros/ros.h>

#include "rf_coppeliasim_connector/rf_coppelia_gripper_interface.h"

int main(int argc, char** argv) {
    ros::init(argc, argv, "coppelia_gripper_interface");

    ros::NodeHandle nh;
    rf::RFRobot robot;

    std::vector<std::string> grippernames;
    std::string tcpName;
    std::string baseName;
    nh.getParam("gripper_names", grippernames);
    nh.getParam("base_name", baseName);
    nh.getParam("tcp_name", tcpName);
    robot.init(grippernames, tcpName, baseName);
    rf::RFCoppeliaGripperInterface gripper(nh, robot);

    ros::spin();    

    return 0;
}