/// @file rf_kinematics.cpp
///
/// @author Tom Hattendorf
/// @date 18.08.2021
///
/// @brief Class implementation of RFKinematics
///
/// rf_kinematics.cpp
/// rf_coppeliasim_connector
///
/// Created by Tom Hattendorf for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.

#include "rf_coppeliasim_connector/rf_kinematics.h"
using namespace KDL;

namespace rf {
RFKinematics::RFKinematics() {
    ros::NodeHandle node;

    std::vector<double> a;
    std::vector<double> d;
    std::vector<double> alpha;
    std::string convention;
    node.getParam("dh_parameters/a", a);
    node.getParam("dh_parameters/d", d);
    node.getParam("dh_parameters/alpha", alpha);
    node.getParam("dh_parameters/convention", convention);
    int numParams = a.size();


    // Build Kinematic Chain from DH parameters depending on the used convention
    if (convention == "CRAIG") {
        m_chainEE.addSegment(
            Segment(Joint(Joint::None), Frame::DH_Craig1989(a[0], alpha[0], d[0], 0)));

        for (int k = 1; k < numParams; k++) {
            m_chainEE.addSegment(Segment(Joint(Joint::RotZ),
                                         Frame::DH_Craig1989(a[k], alpha[k], d[k], 0)));
        }
    } else if ("DH"){
        m_chainEE.addSegment(
            Segment(Joint(Joint::None), Frame::DH(a[0], alpha[0], d[0], 0)));

        for (int k = 1; k < numParams; k++) {
            m_chainEE.addSegment(Segment(Joint(Joint::RotZ),
                                         Frame::DH(a[k], alpha[k], d[k], 0)));
        }
    }

    this->velSolver = new KDL::ChainIkSolverVel_pinv(m_chainEE);
}

RFKinematics::~RFKinematics() { delete velSolver; }

int RFKinematics::getVelocity(const KDL::JntArray &q_in, const KDL::Twist &v_in,
                              KDL::JntArray &qdot_out) {
    auto ret = this->velSolver->CartToJnt(q_in, v_in, qdot_out);
    if (ret != 0) {
        std::cout << "Solver Error Nr. " << ret
                  << " | ErrorStr: " << this->velSolver->strError(ret)
                  << std::endl;
    }

    return ret;
}

}  // namespace rf