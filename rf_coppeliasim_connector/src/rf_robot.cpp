/// @file rf_robot.cpp
///
/// @author Tom Hattendorf
/// @date 18.08.2021
///
/// @brief Class implementation of RFRobot
///
/// rf_robot.cpp
/// rf_coppeliasim_connector
///
/// Created by Tom Hattendorf for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.

#include "rf_coppeliasim_connector/rf_robot.h"

namespace rf {

RFRobot::RFRobot() : m_client("b0RemoteApi_c++Client", "b0RemoteApiAddOn") {}

RFRobot::~RFRobot() {}

// Without Gripper
void RFRobot::init(std::vector<std::string> jointNames, std::string tcpName,
                   std::string baseName) {
    m_numJoints = jointNames.size();
    m_jointPositions.resize(m_numJoints);
    m_jointHandles.resize(m_numJoints);

    for (int i = 0; i < m_numJoints; i++) {
        m_jointHandles[i] = b0RemoteApi::readInt(
            m_client.simxGetObjectHandle(jointNames[i].c_str(),
                                         m_client.simxServiceCall()),
            1);

        m_client.simxGetJointPosition(
            m_jointHandles[i], m_client.simxDefaultSubscriber(boost::bind(
                                   &RFRobot::jointPosition_CB, this, _1, i)));
    }

    m_tcpHandle =
        b0RemoteApi::readInt(m_client.simxGetObjectHandle(
                                 tcpName.c_str(), m_client.simxServiceCall()),
                             1);

    m_baseHandle =
        b0RemoteApi::readInt(m_client.simxGetObjectHandle(
                                 baseName.c_str(), m_client.simxServiceCall()),
                             1);

    registerCallbacks();
}

void RFRobot::registerCallbacks() {
    m_client.simxGetSimulationState(m_client.simxDefaultSubscriber(
        boost::bind(&RFRobot::getSimulationState_CB, this, _1)));

    m_client.simxGetObjectPose(m_tcpHandle, m_baseHandle,
                               m_client.simxDefaultSubscriber(boost::bind(
                                   &RFRobot::endEffectorPose_CB, this, _1)));
}

//-------------------
// MARK: Callbacks
//-------------------

void RFRobot::endEffectorPose_CB(std::vector<msgpack::object>* msg) {
    b0RemoteApi::readFloatArray(msg, m_endEffectorPose, 1);
}

void RFRobot::getSimulationState_CB(std::vector<msgpack::object>* msg) {
    m_simState = b0RemoteApi::readInt(msg, 1);
}

void RFRobot::jointPosition_CB(std::vector<msgpack::object>* msg, int index) {
    m_jointPositions[index] = (double)b0RemoteApi::readFloat(msg, 1);
}

void RFRobot::runSynchronous() {
    m_client.simxSynchronous(m_client.simxServiceCall());
}

void RFRobot::startSimulation() {
    m_client.simxStartSimulation(m_client.simxDefaultPublisher());
}

void RFRobot::stopSimulation() {
    m_client.simxStopSimulation(m_client.simxDefaultPublisher());
}

void RFRobot::stepSimulation() { m_client.simxSynchronousTrigger(); }

void RFRobot::spinOnce() { m_client.simxSpinOnce(); }

//-------------------
// MARK: Getters
//-------------------

std::vector<double> RFRobot::getJointPositions() { return m_jointPositions; }

std::vector<float> RFRobot::getEndEffectorPose() { return m_endEffectorPose; }

int RFRobot::getRobotMode() { return m_simState; }

std::vector<double> RFRobot::getEndEffectorVelocity() {
    return m_endEffectorVelocity;
}

//-------------------
// MARK: Setters
//-------------------

void RFRobot::setJointPositions(std::vector<double> positionCommands) {
    for (int i = 0; i < m_numJoints; i++) {
        m_client.simxSetJointPosition(m_jointHandles[i], positionCommands[i],
                                      m_client.simxDefaultPublisher());
    }
}

void RFRobot::setJointPositions(double positionCommand) {
    for (int i = 0; i < m_numJoints; i++) {
        m_client.simxSetJointPosition(m_jointHandles[i], positionCommand,
                                      m_client.simxDefaultPublisher());
    }
}

void RFRobot::setJointTargetPositions(std::vector<double> positionCommands) {
    for (int i = 0; i < m_numJoints; i++) {
        m_client.simxSetJointTargetPosition(m_jointHandles[i],
                                            positionCommands[i],
                                            m_client.simxDefaultPublisher());
    }
}

void RFRobot::setJointTargetPositions(double positionCommand) {
    for (int i = 0; i < m_numJoints; i++) {
        m_client.simxSetJointTargetPosition(m_jointHandles[i], positionCommand,
                                            m_client.simxDefaultPublisher());
    }
}

void RFRobot::pingServer() {
    m_client.simxSetIntSignal("PING", 0, m_client.simxDefaultPublisher());
}

}  // namespace rf
