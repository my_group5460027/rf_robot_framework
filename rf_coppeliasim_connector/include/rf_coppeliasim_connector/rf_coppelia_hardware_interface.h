/// @file rf_coppelia_interface.h
///
/// @author Tom Hattendorf
/// @date 18.08.2021
///
/// @brief Class definition of RFCoppeliaHardwareInterface
///
/// rf_coppelia_hardware_interface.h
/// rf_coppeliasim_connector
///
/// Created by Tom Hattendorf on 16.08.2021 for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.

#ifndef RF_COPPELIA_INTERFACE_H
#define RF_COPPELIA_INTERFACE_H

#include <actionlib/server/simple_action_server.h>
#include <controller_manager/controller_manager.h>
#include <franka_hw/franka_hw.h>
#include <franka_msgs/ErrorRecoveryAction.h>
#include <franka_msgs/FrankaState.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/JointState.h>
#include <hardware_interface/joint_command_interface.h>
#include <hardware_interface/joint_state_interface.h>
#include <hardware_interface/robot_hw.h>
#include <realtime_tools/realtime_publisher.h>
#include <ros/ros.h>
#include <std_msgs/Int16.h>
#include <std_msgs/Bool.h>
#include <tf/tf.h>

#include <Eigen/Dense>

#include "rf_robot.h"
#include "rf_kinematics.h"

namespace rf {

/// @brief An Hardware Interface to allow interaction between the RFRobot class and controllers
///
/// This hardware interface uses an instance of the RFRobot class to connect to CoppeliaSim.
/// The read()-function read all desired values from the robot and uses them as inputs for the active controllers. 
/// Depending on the controller, the calculated commands are then written to CoppeliaSim.

class RFCoppeliaHardwareInterface : public hardware_interface::RobotHW {
   protected:

    /// @brief A ROS NodeHandle
    ros::NodeHandle m_nodeHandle;

    /// @brief A reference to a RFRobot-Object
    RFRobot& m_robot;

    /// @brief A RFKinematics object providing inverse kinematics functions
    RFKinematics m_kinematics;

    /// @name Interfaces
    ///
    /// These are used to connect the Hardware Interface to the loaded controllers via an instance of the hardware manager
    // @{

    /// @brief Connects the joint states to a generic controller    
    hardware_interface::JointStateInterface m_jointstateInterface;

    /// @brief Connects the joint positions to a generic controller
    hardware_interface::PositionJointInterface m_positionJointInterface;

    /// @brief Connects the joint velocites to a generic controller (currently unused)
    hardware_interface::VelocityJointInterface m_velocityJointInterface;

    /// @brief Connects the joint efforts to a generic controller (currently unused)
    hardware_interface::EffortJointInterface m_effortJointInterface;

    /// @brief Connects the franka robot state to a franka controller
    franka_hw::FrankaStateInterface franka_state_interface;

    /// @brief Connects the cartesian velocities to a franka controller
    franka_hw::FrankaVelocityCartesianInterface franka_velocity_interface;

    /// @}

    /// @name Robot data
    ///
    /// @{
    
    /// @brief The robot state packaged in franka emikas standard format
    franka::RobotState m_robotState;

    /// @brief The robot state packaged as ROS message
    franka_msgs::FrankaState m_frankaState;

    /// @brief A vector containing the cartesian velocities of the end effector, translational and rotational
    std::array<double, 6> m_cartesianVelocities;

    /// @brief A vector containing the elbow configuration - unused
    std::array<double, 2> m_elbow;

    /// @brief The arm id of a the robot as required by the cartesian velocity controller 
    std::string m_armId;

    /// @brief The number of robot arm joints
    int m_numJoints;

    /// @brief The state the simulation is currently in: 0 = Stopped, 8 = Paused, 16 = Running
    int m_simState;

    /// @brief The program state the framework is currently in: 0 = inactive, 1 = active
    int m_programState;

    /// @brief The mode of control currently applied to the robot: PTP for joint motion, LIN for cartesian motion
    std::string m_controlMode;

    /// @brief A vector containing the robot joint names
    std::vector<std::string> m_jointNames;

    /// @brief A vector containing the current robot joint position
    std::vector<double> m_jointPositions;

    /// @brief A vector containing the commanded joint positions as calculated by a controller
    std::vector<double> m_jointPositionsCommand;

    /// @brief A vector containing the commanded joint positions as provided by the jog mode interface
    std::vector<double> m_guidingCommand;

    /// @brief A vector containing the end effector position (x, y, y, qx, qy, qz, qw)
    std::vector<float> m_EePose;
    
    /// @brief A realtime publisher for publishing the franka state 
    realtime_tools::RealtimePublisher<franka_msgs::FrankaState>
        m_frankaStatePublisher;

    /// @brief A subscriber to determine the program state
    ros::Subscriber m_programStateSubscriber;

    /// @brief A subscriber for commanded joint positions from the jog mode interface
    ros::Subscriber m_guidingCommandSubscriber;

    /// @brief A simple action server to acknowledge errors that occured during program execution
    actionlib::SimpleActionServer<franka_msgs::ErrorRecoveryAction>
        m_errorRecoveryServer;
    
    /// @brief A KDl-JointArray containing the theoretical joint positions when using linear motion
    KDL::JntArray velInterface_q;

    /// @brief A KDL-JointArray containing the theoretical joint velocities when using linear motion
    KDL::JntArray velInterface_dq;

    /// @brief A KDL-Twist containing the theoretical cartesian velocities of the end effector
    KDL::Twist velInterface_v;

    /// @brief Uses the joint velocities from the cartesian velocity command and the inverse kinematics to calculate the next joint position each simulation step 
    void updateVelocity();

    /// @brief Sets all positions and velocites to zero 
    void setToZero();

    /// @brief Populates the franka state message according to the current robot state
    void handleFrankaState();

    /// @brief Sets up the generic interfaces to use with ros-control
    void setupFrankaInterface();

    /// @brief Sets up the franka interfaces to use with the custum cartesian controller
    void setupJointInterface();

    /// @brief A callback function to write the current prgram state to m_programState
    void programStateSubscriber_CB(std_msgs::Int16 msg);

    /// @brief A callback function to write the commanded joint positions from guiding mode to m_guidingCommands
    void guidingCommandSubscriber_CB(sensor_msgs::JointState msg);

    /// @brief A the callback function of the error recovrey action server, simply sets succeeded
    bool errorRecovery(const franka_msgs::ErrorRecoveryGoalConstPtr& goal);

    /// @brief Sends joint position commands to the RFRobot object
    void setJoints();

   public:
   /// @brief Constructor
    RFCoppeliaHardwareInterface(ros::NodeHandle& nh, RFRobot& robot);

    /// @brief Desctructor
    ~RFCoppeliaHardwareInterface();

    /// @brief Initializes publisher, subscriber and action server and calls the interface-setup functions 
    void init();

    /// @brief Reads the robot data from the RFRobot object 
    void read(std::string mode);

    /// @brief Writes robot positions according to the controller commands to the RFRobot object
    void write();
};

}  // namespace rf
#endif