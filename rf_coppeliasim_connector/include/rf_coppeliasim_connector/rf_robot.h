/// @file rf_robot.h
///
/// @author Tom Hattendorf
/// @date 16.08.2021
///
/// @brief Class definition of RFRobot
///
/// rf_robot.h
/// rf_coppeliasim_connector
///
/// Created by Tom Hattendorf on 16.08.2021 for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.

#ifndef RF_ROBOT_H
#define RF_ROBOT_H
#include "b0RemoteApi.h"

namespace rf {

/// @brief A generic robot interface to connect to CoppeliaSim with the
/// BlueZero-API
///
/// This class includes a collection of BlueZero-API functions for connection
/// with CoppeliaSim. Reading data from CoppeliaSim is handled by
/// callback-functions, which are called by spinOnce().
//
// Two init()-functions exist, for a robot and a robot with gripper.
// Simulation in CoppeliaSim can also be started and stopped by an Object of
// this class.

class RFRobot {
   protected:
    /// @brief A BlueZero-API client, which is used to connect to CoppeliaSim
    b0RemoteApi m_client;

    /// @brief A vector containing the X, Y, Z coordinates as well as the
    /// quaternions of the end effector
    std::vector<float> m_endEffectorPose;
    /// @brief A vector containing the joint positions of the robot
    std::vector<double> m_jointPositions;
    /// @brief A vector containing the cartesian velocities of the end effector
    std::vector<double> m_endEffectorVelocity;
    /// @brief A vector containing the joint handles of the robot
    std::vector<int> m_jointHandles;

    /// @brief The number of joints of the connected robot
    int m_numJoints;
    /// @brief The state the simulation is in, 0 = Stopped, 8 == Paused, 16 ==
    /// Running
    int m_simState;
    /// @brief The object handle of the end effector
    int m_tcpHandle;
    /// @brief The object handle of the robot base
    int m_baseHandle;

    /// @brief Register callbacks for simulation state and effector pose
    void registerCallbacks();

    /// @brief Callback function for the simulation state
    ///
    /// Receives a msgpack object vector containing an integer, unpacks it and
    /// writes it to m_simState
    void getSimulationState_CB(std::vector<msgpack::object>* msg);

    /// @brief Callback function for the simulation state
    ///
    /// Receives a msgpack object vector containing a float array, unpacks it
    /// and writes it to m_jointPositions at index
    ///
    /// @param index integer indicating the robot joint
    void jointPosition_CB(std::vector<msgpack::object>* msg, int index);

    /// @brief Callback function for the simulation state
    ///
    /// Receives a msgpack object vector containing a float array, unpacks it
    /// and writes it to m_endEffectorPose
    void endEffectorPose_CB(std::vector<msgpack::object>* msg);

    /// @brief Callback function for the simulation state
    ///
    /// Receives a msgpack object vector containing a float array, unpacks it
    /// and writes it to m_endEffectorVelocity
    void objectVelocity_CB(std::vector<msgpack::object>* msg);

   public:
    /// @brief Contructor
    RFRobot();
    /// @brief Destructor
    ~RFRobot();
    /// @brief Initializes callbacks for a robot
    ///
    /// @param jointNames Names of joints to register, have top match robot
    /// loaded in CoppeliaSim
    /// @param tcpName Name of point considered to be the end effector
    void init(std::vector<std::string> jointNames, std::string tcpName, std::string baseName);

    /// @brief Returns cartesian velocity of end effector
    ///
    /// @return Double vector  containing linear [m/s] and angular [°/s]
    /// velocities
    std::vector<double> getEndEffectorVelocity();

    /// @brief Returns joint positions of robot
    ///
    /// @return Double vector containing joint positions [°]
    std::vector<double> getJointPositions();

    /// @brief Returns endeffector position
    ///
    /// @return Float vector containing the X, Y, Z coordinates as well as the
    /// quaternions of the end effector
    std::vector<float> getEndEffectorPose();

    /// @brief Returns the simulation state
    ///
    /// @return Simulation state (0 = Stopped, 8 == Paused, 16 == Running)
    int getRobotMode();

    /// @brief Sets the joint positions of the robot. Only works if simulation
    /// is not running
    ///
    /// @param positionCommands Double vector containing the robot joint
    /// positions
    void setJointPositions(std::vector<double> positionCommands);

    /// @brief Sets the same joint position for all joints. Useful for actuating
    /// a gripper. Only works if simulation is not running
    ///
    /// @param positionCommands Double vector containing the robot joint
    /// positions
    void setJointPositions(double positionCommands);

    /// @brief Sets the target joint positions of the robot. Only works if
    /// simulation is running
    ///
    /// @param positionCommands Double vector containing the robot joint
    /// positions
    void setJointTargetPositions(std::vector<double> positionCommands);

    /// @brief Sets the same target joint positions for all joints. Useful for
    /// actuating a gripper. Only works if simulation is running
    ///
    /// @param positionCommands Double joint position
    void setJointTargetPositions(double positionCommand);

    /// @brief Triggers a simulation step if simulation is running in
    /// synchronous mode
    void stepSimulation();

    /// @brief Gets BlueZero callback queue and calls it functions
    void spinOnce();

    /// @brief Starts the simulation
    void startSimulation();

    /// @brief Stops the simulation
    void stopSimulation();

    /// @brief Sets the simulation in synchronous mode
    void runSynchronous();

    /// @brief Sends a dummy signal to the server to avoid connection timeout from inactivity
    void pingServer();
};

}  // namespace rf
#endif