/// @file rf_kinematics.h
///
/// @author Tom Hattendorf
/// @date 18.08.2021
///
/// @brief Class definition of RFKinematics
///
/// rf_kinematics.h
/// rf_coppeliasim_connector
///
/// Created by Tom Hattendorf on 16.08.2021 for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.

#ifndef KDL_H
#define KDL_H
#include <math.h>
#include <ros/ros.h>
#include <stdio.h>

#include <iostream>
#include <kdl/chain.hpp>
#include <kdl/chaindynparam.hpp>
#include <kdl/chainfksolver.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl/chainiksolver.hpp>
#include <kdl/chainiksolvervel_pinv.hpp>
#include <kdl/chainjnttojacdotsolver.hpp>
#include <kdl/chainjnttojacsolver.hpp>
#include <kdl/frames.hpp>
#include <kdl/frames_io.hpp>
#include <kdl_parser/kdl_parser.hpp>

namespace rf {

/// @brief A class to perform calculations based on the inverse kinematics
/// 
/// Parses the denavit hartenberg parameters from the current robots URDF-file and build the inverse kinematics.

class RFKinematics {
   protected:

   /// @brief A helper object to calculate joint velocities
   KDL::ChainIkSolverVel *velSolver;

   /// @brief A kinematic chain built from DH parameters
   KDL::Chain m_chainEE;

   public:
    /// @brief Constructor 
    ///
    /// Build the inverse kinematics from the parsed DH-parameters from a URDF-file
    RFKinematics();

    /// @brief Destructor
    ~RFKinematics();
    
    /// @brief Calculates joint velocities based on the robot configuration and  cartesian velocities
    ///
    /// @param q_in A reference to a JointArray containing joint positions
    /// @param v_in A reference to a Twist containing cartesian velocities
    /// @param qdot_out A reference to a JointArray containing joint velocities - OUTPUT
    int getVelocity(const KDL::JntArray &q_in, const KDL::Twist &v_in,
                    KDL::JntArray &qdot_out);
};


}  // namespace rf
#endif