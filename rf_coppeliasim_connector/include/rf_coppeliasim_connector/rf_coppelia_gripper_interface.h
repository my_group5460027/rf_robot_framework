/// @file rf_coppelia_gripper_interface.h
///
/// @author Tom Hattendorf
/// @date 18.08.2021
///
/// @brief Class definition of RFCoppeliaGripperInterface
///
/// rf_coppelia_gripper_interface.h
/// rf_coppeliasim_connector
///
/// Created by Tom Hattendorf on 18.08.2021 for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.

#include <actionlib/server/simple_action_server.h>
#include <franka_gripper/GraspAction.h>
#include <franka_gripper/HomingAction.h>
#include <franka_gripper/MoveAction.h>
#include <franka_gripper/StopAction.h>
#include <ros/ros.h>

#include "rf_robot.h"

namespace rf {

/// @brief An interface for gripper control using the RFRobot class
///
/// This class includes a collection of ActionServers functions to allow control
/// of a gripper using the DHRobot class and its generic interface to
/// CoppeliaSim.

class RFCoppeliaGripperInterface {
   protected:
    /// @brief A reference to a DHRobot Object
    RFRobot& robot;

    /// @brief A ROS nodehandle
    ros::NodeHandle nh;

    /// @name Active goal
    /// The active goal of the gripper, may be set by any of the four
    /// ActionServers
    /// @{

    /// @brief Indicates whether a goal is being pursued
    bool m_activeGoal;

    /// @brief The goal position of both gripper fingers, i.e. width / 2
    double m_activeGoalPosition;

    /// @brief The speed of the gripper motion
    double m_activeGoalSpeed;

    /// @brief The force the gripper should grip with (currently unused)
    double m_activeGoalForce;

    /// @brief The distance in m the gripper has to move each simulation step
    /// (100 Hz for CoppeliaSim connection)
    double m_gripperStep;

    /// @brief The current Gripper Position
    double m_gripperPosition;

    /// @brief The maximum opening of a Gripper finger
    double m_maxOpening = 0.04;

    ///@}

    /// @name Action servers and callbacks
    /// @{

    /// @brief Action Server for "stop"
    actionlib::SimpleActionServer<franka_gripper::StopAction>
        stop_action_server;

    /// @brief Action Server for "move"
    actionlib::SimpleActionServer<franka_gripper::MoveAction>
        move_action_server;

    /// @brief Action Server for "grasp"
    actionlib::SimpleActionServer<franka_gripper::GraspAction>
        grasp_action_server;

    /// @brief Action Server for "homing"
    actionlib::SimpleActionServer<franka_gripper::HomingAction>
        homing_action_server;

    /// @brief Callbackfunction for "homing"
    /// Currently no homing function is implemented, so an error message is
    /// printed
    bool homing(const franka_gripper::HomingGoalConstPtr& goal);

    /// @brief Callbackfunction for "grasp"
    /// Sets active goal and calls the actuateGripper function
    bool grasp(const franka_gripper::GraspGoalConstPtr& goal);

    /// @brief Callbackfunction for "move"
    /// Sets active goal and calls the actuateGripper function
    bool move(const franka_gripper::MoveGoalConstPtr& goal);

    /// @brief Callbackfunction for "stop"
    /// Currently not in use.
    bool stop(const franka_gripper::StopGoalConstPtr& goal);

    ///@}

    /// @brief Actuates gripper according to the current goal
    bool actuateGripper();

    /// @brief Cuts off decimal places of a double number
    ///
    /// @param val The value to be truncated
    /// @param prec The precision / number of decimal places
    /// @return The truncated input value
    double roundOff(double val, int prec);

   public:
    /// @brief Constructor
    RFCoppeliaGripperInterface(ros::NodeHandle& nh, RFRobot& robot);

    /// @brief Destructor
    ~RFCoppeliaGripperInterface();
};

}  // namespace rf