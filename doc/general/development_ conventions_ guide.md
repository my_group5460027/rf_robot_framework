#  Development Conventions Guide 


The following code conventions are a style mix between the [ROS C++ Style Guide](https://wiki.ros.org/action/fullsearch/CppStyleGuide?action=fullsearch&context=180&value=linkto%3A%22CppStyleGuide%22), 
[hungarian notation](https://en.wikipedia.org/wiki/Hungarian_notation) and personal preferences. 
Some chapters are just copies of the ROS C++ Style Guide.

This is just a suggestion. Every aspect can be discussed and adjusted.




## Naming

The following shortcuts are used in this section to denote naming schemes:

* `CamelCased`: The name starts with a capital letter, and has a capital letter for each new word, with no underscores.
* `camelCased`: Like `CamelCase`, but with a lower-case first letter
* `under_scored`: The name uses only lower-case letters, with words separated by underscores.  
* `ALL_CAPITALS`: All capital letters, with words separated by underscores.


### Packages
ROS packages are `under_scored`.


### Topics / Services
ROS topics and service names are `under_scored`.


### Files
All files are `under_scored`.

Source files have the extension `.cpp`.

Header files have the extension `.h`.

If the file primarily implements a class, name the file after the class. 
For example the class `ActionServer` would live in the file `action_server.h`.



### Classes / Types 
Class names (and other type names) are `CamelCased`

E.g.:

```
class ExampleClass;
```

Name the class after what it is. If you can't think of what it is, perhaps you have not thought through the design well enough.

Compound names of over three words are a clue that your design may be unnecessarily confusing.



### Function / Methods
In general, function and class method names are `camelCased`, and arguments are also  `camelCased`, e.g.:

```
int exampleMethod(int exampleArg);
```

Functions and methods usually perform an action, so their name should make clear what they do: checkForErrors() instead of errorCheck(), dumpDataToFile() instead of dataFile().  Classes are often nouns. By making function names verbs and following other naming conventions programs can be read more naturally.


### Variables
In general, variable names are `camelCased`.

Be reasonably descriptive and try not to be cryptic.  Longer variable names don't take up more space in memory.

Integral iterator variables can be very short, such as `i`, `j`.  Be consistent in how you use iterators (e.g., `i` on the outer loop, `j` on the next inner loop).



### Constants 
Constants, wherever they are used, are `ALL_CAPITALS`.

### Member variables 
Variables that are members of a class are `camelCased`, with a prefix `m_` added.

E.g.:

```
int m_example;
```
### Global variables 
Global variables should never be used!!!

### Namespaces 
Namespace names are '''under_scored'''.








## Formatting

### Indentation
Use tabs for indentation so everyone can configure his or
her prefered indentation length.

### Placing Braces and Spaces

>The other issue that always comes up in C styling is the placement of
braces. [...] [P]ut the opening brace last on the line, and put the closing 
brace first, thusly. [Linux kernel coding style](https://www.kernel.org/doc/Documentation/process/coding-style.rst)


``` 
void MobilePlatform::stop(bool resetSpeed,bool writeToOutput){
    if (resetSpeed){
        m_nSpeed = 0;
    }
    m_nDirection = 0;
    if (writeToOutput){
        delegateMotorControlValues();
    }
}
```




### Comments

>NEVER try to explain HOW your code works in a comment: it's much better to
write the code so that the **working** is obvious, and it's a waste of
time to explain badly written code.
Generally, you want your comments to tell WHAT [and WHY] your code does, not HOW.
[Linux kernel coding style](https://www.kernel.org/doc/Documentation/process/coding-style.rst)

Only use `//` and not `/* */` for coments.
Use  `///` for Doxygen comments.

Use [Doxygen](https://en.wikipedia.org/wiki/Doxygen) comments for every file, class and function 
you write. 
Use `@` as the doxygen command trigger instead of `\`. 
You should at least provide the following commands for each function: `@brief`, `@param`, `@return`.

```
/// @brief Stopps motors
///
/// @param resetSpeed Should it set the speed value to 0?
/// @param writeToOutput Write set values to motors?
///
void stop(bool resetSpeed = true,bool writeToOutput = true);
```









## (Git)-Workflow

We use the [gitflow](https://nvie.com/posts/a-successful-git-branching-model/) workflow
with some exceptions and additions:
* There are no release branches
* Feature branches can and should be pushed to origin for backup. (And can be deleted after the merge.)
* For better code review never merge your own feature branch back in develop. Let another team member review your code and merge it for you (Merge requests). 

Please commit often. Rule of thumb: Commit at least every hour.

Also see [this guide](https://chris.beams.io/posts/git-commit/) on writing commit messages.
