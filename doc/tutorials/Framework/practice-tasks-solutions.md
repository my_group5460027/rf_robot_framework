
# Practice tasks for self-control (Solution)
The following page lists some tasks and questions to check if you are ready for the robothon. 
If you can answer all questions and tasks, you are ready to go. If not, you should 
re-read some of the chapters in this documentation. Please do not look in the solutions 
until your done with your work.

## git



1. Name 4 git commands and explain what they do.


	status 	// Get the status of files and the branch
	add		// Stage the argument files
	commit 	// Commit the staged files (optional arguments: --all -m "My Message")
	push	// Push the checked out branch to a remote branch (on origin)
	pull	// Pull changes from a remote branch into the local branch
	...		// Much more


2. You have changed the file test.txt on your latest branch. 
Which commands do you have to use to commit these changes?


	git status 					// ... optional, to see that the file actually changed
	git add test.txt			// ... to add the file to the staging area
	git commit -m "Add funny test messages" // ... to commit the staged file and add a one line commit message

Alternative solution:

	git commit --all -m "Add funny test messages" // ... Stage all changed files and commit them



## Framework

Write a simple program execution for the following task:
The points P1, P2, Home were teached in a previous step.

1. Move as fast as possible to the point P1.
2. From P1 move linear downward for 5cm with a maximum speed of 0.1 m/s.
3. Pick up a cube with an edge length of 1 cm.
4. Move linear with default speed to point P2.
5. Place the object at P2.
6. Move as fast as possible to Home.

For a simple context, you only have to rewrite this empty template of the existing
testprogram class RFTestProgram.

	void RFTestProgram::execute(){

		// Construct required commands
		RFPTP ptp(*m_pNodeHandle);
		RFLIN lin(*m_pNodeHandle);
		RFGripper gripper(*m_pNodeHandle);
		RFController linController(*m_pNodeHandle, RF_LIN_MOTION_GENERATOR_NAME,true);

		// 1:
		// PTP motion is in general faster than a linear motion
		ptp.moveToPoint("P1", 2);

		// 2:
		// Set the desired speed for translation. Rotation set as default value.
		// Execute a relative motion.
		lin.setSpeed(0.1, 0.5);
		lin.moveRelativeToPoint(0, 0, -0.05);

		// 3:
		// Grasp the object.
		// close() could also be possible, but we know the exact object size.
		gripper.grasp(0.01, 0.05, 20);

		// 4:
		// The speed from 2. is still set. Therefore, we have to reset it.
		lin.resetToDefaultValues();
		lin.moveToPoint("P2");

		// 5:
		// Simply open the gripper with default speed.
		gripper.open();

		// 6:
		ptp.moveToPoint("Home", 2);

	}

---

When you are done, think about possible exceptions in your command sequence
and how to handle them accordingly.


	void RFTestProgram::execute(){

		// Construct required commands
		RFPTP ptp(*m_pNodeHandle);
		RFLIN lin(*m_pNodeHandle);
		RFGripper gripper(*m_pNodeHandle);
		RFController linController(*m_pNodeHandle, RF_LIN_MOTION_GENERATOR_NAME,true);


		ptp.moveToPoint("P1", 2);


		lin.setSpeed(0.1, 0.5);
		lin.moveRelativeToPoint(0, 0, -0.05);

		// The gripper might not grasp the desired cube or nothing
		// Therefore, we open the gripper and go back to Home
		try {
			gripper.grasp(0.01, 0.05, 20);
		} catch(const std::runtime_error& e) {
			gripper.open();
			ptp.moveToPoint("Home", 5);
			throw;
		}

		lin.resetToDefaultValues();
		lin.moveToPoint("P2");

		gripper.open();


		ptp.moveToPoint("Home", 2);

	}
