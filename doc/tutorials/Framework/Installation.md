# rf-robot-framework

## About RFRobotFramework

RFRobotFramework is a single ROS node that wraps franka_ros and ros_control and provides a graphical user-interface to offer additional useful features, such as teach-in of task points and joint configurations, easy startup of programs, resetting errors... The components are, as usual in ROS, designed to be executed on multiple machines. With the typical laboratory setup at the Institute of Automatic Control of the Leibniz University the franka_ros and ros_control part is expected to run on an intel NUC, that is directly connected to the franka master controller.


## Preparing the Franka 


### Real-Time Kernel
In order to control your robot using `libfranka`, the controller program on the workstation PC must run with real-time priority under a `PREEMPT_RT` kernel. This section describes the procedure of patching a kernel to support `PREEMPT_RT` and creating an installation package. The process of compiling the Kernel make take a few hours. To find more details about the process, see the [Linux Kernel Archive](https://www.kernel.org/signature.html)

1. Install necesary dependencies for the real time kernel 
   
   		sudo apt-get install build-essential bc curl ca-certificates gnupg2 libssl-dev lsb-release libelf-dev bison flex dwarves zstd libncurses-dev

2. Depending on your Ubuntu, you can use curl to download form the source files: 
	
	for `Ubuntu 18.04` tested with the kernel version 5.4.19: 

		curl -SLO https://www.kernel.org/pub/linux/kernel/v4.x/linux-4.14.12.tar.xz
		curl -SLO https://www.kernel.org/pub/linux/kernel/v4.x/linux-4.14.12.tar.sign
		curl -SLO https://www.kernel.org/pub/linux/kernel/projects/rt/4.14/older/patch-4.14.12-rt10.patch.xz
		curl -SLO https://www.kernel.org/pub/linux/kernel/projects/rt/4.14/older/patch-4.14.12-rt10.patch.sign
	
	for `Ubuntu 20.04` tested with the kernel version 5.9.1:

		curl -SLO https://www.kernel.org/pub/linux/kernel/v5.x/linux-5.9.1.tar.xz
		curl -SLO https://www.kernel.org/pub/linux/kernel/v5.x/linux-5.9.1.tar.sign
		curl -SLO https://www.kernel.org/pub/linux/kernel/projects/rt/5.9/patch-5.9.1-rt20.patch.xz
		curl -SLO https://www.kernel.org/pub/linux/kernel/projects/rt/5.9/patch-5.9.1-rt20.patch.sign

3. Decompress them with 
   
		xz -d *.xz
	
4. Verifying file integrity (Optional but recommended)
   
   The `.sign` files can be used to verify that the downloaded files were not corrupted or tamp	@realtime soft rtprio 99
			@realtime soft priority 99
			@realtime soft memlock 102400
			@realtime hard rtprio 99
			@realtime hard priority 99
			@realtime hard memlock 102400ered with. you can used `gpg2` to verify the `.tar` archives: 
   
		gpg2 --verify linux-*.tar.sign
		gpg2 --verify patch-*.patch.sign

	If your output is similar to the following: 

		$ gpg2 --verify linux-*.tar.sign
		gpg: assuming signed data in 'linux-4.14.12.tar'
		gpg: Signature made Fr 05 Jan 2018 06:49:11 PST using RSA key ID 6092693E
		gpg: Can't check signature: No public key

	You have to first download the public key of the person who signed the above file. As you can see from the above output, it has the ID 6092693E. You can obtain it from the key server:

		gpg2  --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 6092693E

	Similarly for the patch:

		gpg2 --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 2872E4CC
	
	Having downloaded the keys, you can now verify the sources. Here is an example of a correct output:

		$ gpg2 --verify linux-*.tar.sign
		gpg: assuming signed data in 'linux-4.14.12.tar'
		gpg: Signature made Fr 05 Jan 2018 06:49:11 PST using RSA key ID 6092693E
		gpg: Good signature from "Greg Kroah-Hartman <gregkh@linuxfoundation.org>" [unknown]
		gpg:                 aka "Greg Kroah-Hartman <gregkh@kernel.org>" [unknown]
		gpg:                 aka "Greg Kroah-Hartman (Linux kernel stable release signing key) <greg@kroah.com>" [unknown]
		gpg: WARNING: This key is not certified with a trusted signature!
		gpg:          There is no indication that the signature belongs to the owner.
		Primary key fingerprint: 647F 2865 4894 E3BD 4571  99BE 38DB BDC8 6092 693E

5. Compiling the kernel 
   
   Once you are sure the files were downloaded properly, you can extract the source code and apply the patch:

		tar xf linux-*.tar
		cd linux-*/
		patch -p1 < ../patch-*.patch

	Next copy your currently booted kernel configuration as the default config for the new real time kernel:
	 
		cp -v /boot/config-$(uname -r) .config

	Now you can use this config as the default to configure the build:

		make olddefconfig
		make menuconfig	

	The second command brings up a terminal interface in which you can configure the preemption model.

	Navigate with the arrow keys to `General Setup > Preemption Model` and select `Fully Preemptible Kernel (Real-Time)`.

	After that navigate to `Cryptographic API > Certificates for signature checking` (at the very bottom of the list) > `Provide system-wide ring of trusted keys > Additional X.509 keys for default system keyring`.   Remove the “debian/canonical-certs.pem” from the prompt and press Ok. Save this configuration to `.config` and exit the TUI.

	Afterwards, you are ready to compile the kernel. 

		make -j$(nproc) deb-pkg

	Finally, you are ready to install the newly created package. The exact names depend on your environment, but you are looking for headers and images packages without the dbg suffix. To install:

		sudo dpkg -i ../linux-headers-*.deb ../linux-image-*.deb

6. Verify the new kernel 

	Restart your system. The Grub boot menu should now allow you to choose your newly installed kernel. To see which one is currently being used, see the output of the `uname -a` command. It should contain the string `PREEMPT RT` and the version number you chose. Additionally, `/sys/kernel/realtime` should exist and contain the the number `1`.

7. Allow a user to set a real-time permission for its processes
 
   After the `PREEMPT_RT` kernel is installed and running, add a group named realtime and add the user controlling your robot to this group:

   		sudo addgroup realtime
		sudo usermod -a -G realtime $(whoami)

	Afterwards, add the following limits to the realtime group in `/etc/security/limits.conf`

		@realtime soft rtprio 99
		@realtime soft priority 99
		@realtime soft memlock 102400
		@realtime hard rtprio 99
		@realtime hard priority 99
		@realtime hard memlock 102400



### Docker Container 
The repository contains a dockerfile for an image with all the needed dependencies pre installed. It mounts the repo direct from the host, so it can be edited and will not be reset like everything else after the container terminates. 

Install Docker Container following [this](https://docs.docker.com/desktop/install/ubuntu/) tutorial 

This Framework can also be used without a Docker Container. If you want to do it this way, follow [this](@ref tutorials/Framework/docker.md) additional installation instructions.

## Preparing the UR 

##### tested for 5.7.0.90932

First download the UR software provided [here](https://www.universal-robots.com/download/?option=66136#section41511) and save it to your home folder. Change to your home folder and extract the file to the root of your home folder:

```console
cd ~
tar xvzf [FILE NAME]
cd ursim-5.X.X.XXXXX
./install.sh
```

Now you can simply run the software from the terminal by running:

```console
cd ..
sudo ursim-5.X.X.XXXXX/start-ursim.sh
```

### Enable Modbus server in URSim

As per default, the Modbus server is activated. In case it is not then open the file starturcontrol.sh which is located in the installation folder using a file editor.

Modify this line:

```console
HOME=$SCRIPT_DIR $SCRIPT_DIR/URControl &>$SCRIPT_DIR/URControl.log &
```

into:

```console
echo <linux_password> | sudo -S HOME=$SCRIPT_DIR $SCRIPT_DIR/URControl &>$SCRIPT_DIR/URControl.log &
```

where password is the login password for Linux.

### Install URCaps "externalcontrol".

Follow these steps

1. After Starting URSim click on the Hamburger Menu in the upper right corner
2. Select "Settings"
3. In the menu on the left click on "System"
4. Now select URCaps
5. Here you can see all installed and active URCaps. To add an URCaps click on the plus in this window.
6. You are directed to "cd /". Now select your URCaps and click on open.
7. Restart URSim 

### Complete Start-Up GUIDE Simulation

```console
cd ursim-X.X.X.XXXXX/
sudo ./start-ursim.sh
```

In URSim under the programwindow "Installation", select "URCaps", than "External Control" and change the IP to "192.168.0.XX".
The IP should be your own IP.
Start the robot by clicking on the red button on the bottom left corner. In the new window select ON, wait for it to start and click on start.
Back in the terminal:

```console
roslaunch ur_calibration calibration_correction.launch\
robot_ip:=<robot_ip> kinematics_config:="${HOME}/robot_calibration.yaml"

rosrun rf_robot_control ur_adapter
```

Now the simulation can be controlled by the franka-ros-framework

## Preprare the PCs

1. Make a new workspace with  

		mkdir -p ~/catkin_ws/src
	  
2. Change the directory 

		cd  ~/catkin_ws/src

3. Clone the repo with 

	    git clone https://gitlab.com/my_group5460027/rf_robot_framework.git .

4. On the host change the IPs in `ros_ips.txt` to the correct ROS_MASTER_URI (if you're using a NUC this should be the NUCs IP otherwise choose one PC to be the ROS master) and the ROS_IP to the hosts IP (Note that the `ros_ips.txt` must have one blank line at the end).
5. In the files `container_preparation.sh` line 6 change the PATH to your home directory, as well as on the file `kernel_preparation.sh` on line 12 (there indicated)
6. Make sure, that the repository was cloned into ~/catkin_ws/src (On the host). Then run the script with
	
		 ./host_preparation.sh
		 ./kernel_preparation
		
	 and reboot with 
	 
	 	sudo reboot

	 After that, run 
	 
	 	./pull_docker_image.sh
		
	If you don't have access to the gitlab docker repository or you want to build the container image yourself you can run `contbuild` instead of `./pull_docker_image.sh`. 









