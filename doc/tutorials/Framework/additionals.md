
# Additional Instalation steps to follow for a docker free conection

### ROS 
Install [ROS](https://wiki.ros.org/ROS/Installation) according to your Ubuntu version. 

### Installing Packages 

Install `franka_ros` and `libfranka` using apt 

        sudo apt install ros-noetic-franka-ros ros-noeticsii -libfranka.

In order for the Cartesian motion generator to work, a simulink plugin is used. The corresponding library is part of this repository and has to be installed:


    cd rf_cartesian_motion_generator/plugins/mogen_p2p
    sudo cp libmogen_p2p.so /usr/local/lib/libmogen_p2p.so
    ldconfig -n -v /usr/local/lib