
# Exception handling
Working with real robots, a lot of unpredictable things can happen.
Therefore a lot of the commands in this framework throw exceptions 
if they do not succeed.

Lets suppose you want to grasp a brick from a table and place it on top of another 
brick. You would use the Command RFGripper::grasp(). One of its 
parameters is the expected object width. Under normal circumstances 
you may grasp the object perfectly fine. However, what happens 
if you forget to place a brick at the required pick up location?

In this case an exception is thrown by grasp(), because the gripper closed way more than 
the expected object width. This is actually a good thing because something went wrong. 
You may choose to handle this exception with a try-catch-block.

try-catch-blocks are C++'s way of handling exceptions. Everything that can go wrong 
will stand in the try block. If something throws an exception the catch block can 
catch the exception object and react to it accordingly. Otherwise the code in the catch block 
will never be executed. Most exceptions in this framework are std::runtime_error.

If an exception is thrown somewhere the program goes up the call stack to find a suitable 
catch block. If no block is found the program is terminated. In other words: 
If you do not catch an exception the program will not continue. 
For safety reasons this is actually a good thing.

Have a look at the following examples: The following code snippet is the basic routine for grasping 
one of the bricks.

		void RFTestProgram::execute(){
			RFPTP ptp(*m_pNodeHandle);
			RFGripper gripper(*m_pNodeHandle);

			ptp.moveToPoint("Pickup");

			gripper.grasp(0.015, 0.05, 20);

			ptp.moveToPoint("Place");
			gripper.open();
			ptp.moveToPoint("Home");


		}


If no brick is at Pickup, grasp() will throw an uncaught exeption. The robot will 
just remain in the Pickup position with its gripper closed. The movements to place and 
Home are not executed.

		void RFTestProgram::execute(){
			RFPTP ptp(*m_pNodeHandle);
			RFGripper gripper(*m_pNodeHandle);

			ptp.moveToPoint("Pickup");

			try {
				gripper.grasp(0.015, 0.05, 20);
			} catch(const std::runtime_error& e) {
				ROS_ERROR_STREAM("Gripper grasp failed. Aborting and moving back to Home: " <<  e.what());
				gripper.open();
				ptp.moveToPoint("Home");
				throw;
			}

			ptp.moveToPoint("Place");
			gripper.open();
			ptp.moveToPoint("Home");


		}

If no brick is at Pickup, grasp() will throw an exception that is caught in the catch block. 
As an reaction the user is informed that it failed and the robot opens the gripper again 
and moves back to Home. Please note that the exception is re-thrown at the end of the catch block. 
This is because the execute() function did not fulfill its purpose and therefore also failed. 
However the robot is now in a defined state (Home, Gripper open).
