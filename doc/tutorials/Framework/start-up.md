# Start-Up for the Franka Emika 

This tutorial walks you through the process of starting the robot
and RFRobotControl. Your Developer-PC should be connected directly to the robot and your Real-time kernel should be running. 

1. Start your PC and the franka controller.

2. On PC:
   
    Open a browser using 
   
        chromium-browser

    After that, type in the IP of your robot on the search bar.
    This will open the Franka Desk Interface. It will be used to unlock and lock the joints and shut down the controller

3. In Desk: Unlock the joints.
   
    The robot will move slightly.
    If the joints are unlocked the interface color will switch from yellow to white.
   Activate the Franka Control Interface (FCI) by going to th  menu on the right side and clicking on `Activate FCI`

4. On terminator shell:
   
        contk
    
    This step is to start the docker container on the PC with real time capabilities

5. Open a new terminal and run: 
   
        cont 

    This step is to start the docker container on the PC and  built your entire workspace, including the qr_tracker and station-dummys packages. 

6. You can also attach your files into Visual Studio Code

   Open Visual Studio code and on the left side menu click on the Docker extension, once your PC container is started, you'll be able to see it listed on the left upper side under containers category. 

   Do a left click on `[container] rf_robot_framework` and click on attach to visual studio code. A new window shall open with the new workspace. 

   [suggestion] Go to Terminal>New Terminal to attach a terminal into visual studio code, where you can open several tabs on the container. 

7. On the first terminal, where the container with real time capabilities run, launch 

         roslaunch rf_robot_control Panda.launch robot_ip:=<your_robot_ip> load_gripper:=true
   
    Make sure to change you write the right ip as robot has. In order for this script to work, the robot joints have to be unlocked at start. 


8.  You are now setup to start developing within RFRobotControl. For building  the project you can run on your second terminal 
    
        catkin build 
    

    If you need to rebuild the container image (for updates) you can use the command  
	
		contbuild
        
8. To start the GUI run on your second terminal 
    
        rosrun rf_robot_control node
    
    The GUI of RFRobotControl will open.
    You can teach points or execute your developed programs. After you made changes to your program, close the GUI and perform steps 7 and 8 again. If your program doesn't behave in a desired way, it is likely that have not rebuilt your package.


# Start up for the UR 

Although the framework has been developed for and with the Franka Emika Panda robot, other robots can be supported by the use of adapters.
In particular, this approach has been applied for the Universal Robot UR3.
For the UR you do not need a second PC, but your PC needs a direct connection to the UR. 


1. Press the power button on the UR.

2. Make sure the IP of the PC is correctly registered in the external control URCap of the UR (e.g. 192.168.1.47).

3. Initialize the robot (unlock brakes etc.). 

4. On the host PC, the command  

		contk
	
	will bring up the developer container

Open Visual Studio code and on the left side menu click on the Docker extension, once your PC container is started, you'll be able to see it listed on the left upper side under containers category. 

Do a left click on `[container] rf_robot_framework` and click on attach to visual studio code. A new window shall open with the new workspace. 

[suggestion] Go to Terminal>New Terminal to attach a terminal into visual studio code, where you can open several tabs on the container. 

1. Now you can start develop your code. For building the project your can either use the command  

	    catkin build

		
    If you need to rebuild the container image (for updates) you can use the command 

		 contbuild
		

6. Run  

		roslaunch rf_robot_control UR.launch

7. Start the external control program on the UR.

8. Run  

		rosrun rf_robot_control node