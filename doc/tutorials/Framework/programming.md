
# Writing your own Robot Program
Writing your own Program is straight forward.
Declare a class with a name of your choice and inherit RFCommand. 
Add the header files of your class to the folder include/rf_robot_control. 
Add the source file of your class to the folder src.
Do not forget to add your created files to the CMakeLists (under project_sources and project_headers).


		#ifndef rf_testprogram_hpp
		#define rf_testprogram_hpp

		#include "rf_command.h"
		#include <ros/ros.h>

		namespace rf {

		class RFTestProgram : public RFCommand {

		public:
			RFTestProgram(ros::NodeHandle &nodeHandle);
			virtual ~RFTestProgram();

			void execute() override;
		};

		} // namespace rf

		#endif // rf_testprogram_hpp


Overwrite the execute method with the sequence of your commands.

		void RFTestProgram::execute(){
			RFPTP ptp(*m_pNodeHandle);
			RFController ptpController(*m_pNodeHandle, RF_PTP_MOTION_GENERATOR_NAME);
			RFGripper gripper(*m_pNodeHandle);

			ptpController.startController();
			try {
				ptp.moveToPoint("P1",2);
			} catch (const std::runtime_error& e) {
				ROS_ERROR_STREAM(e.what());
				throw;
			}

			try {
				gripper.grasp(0.015, 0.05, 20);
			} catch(const std::runtime_error& e) {
				ROS_ERROR_STREAM(e.what());
				gripper.open();
				ptp.moveToPoint("Home", 2);
				ptpController.stopController();
				throw;
			}
			for (size_t i = 2; i <= 3; i++) {
				ptp.moveToPoint("P", i, 2);
			}
			ptp.moveToPoint("P1", 2);
			gripper.open();
			ptp.moveToPoint("Home", 2);

			ptpController.stopController();
		}



In RFRobotControl::RFRobotControl() construct your program object and add it to the m_commands array.

		m_commands.push_back(new RFTestProgram(m_nodeHandle));


Please read the class descriptions on how to use the implemented commands. 
Do not change any of the framework files to write a custom program. 
Always create a new dedicated class and inherit RFCommand.
