# Understanding Basic Design Concepts

The Framework can be split into two parts. the first being the actual controllers and motion generators and the second one the robot control. The controllers are designed to be executed with cycles of 1kHz and can therefore run on a designated control PC. Contrary the robot control does not need to be that fast. It may be run on a developer PC over the local network. Think of it as a process supervisor supervising the progress of the controllers.

Motion generators are used to plan the trajectories of the robot. For the joint-space, the framework uses a controller included in the ros_control package. For the linear motion and the move to contact motion, custom controllers were implemented (RFCartersianMotionGenerator and RFMoveToContact), which uses a simulink plugin that does the necessary calculations.

The basic controllers and motion generators are already implemented.
If you want to use them you have to load them through the controller manager whilst executing the launchscript as described [here](start-up.md).
If they are running, the robot control interacts with the controllers and the controller manager via ROS actions, messages and services.
If you want to read more about the ros_control framework see [this](https://wiki.ros.org/ros_control).
However, a deeper understanding is not necessary to write and use robot programs with the framework.

The following sections will focus on understanding the robot control part of the framework.

# Robot Control

The base class of the framework is [RFRobotControl](@ref rf::RFRobotControl).
One instance of this class will interact with the UI and setup all utilities required for running a robot program.

## Model-View-Presenter with QT

The C++ framework QT is used to display the UI.
Therefore [RFRobotControl](@ref rf::RFRobotControl) is designed to be a model within the *Model-View-Presenter* design pattern used by QT.
This design pattern separates UI specific implementations from application specific implementations.
The data/model does not need to know how its data is displayed to the user.
In order to accomplish this, QT uses so called signals and slots. Think of these like app internal ros service calls between classes.
Slot functions can be treated (and called) as standard methods of a class.

Since the model is UI independent, the corresponding ros node does not necessarily need a UI to be executed.
However, the UI will be started automatically if you launch the node.

## Threading Model and callback execution

ROS uses function callbacks to handle all replies it receives from other nodes of the network.
A simple node usually has one global callback queue.
Lets suppose your node receives a sensor message from a sensor.
This message will not be processed immediately.
Instead the request to call the corresponding callback will be added to the callback queue.
Therefore, think of the callback queue as a To-Do-List of your node.

When you write a simple ros node, you usually create one instance of a class
and call ros::spin(). The call of ros::spin() invokes the execution of the callbacks
in the callback queue.
For the following it is important to understand that ros::spin() is short for:

        while (ros::ok())
        {
            ros::getGlobalCallbackQueue()->callAvailable(ros::WallDuration(0));
        }

This results in one basic problem: Lets suppose a callback for a service is called.
It is blocking and waits for some reason until it receives two sensor data messages
while it is executed.
This results in a deadlock.
The callbacks for the sensor data can not be called because the service callback
is executed and the callback queue is not serviced.

The same problem occurs with the execution of robot programs within RFRobotControl.
Whilst a program is called, it will block the execution of callbacks that are
required to monitor the robot state. Therefore RFRobotControl uses two threads.
One Thread services the base class RFRobotControl, the UI, and all utility callback
calls. The other thread services only the active robot program that is currently executed.
Therefore a robot program can react to sensor data changes.

However RFRobotControl does not use ros::spin() directly, because QT implements its own
callback mechanisms (called QTEventLoop) in order to support signals and slots (and a lot more).
ros::spinOnce() is called manually via a QTimer to mimic the normal ros::spin() behavior.

## Robot Commands and Robot Programs

The framework adapts ideas of the *composite pattern* because robot programs
may contain a mixture of subprograms and single commands.
A single superclass [RFCommand](@ref rf::RFCommand) provides an interface for all Robot Commands or Robot Programs.
In fact, robot programs (e.g. *Build Pyramid*) are treated exactly like single commands (e.g. *Move PTP*).

Each subclass of RFCommand has to implement an execute() function, that executes the 
main functionality. They may also implement a prepare() function. 
This function is intended to be called (manually) if heavy computation is necessary **prior** to execution.
Commands that require a preparation prior to execution should call prepare() automatically in case the user 
forgets or doesn't care.

Different commands may configure the same resource (e.g. ROS controllers). 
While the commands may change the configuration for this resource temporarily whilst they are being executed,
they have to reset the configuration afterwards. In other words: **Don't modify resources in nested command calls.**

An example for this design are the classes RFController and RFPTP.
The PTP command requires a running PTP controller. With RFController you can start this PTP controller manually.
Following the described concept RFPTP either starts its own controller, executes 
and stops it, if no controller is running prior to the call of RFPTP::execute(), or it uses the Controller controlled and started through RFController. Either way the status of the PTP controller remains the same before and after the call to RFPTP::execute().
