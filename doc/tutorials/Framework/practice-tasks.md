# Practice tasks for self-control

The following page lists some tasks and questions to check if you are ready for the robothon. 
If you can answer all questions and tasks, you are ready to go. If not, you should 
re-read some of the chapters in this documentation. Please do not look in the solutions 
until your done with your work.

## git

1. Name 4 git commands and explain what they do.

2. You have changed the file test.txt on your latest branch. 
   Which commands do you have to use to commit these changes?

## Framework

Write a simple program execution for the following task:
The points P1, P2, Home were teached in a previous step.

1. Move as fast as possible to the point P1.
2. From P1 move linear downward for 5cm with a maximum speed of 0.1 m/s.
3. Pick up a cube with an edge length of 1 cm.
4. Move linear with default speed to point P2.
5. Place the object at P2.
6. Move as fast as possible to Home.

For a simple context, you only have to rewrite this empty template of the existing
testprogram class RFTestProgram.

    void RFTestProgram::execute(){


​    
    }

---

When you are done, think about possible exceptions in your command sequence
and how to handle them accordingly.

 [Solution](practice-tasks-solutions.md)
