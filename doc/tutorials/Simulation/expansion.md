## Adding a currently unsupported robot or gripper

In order to successfully control a new robot or gripper, only a few tasks have to be done. 

1. Provide a full robot description in a URDF file. This is required in order for the controllers to work properly, since this file includes information about the links and joints. It contanis for example the joint names, their order, as well as the joint limits and collision behaviour. 

2. Provide a adequate CoppeliaSim model of the robot (and/or gripper). This means, that the joints of the model should match the URDF file, regarding their name and arrangement. Also, this has to be consistent with the parameters in the `config.yaml` file.

3. Create a `config.yaml` file. One can use the existing ones in this repository as a guideline. The neccessary parameters are the following:
    - **joint_names**
    - **arm_id** (Can be chosen, but makes sense to use the robot name)
    - **gripper_names** (i.e. the joint names of the gripper model in CoppeliaSim)
    - **tcp_name**
    - **base_name**
    - **dh_parameters** (can be retrieved from the robot documentation)
    - **controllers** (here: `rf_ptp_motion_generator` and `rf_lin_motion_generator`)

4. Combine the information above in a `robot.launch`. Here, arguments can be used to for example load specific parameters for different models of the same manufacturer. Also, an argument indicating whether to load the gripper control node is useful. In the launch file, two or three nodes are then started: the robot control node, the gripper control node (if needed) and the controller manager (responsible for loading the desired controllers).

