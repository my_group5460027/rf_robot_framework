# Structure

## General

Since the control framework works with generic controllers from the `ros_control` package, a hardware interface is required in order to send and receive data from the robot. In this case, the hardware interface is not specifically designed to fit one robot, but offers a generic solution for CoppeliaSim.  This is done, so that the customization of the simulation (switching robots, grippers etc.) only happens within CoppeliaSim, the source code of the control framework can be left as is. The designed hardware interface thus represents a "one-fits-all" solution. Development and tests were performed with the Franka Emika Panda and Universal Robots, but new robots can be easily added with this approach.

## Hardware Interface

### Controllers

As mentioned, the hardware interface is responsible for connecting the controllers to the their hardware. This is done by registering the corresponding interfaces with controller manager and implementing `read` and `write` functions. In this case, the robot control framework uses a `JointTrajectoryController` and a custom cartesian controller, which require a `JointStateHandle` and `FrankacartesianVelocityHandle`, respectively.  The corresponding ROS-node then creates an instance of this hardware interface as well as a controller manager, and runs the `read` -> `update` -> `write` cycle with 100 Hz. This rate has been shown to ensure smooth simulation results without using the BlueZero-Connection too extensively. Note that the cartesian motion generator returns cartesian velocities at a fixed rate of 1 kHz. Thus, in case of cartesian control, the controller manager is simply updated 10 times each cycle. The 10th cartesian velocities are then used to calculate the joint velocities using the inverse kinematics of the robot.

### Recovery

In certain cases, the control framework requires errors to be acknowledged by the user. Normally, the error recovery service of a real robot is used. Here, a simple action server is contained in the hardware interface, which simply returns _success_ once it receives a recovery goal. This is sufficient to give control back to the user.

### Franka_State Topic

Since the control framework is based on the `franka_ros` package, it relies on the `franka_state` topic. Normally, the real robot would publish on this topic, which contains data such as the joint positions/velocities/accelerations, the robot pose w.r.t the end effector, the current mode, information about errors and collisions etc. To ensure basic functionality, the joint positions, pose and robot mode need to be included in a state message. So, the hardware interface includes a routine which populates the corresponding fields in the message and works out the robot mode based on the simulation status (simulation running?) and robot movement (robot moving?)

## BlueZero-API

The hardware interface does not actually connect with CoppeliaSim directly, but via the BlueZero-Remote-API. The class `rf_robot` presents a generic robot. Here, all necessary API functions are set-up (such as `simxGetJointPosition` to read the joint positions), as well as the public functions to connect to the hardware interface (e.g. `setJointTargetPositions`). The API functions are easily identifiable by the `simx`-prefix. An object of this class creates a BlueZero-Client, which automatically connects to the BlueZero-Server of CoppeliaSim. Destructing the object automatically shuts down the BlueZero-Connection.

## Gripper

The gripper functionality is separated into four components: **Move**, **Grasp**, **Homing** and **Stop**. Each of those requires an action server. To keep the modular nature of the framework, Gripper functionalities are separated from the hardware interface and handled in a new class, `rf_gripper_interface`, with its own node. It contains the four necessary action servers, as well as an instance of the `rf_robot` class. This approach allows using different grippers or other end effectors with the same robot.