### List of common errors and how to fix them

This list may not cover all errors yet, thus will updated

#### 1. The model is not controllable using the GUI
**Possible reasons:** 
- The CoppeliaSim-Interface node is not running, has been started on its own -> start it using a launchfile corresponding to the used robot
- The b0remoteApi plugin is not running -> start it from CoppeliaSim under the `AddOns`-tab. Alternatively, enable autostart and restart CoppeliaSim.
- A wrong model is loaded -> make sure to either use the models provided with this framework, or verify that a custom model is parameterized correctly.

#### 2. A BlueZero Connection could not be established
**Possible reasons:**
- The b0remoteApi plugin is not running -> start it from CoppeliaSim under the `AddOns`-tab. Alternatively, enable autostart and restart CoppeliaSim.
- CoppeliaSim was started after the framework -> Order of operation is important, restart the framework's nodes once CoppeliaSim and the b0RemoteApi Server are running

#### 3. Connection is succesfully established, but robot does not move when simulation is running
**Possible reasons:**
- The control loops of the robot joints are disabled. -> Select joint, open the Joint Dynammics Properties under the Scene Object Properties. Verify that the control loop is enabled for all joints.
- A previous error has not been acknowledged -> Acknowlegde from the GUI

#### 4. The gripper does not move
**Possible reasons:**
- The control loops of the gripper joints are disabled -> see _Error 3_ 

#### 5. Robot performs incorrect linear movements 
**Possible reasons:**
- Incorrect DH parameters -> verify 
- Incorrection DH convention -> either `DH` or `Craig`, verify
