## Requirements

First, download and unpack [Coppeliasim](https://www.coppeliarobotics.com/downloads). 

This framework has been developed with Ubuntu 18.04, so it is recommended to download the corresponding Version.

Communication between the framework and Coppeliasim is handled via a [BlueZero-Connection](https://github.com/CoppeliaRobotics/bluezero). 
It is lightweight server/client solution for fast message transport between nodes. Here, Coppeliasim acts as the **server**, and the frameworks establishes a **client**. Firstly, the BlueZero-Api must be enabled on the server and client side. 

__*Server Side*__

 Make sure the file `libsimExtB0.so` is in the Coppeliasim-folder and succesfully loaded on startup. To do so, either: 

- Manually launch it via Add-Ons->B0RemoteApiServer 
  
  __OR__ 

- Open *lua/b0RemoteApiServer.lua* in the Coppeliasim-folder and edit the `sysCall_info()` function, set `autoStart=true` an restart Coppeliasim

__*Client Side*__

**The following steps are only needed if you do not want to use the rf_robot_control framework** 

- From the CoppeliaSim-folder, copy the following files and folders into your project
  
  - _programming/b0remoteApiBindings/cpp/b0RemoteApi.h_ 
  
  - _programming/b0remoteApiBindings/cpp/b0RemoteApi.cpp_
  
  - _programming/b0remoteApiBindings/cpp/msgpack-c/include_
  
  - _programming/blueZero/include/b0/bindings_
    
    or link the two folders accordingly in your `CMakeList.txt`

- Link the library `libb0.so` in your `CMakeList.txt` as follows:
  
    `find_library(b0lib b0)`
    `target_link_libraries(NAME_OF_PROJECT ${b0lib} )`

In order to use THe blueZero API, the corresponding library has to be copied and linked. From a terminal in the CoppeliaSim main-folder:

    sudo cp libb0.so /usr/local/lib/libb0.so
    ldconfig -n -v /usr/local/lib

All of the above steps have already been performed if you clone this project, however if you face issues a newer version of CoppeliaSim, it may help to perform those steps again. The Framework has been tested for CoppeliaSim Version 4.2.0, for Ubuntu 18.04 and 20.04.
