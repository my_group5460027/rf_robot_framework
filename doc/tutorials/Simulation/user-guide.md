# Using the simulation environment

## Prerequisites

Working with the simulation environment requires only little preparation. Each robot requires a configuration and a launch file as well as a working CoppeliaSim model and URDF-file. So far, Franka Emika Panda and Universal Robots are supported. 

Verify that the configuration-file is in the correct folder and contains  the necessary fields `ptp_motion_generator`, `lin_motion_generator` , `joint_names`, `arm_id`,  `gripper_names` and `tcp_name`.  The joint names used here have to exactly match the ones in the URDF-file and the CoppeliaSim-model! 

### New robot

For a new robot, consider following aspects:

**Configuration-file**

- define motion generators with correct joint names
- names of gripper joints must match CoppeliaSim model

**Launch file**

- `robot_description` links to URDF-model of the robot
- load the corresponding configuration-file
- start the hardware-interface node (and gripper node if desired)
- start the controller manager with your specified controllers

**URDF file**

- required by `ros_control` and the inverse kinematics solver used for cartesian motion

The Simulation with a new robot is not guaranteed to be stable or fully functional!

## Start-Up
In order to simulate robot programs, the software CoppeliaSim is used

The order of operation is crucial, CoppeliaSim needs to be prepared before starting the control framework!

1. On the PC, the command  

		cont
	
	will bring up a development container. Open vscode and select the Docker extension in the left sidebar, then right click on the running container (simrosframework:latest) and select `Attach Visual Studio Code`. Now you can start develop your code. For building the project your can either use the command  

		catkin build 
		
	If you need to rebuild the container image (for updates) you can use the command 

		 contbuild

2. Open a terminal and run

		cd ~/coppelia_sim
	
 	to your CoppeliaSim-Folder and run  
 
 		./coppeliaSim.sh
	
	to start the software.

3. Start the BlueZero-server by clicking Modules -> Connectivity -> b0 remote API server. (To enable autostart, open *lua/b0RemoteApiServer.lua* in the Coppeliasim-folder and edit the `sysCall_info()` function, set `autoStart=true` ).

4. Select a robot from the model folder of this framework (not the model browser supplied by CoppeliaSim! Those are not guaranteed to work correctly). So far, you can choose between the Franka Emika Panda and Universal Robot models UR3 and UR5, just make sure your configuration files are set up to match the chosen robot.

5. Run the command 

		rosrun rf_robot_control node

 	to start the control-GUI, and 
 
 		roslaunch rf_coppelia_interface YOUR_ROBOT.launch

	to start the hardware interface and controller manger. You may specify whether you want to use a gripper, the default is `gripper:=true` for Panda and `false` for Universal.

6. 	Once the GUI shows the robot mode **other** (yellow), the 	simulation can be controlled. You might want to change the mode to `Simulation` for extra functionality.




