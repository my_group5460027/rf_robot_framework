/// @file rf_move_to_contact.h
///
/// @author Tom Hattendorf
/// @date 08.10.2021
///
/// @brief Class definition of RFMoveToContact
///
/// rf_move_to_contact.h
/// rf_move_to_contact
///
/// Created by Tom Hattendorf on 08.10.2021 for a project thesis at the
/// Institute of Automatic Control (IRT), Leibniz University Hannover.
///
///

#ifndef RF_MOVE_TO_CONTACT_H
#define RF_MOVE_TO_CONTACT_H

#include <actionlib/server/simple_action_server.h>
#include <controller_interface/multi_interface_controller.h>
#include <hardware_interface/robot_hw.h>
#include <pluginlib/class_list_macros.h>
#include <math.h>

#include <future>
#include <memory>

#include "franka_hw/franka_cartesian_command_interface.h"
#include "franka_hw/franka_state_interface.h"
#include "mogen_p2p/mogen_p2p_wrapper.hpp"
#include "rf_move_to_contact/MoveToContactAction.h"
#include "ros/ros.h"

namespace rf {

/// @brief A cartesian motion generator for the FrankaVelocityCartesianInterface
///
/// This motion generator recieves MoveToContact actions and pursues their
/// goal. It uses a simulink plugin pipeline motion generator internally, that
/// is C2 steady. Using the Franka State Interface, the external Wrench in end effector coordinate
/// is constantly monitored and the motion is cancelled, if the threshold given by the 
/// action is exceeded.
///
/// To seperate the fast control cycles from the slow action server commands,
/// this class uses two threads when necessary: One for the normal update()
/// functions of ros_control and one for the action server. Please note that the
/// action server handling thread is only launched if the action server is
/// running (via std::async).
///
/// To sync the action server status with the control logik simple bool flags
/// are used.
///
/// Typical lifecycle behaviour:
/// 1. Controller is not running
/// 2. Controller manager starts controller. The action server comes up.
///	3. If no goal is recieved, update() is called but no velocities are
///commanded.
/// 4. If a goal is recieved and accepted the motion generator is initialized
/// and velocities comanded.
/// 5. If the goal is reached. Set succeded => Jump to 3.
class RFMoveToContact
    : public controller_interface::MultiInterfaceController<
          franka_hw::FrankaVelocityCartesianInterface,
          franka_hw::FrankaStateInterface> {
    typedef actionlib::SimpleActionServer<
        rf_move_to_contact::MoveToContactAction>
        ActionServer;

   private:
    /// @name Action Server
    /// @{

    /// @brief The action server to get goals to pursue
    ActionServer m_actionServer;

    /// @brief The void result of the action server thread
    /// This needs to be in scope in order for the std::async call to work.
    std::future<void> m_actionServerThreadResult;

    /// @brief A pointer to the currently pursued goal
    rf_move_to_contact::MoveToContactGoalConstPtr m_activeGoal;

    /// @}

    /// @name ros_control hardware resources
    /// @{

    /// @brief claim Hardware Resource through interface
    /// This interface needs to be in scope as long as the handel exists.
    franka_hw::FrankaVelocityCartesianInterface* m_pVelocityCartesianInterface;

    /// @brief Handle and command the claimed Interface
    std::unique_ptr<franka_hw::FrankaCartesianVelocityHandle>
        m_pCartesianVelocityHandle;

    franka_hw::FrankaStateInterface* m_pFrankaStateInterface;
    std::unique_ptr<franka_hw::FrankaStateHandle> m_pFrankaStateHandle;

    /// @}

    /// @name Simulink motion generator
    /// @{

    /// @brief Simulink motion generator class
    mogen_p2p::mogen_p2p m_motionGenerator;

    /// @brief Input values for the motion generator
    ///
    /// Only inputs are the t_scale (override) factors
    /// for translation and rotation
    ///
    /// The input is used every control step.
    mogen_p2p::In_U_mogen_p2p m_inputMotionGenerator;

    /// @brief Input parameters for the motion generator
    ///
    /// Parameters are max velocity and acceleration in m/s and rad/s,
    /// The starting transformation and the desired transformation.
    ///
    /// The parameters are used once at initialization.
    mogen_p2p::In_P_mogen_p2p m_parametersMotionGenerator;

    /// @brief Output of the motion generator
    ///
    /// The output is a velocity vector (aka twist) that can be passed to
    /// the cartesian velocity interface.
    ///
    /// It is calculated every time step.
    mogen_p2p::Out_Y_mogen_p2p m_outputMotionGenerator;

    /// @brief Log output
    ///
    /// Bool flag if done.
    mogen_p2p::Out_L_mogen_p2p m_logMotionGenerator;

    /// @}

    /// @name control status
    /// @{

    /// @brief Should the output of m_motionGenerator be calculated and
    /// commanded to the robot?
    bool m_control = false;

    /// @brief Is the force limit exceeded and should the motion be stoppped?
    bool m_forceStop = false;

    /// @brief Vector containing the x, y, z translational and rotational velocity commands (in base frame) to be sent to the robot
	std::array<double, 6> m_command;

    /// @brief Measured contact force when Controller is initialized, in order to tare incoming measurements
    std::array<double, 3> m_forceBias;

    /// @brief Is the motion generator initalized
    bool m_motionGeneratorInitialized = false;

    /// @brief The goal was reached
    ///
    /// @see m_controlCyclesToSuccess
    bool m_controlGoalReached = false;

    /// @brief The count of zero velocity commands.
    /// This is used to check if m_controlGoalReached
    int m_zeroCommandsCount = 0;

    /// @brief Is the controller started?
    bool controllerIsRunning = false;

    /// @brief Update rate of the action server in Hz
    /// Running the controller with 1kHz the action server is approximatly
    /// called every 10th update cycle.
    const int m_ACTION_SERVER_UPDATE_RATE = 100;

    /// @brief Number of zeroCommands in sequence until the goal is marked as
    /// succeded A value of 20 Cycles means a success if the robot does not move
    /// within 0.02s, if the update rate is 1kHz.
    ///
    /// @attention Keep in mind that a lower value increases the risk of an
    /// aborted motion. Usually the first update cycles in the acceleration
    /// phase of the motion have also zero velocity and may be classified as a
    /// success.
    const int m_controlCyclesToSuccess = 5;

    /// @brief Elapsed time after the start.
    /// Currently not used.
    ros::Duration m_elapsedTime;

    /// @brief Initial robot pose when accepting a new goal
    std::array<double, 16> m_initialPose{};

    /// @brief Goal robot pose when accepting a new goal
    std::array<double, 16> m_goalPose{};

    /// @brief The absolute value of the measured contact force in all directions
    double m_contactForce;

    /// @brief Force limit for the commanded movement. Exceeding it will abort movement.
    double m_forceLimit;

    /// @brief Control cycles to satisfy the force limit criterion continuously
    ///
    /// @see m_controlCyclesToSuccess
    int forceLimitToSuccess = 20;

    /// @brief Count of continuous force measurements that exceed the limit
    ///
    /// @see m_zeroCommandsCount
    int forceLimitCount = 0;

    /// @}

    /// @brief Update the action server and control status once
    ///
    /// Accepts new goals or preempts them if necessary and
    /// updated the required control status
    void updateActionServer();

    /// @brief Generate stop commands, the commanded velocity is descreased by 10% in every control cycle until it is low enough 
    /// to be considered = 0
    ///
    /// @param command Vector of commanded velocities
    /// @param period Time elapsed since last controller update
    bool stopCommand(std::array<double, 6> &command, ros::Duration period);

    /// @brief Function to execute via std::async to handle the ActionServer
    /// whilst the controller is running.
    ///
    /// Only exits with a result if controllerIsRunning is false.
    void handleActionServer();

    /// @brief Parse the goal of MoveToContact to parameters of Simulink
    /// motion generator
    ///
    /// In relative mode O_T_EE_d does not store an absolute position
    /// but an relativ value offset.
    /// The target position will be calculated as:
    /// O_TT_EE_target = O_TT_EE_now + O_T_EE_d
    ///
    /// @todo Only checked for translation.
    /// Does this work for rotation, too?
    void parseGoal();

    /// @brief Parse the output of the motion generator to an array
    ///
    /// This array can be commanded to the cartesian velocity interface
    ///
    /// @param [OUT] command twist array to command to the cartesian velocity
    /// interface
    ///
    /// @returns True if at least one commanded value is non zero.
    bool generateCommand(std::array<double, 6>& command);

    /// @brief Check if the double is Zero
    bool isZero(double number);

    /// @brief Increment m_zeroCommandsCount and check if
    /// m_controlCyclesToSuccess are reached.
    ///
    bool checkSuccess(bool isZeroComand);

    /// @brief Check if the measured contact force exceeds the determined limit,
    /// increment forceLimitCount and check if forceLimitToSuccess is reached
    bool checkForce();

    /// @brief Get measured end effecor contact force and calculate absolute value, determined by 
    /// direction of the end effector
    double getForce(); 

   public:
    RFMoveToContact();
    virtual ~RFMoveToContact();

    /// @name ros_control functions
    /// Please see ros_control for a description of the purpose of these
    /// functions.
    /// @{
    bool init(hardware_interface::RobotHW* robot_hardware,
              ros::NodeHandle& node_handle) override;
    void starting(const ros::Time&) override;
    void update(const ros::Time&, const ros::Duration& period) override;
    void stopping(const ros::Time&) override;
    ///@}
};

}  // namespace rf

#endif
