#!/bin/bash

# Run this script to prepare the nuc

sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install -y docker.io curl 
sudo groupadd docker || true
sudo usermod -aG docker $USER

# Start a docker container without gui but with real time capabilities
while read -r line; do 
    echo "alias contkernel='docker run -it --network host --cap-add SYS_NICE --mount type=bind,source=$line,target=/root/catkin_ws registry.gitlab.com/my_group5460027/rf_robot_framework:latest'" >> ~/.bashrc
    echo "Setting workspace path to: $line"
done < ~/catkin_ws/src/workspace_path.txt

echo "

Success, please restart the pc before you continue."


